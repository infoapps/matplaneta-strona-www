<!-- get header -->
<?php $root = realpath($_SERVER["DOCUMENT_ROOT"]);
require("$root/inc/header.php");?>

<!-- get sidebar-->
<?php require("$root/inc/sidebar.php");?>

<!-- start szkoly-->
<section id="success" class="wrapper">
        <div class="container">
            <div class="section-header">
                <h2>
                    Sukcesy
                </h2>
                <img src="/assets/images/border.png" alt="">
            </div><!-- end section-header -->
            <div id="success-info">
                <h5>Sukcesy naszych uczniów są dla nas zarówno ogromnym powodem do dumy jak i potwierdzeniem tego, że zajęcia w Matplanecie rozwijają logiczne oraz matematyczne myślenie u dzieci już od najmłodszych lat.
                    <br>
                    Zatem z wielką przyjemnością dzielimy się tymi sukcesami, o których wiemy oraz zachęcamy wszystkich do wysyłania informacji o swoich osiągnięciach matematycznych, logicznych, komputerowych, a także sportowych jak się okazuje:)
                </h5>
            </div><!-- end success info -->
        </div><!-- end container -->
    <!-- celebryty list -->
    <ul class="celebryty">
	    <li class="smokee">
           <div class="container">
               <div class="circle">
                   <img src="/assets/images/miejsce_odkrywania_talentow.jpg" class="circlee">
                   <div class="caption">
                       <h5 class="celebryty-name">Miejsce Odkrywania Talentów</h5>
                       <p>
                           W 2013 roku Ośrodek Rozwoju Edukacji, działający przy Ministerstwie Edukacji Narodowej przyznał Matplanecie prestiżowy tytuł „Miejsca Odkrywania Talentów”.
                       </p>
                   </div><!-- end caption -->
               </div><!-- end circle -->
           </div><!-- end container -->
        </li><!-- end list-element -->
        <li>
            <div class="container">
               <div class="circle-second">
                   <img src="/assets/images/slonecznik.jpg">
                   <div class="caption-second">
                       <h5 class="celebryty-name">Nagroda "Słoneczniki" dla Matplanety</h5>
                       <p>
W latach 2013, 2014, 2015 Matplaneta uzyskała nominacje do nagrody „Słoneczniki” na najbardziej rozwojową inicjatywę dla dzieci w kategorii Logika w Polsce. W 2015 r. oddział Matplanety w Warszawie oraz Krakowie  zdobył Złotego Słonecznika - główną nagrodę przyznawaną przez zespół ekspertów z zakresu rozwoju i wychowania.
                       </p>
                   </div>
               </div>
            </div><!-- end container -->
        </li><!-- end list-element -->
        <li class="smokee">
           <div class="container">
               <div class="circle">
                   <img src="/assets/images/matplaneta.jpg">
                   <div class="caption">
                       <h5 class="celebryty-name">Marzenka - Grupa Pitagoras 2</h5>
                       <p>Marzenka zajęła w tym roku 209 miejsce w kraju w Alfiku Matematycznym (na 8 924 uczniów biorących udział). Kangura Matematycznego w kategorii Maluch napisała na 113,75 pkt / 120 pkt - wynik bardzo dobry.W Dzielnicowym Konkursie Matematycznym Sigma (Wola) "ex aequo" zajęła 1 miejsce (25/25 pkt), a w ogólnopolskim 7!</p>
                   </div><!-- end caption -->
               </div><!-- end circle -->
           </div><!-- end container -->
        </li><!-- end list-element -->

        <li>
           <div class="container">
               <div class="circle-second">
                   <img src="/assets/images/matplaneta.jpg">
                   <div class="caption-second">
                       <h5 class="celebryty-name">Jaś - Grupa Młode Talenty</h5>
                       <p>Jaś po raz drugi z rzędu zdobył wyróżnienie w Kangurze Matematycznym, osiągnął na testach na koniec roku poziom zakładany dla klas starszych, a jego trener z piłki nożnej szybko zauważył, że na boisku myśli bardzo logicznie, bo najmniej się nabiegał, a osiągnął najwięcej punktów w określonych zadaniach.
</p>
                   </div><!-- end caption-second -->
               </div><!-- end circle-second -->
           </div><!-- end container -->
        </li><!-- end list-element -->


        <li class="smokee">
           <div class="container">
               <div class="circle">
                   <img src="/assets/images/Oliwka2.jpg" class="circlee">
                   <div class="caption">
                       <h5 class="celebryty-name">Oliwka – Pascal 1</h5>
                       <p>
                          Moja córka 6 letnia Oliwka już drugi rok uczestniczy w zajęciach w Matplanecie. Rok temu moim marzeniem było, aby z wiekiem nauczyła się myśleć w kategoriach matematycznych, po kilku zajęciach moje marzenie się spełniło. Oliwka liczyła w niestandardowy sposób 4 +6, odjęła od sześciu jeden i dodała do czterech, bo 5+5 to 10. Pani prowadząca zajęcia nie skarciła jej, że liczy dziwnie, ale zapytała inne dzieci co sądzą o sposobie obliczeń Oliwki. Oliwka, prawdopodobnie podświadomie, uzmysłowiła sobie, że nie musi myśleć tak jak większość, ale śmiało może tworzyć własne sposoby liczenia. 
                        <br>
					   <span class="celebryty-name">~ Sławomir Czapliński (tata 6 letniej Oliwki i 4 letniej Zuzi)</span>
					   </p>
                   </div><!-- end caption -->
               </div><!-- end circle -->
           </div><!-- end container -->
        </li><!-- end list-element -->
         <li>
            <div class="container">
               <div class="circle-second">
                   <img src="/assets/images/Zuzia2.jpg">
                   <div class="caption-second">
                       <h5 class="celebryty-name">Zuzia – Fermat 1</h5>
                       <p>
                          Moja druga córka Zuzia kiedyś nie była na zajęciach, na których dzieci uczyły się ułamków zwykłych. Na następnych zajęciach Pani wyjaśniła jej, a Zuzia ku naszemu z żoną  pewnemu zaskoczeniu podzieliła w domu pizzę stosując ułamki zwykłe. Matplaneta pomaga dzieciom słabszym z matematyki zrozumieć podstawowe zasady, a z tych przeciętnych czyni uzdolnionymi matematycznie, którzy za jakiś czas będą laureatami olimpiad matematycznych. 
                          <br>
                          
                          <span class="celebryty-name">~ Sławomir Czapliński (tata 6 letniej Oliwki i 4 letniej Zuzi)</span>    
                       </p>
                   </div>
               </div>
            </div><!-- end container -->
        </li><!-- end list-element -->
         <li class="smokee">
           <div class="container">
               <div class="circle">
                   <img src="/assets/images/matplaneta.jpg" class="circlee">
                   <div class="caption">
                       <h5 class="celebryty-name">Maja – Pascal 1</h5>
                       <p>
                          Maja jest uczennicą Matplanety w grupie Pascal 1. Kiedy wykonano jej test gotowości szkolnej, oceniono ją, że jest geniuszem z matematyki. Mogę przypuszczać, że jej  matematyczne umiejętności kilkakrotnie przewyższały rówieśników. W Matplanecie umysł dziecka uczy się myślenia w kategoriach matematycznych. Dziecko nie boi się wypłynąć na głębokie wody, wierzy w siebie, ponieważ wielokrotnie znajdowało rozwiązanie.
Matplaneta stworzyła warunki dla dzieci do lepszego, niż szkolna edukacja, poznania matematyki.
<br>

  
                       </p>
                   </div><!-- end caption -->
               </div><!-- end circle -->
           </div><!-- end container -->
        </li><!-- end list-element -->
    </ul><!-- end list -->
    </section><!-- end sukcesy -->
<!-- get footer -->
<?php require("$root/inc/footer.php");?>