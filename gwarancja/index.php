<!-- get header -->
<?php $root = realpath($_SERVER["DOCUMENT_ROOT"]); 
require("$root/inc/header.php");?>

<!-- get sidebar-->
<?php require("$root/inc/sidebar.php");?>

<!-- start zapisy -->
            <section id="zapisy" class="wrapper">
                <div class="container">
                    <div class="section-header">
                        <h2>
                            Gwarancja
                        </h2>
                        <img src="/assets/images/border.png" alt="">
                    </div>
                    <div class="regulamin">
                        <h4>
                            Szanowni Państwo,
                        </h4>
                        <p>
                           w bieżącym roku szkolnym wprowadzamy promocję dla dzieci uczęszczających na nasze zajęcia. Celem tej promocji jest danie rodzicom do ręki
						   namacalnej korzyści z chodzenia na nasze zajęcia (a wielu bardzo tego potrzebuje) oraz zwiększenie powtarzalności (kontynuacji) dzieci na zajęciach.
                        </p>
						<p>
                           W tej chwili wielu rodziców traktuje nasze zajęcia jako coś zupełnie dodatkowego, nieco jak rozwijanie talentów. Z tego względu, w kolejnych latach
						   edukacji dzieci zajęcia te nie są priorytetem jeżeli nie da się ich wpleść do harmonogramu dziecka. Chcąc temu zaradzić wprowadzamy system, w którym
						   regularne chodzenie na nasze zajęcia będzie się wiązało z gwarancją uzyskania 90% punktów z części matematycznej egzaminu szóstoklasisty. Poniżej
						   szczegółowe zasady funkcjonowania tej gwarancji.
                        </p>
                        <h5 class="celebryty-name">
                            Warunki uzyskania tej gwarancji są następujące:
                        </h5>

                        <p>
                            •	dziecko uczestniczy w naszych zajęciach co najmniej od poziomu Pitagoras (nie później niż 2 klasa szkoły podstawowej) do 6 klasy
                        </p>
						<p>
                            •	ma 80% obecności na zajęciach w każdym semestrze
                        </p>
						<p>
                            •	ma opłacone wszystkie miesiące nauki w terminie (dopuszczamy poślizg do 7 dni)
                        </p>
						<p>
                            •	prowadzi dzienniczek, w którym rejestrowane są jego obecności na zajęciach.
                        </p>


						<h5>
                            Promocja jest dostępna również jeżeli dziecko chodziło do Matplanety dotychczas (czyli wstecznie spełnia te warunki, poza dzienniczkiem oczywiście).
                        </h5>
						
                        <h5 class="celebryty-name">
                            Gwarancja polega na tym, że jeżeli dziecko osiągnie mniej niż 50% punktów - zwracamy całą kwotę zapłaconą za jego zajęcia. Jeżeli zaś uzyska od 50 do 90% - zwracamy połowę kwoty zapłaconej za zajęcia.
                        </h5>

                      
                    </div>
                    

                </div>
            </section>

<!-- get footer -->
<?php require("$root/inc/footer.php");?>