<?php

class Harmonogram {
    private $postBody = 
    '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
        <soapenv:Header/>
          <soapenv:Body>
                <tem:GetHarmonogram/>
          </soapenv:Body>
     </soapenv:Envelope>';
     
     private $headers = array('Content-Type: text/xml', 'SOAPAction: http://tempuri.org/IMatplanetaService/GetHarmonogram');
     
     private $serviceUrl = 'http://polygon.matplaneta.pl/Service/MatplanetaService.svc';
     
     private $soapResponse;
     
     private $harmonogram;
     
     public function setSoapResponse() {
         $ch = curl_init($this->serviceUrl);
         curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers); // Request headers
         curl_setopt($ch, CURLOPT_POST, true); // Request method
         curl_setopt($ch, CURLOPT_POSTFIELDS, $this->postBody); // Request fields
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Request response
         $this->soapResponse = curl_exec($ch);
     }
     
     public function parseToJson() {
         $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $this->soapResponse);
         $xml = new SimpleXMLElement($response);
         $body = $xml->xpath('//sBody')[0];
         $json = json_encode((array) $body->GetHarmonogramResponse->GetHarmonogramResult ); 
         $this->harmonogram = $json;
     }
     
     public function getHarmonogram() {
         return $this->harmonogram;
     }
}

$harmonogram = new Harmonogram();
$harmonogram->setSoapResponse();
$harmonogram->parseToJson();

header('Content-Type:  text/json');
echo $harmonogram->getHarmonogram();