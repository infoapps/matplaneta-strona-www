<!-- get header -->
<?php $root = realpath($_SERVER["DOCUMENT_ROOT"]);
require("$root/inc/header.php");?>

<!-- get sidebar-->
<?php require("$root/inc/sidebar.php");?>

    <!--work-section-->
    <section id="work-section" class="wrapper">
        <div class="container">
            <div class="section-header">
                <h2>
                    Praca
                </h2>
                <img src="/assets/images/border.png" alt="border">
            </div>
            <div class="col12">
                <h4>
                    Kogo poszukujemy ?
                </h4>
            </div>
            <div id="Oferty" class="clear-row-2 cushycms">
                <div class="col6 work-offer">
                    <h4>Nauczyciel matematyki lub programowania</h4>

                    <p class="expectations">OCZEKUJEMY:</p>
                    <ul>
                        <li>wykształcenia z zakresu nauk ścisłych chętnie uzupełnionego o wykształcenie
                            pedagogiczne
                        </li>
                        <li>doświadczenia w pracy z dziećmi w wieku 5-12 lat</li>
                        <li>otwartości na nauczanie matematyki według innej metodyki niż w szkole</li>
                        <li>chęci przekazywania umiejętności i czerpania satysfakcji z postęp&oacute;w osiąganych
                            przez dzieci
                        </li>
                        <li>kreatywności w ilustrowaniu praw matematycznych rzeczywistymi sytuacjami</li>
                        <li>systematyczności w dokumentacji przeprowadzanych zajęć</li>
                        <li>komunikatywności w interakcjach z dziećmi i rodzicami</li>
                        <li>odpowiedzialności w podejściu do obowiązk&oacute;w zawodowych</li>
                        <li>znajomości języka angielskiego w stopniu umożliwiającym komunikację r&oacute;wnież w
                            zakresie matematyki (mile widziane certyfikaty potwierdzające umiejętności)
                        </li>
                    </ul>
                    <p class="expectations">OFERUJEMY:</p>
                    <ul>
                        <li>Osoby pracujące w Matplanecie to młody zesp&oacute;ł. Wchodząc do tego zespołu,
                            przystępuje się do organizacji dynamicznej i bardzo kreatywnej.
                        </li>
                        <li>Ciągle szukamy nowych pomysł&oacute;w na przybliżanie piękna nauk ścisłych dzieciom,
                            dlatego dla os&oacute;b ceniących piękno tych nauk na pewno Matplaneta jest odpowiednim
                            środowiskiem pracy.
                        </li>
                        <li>Nasi najlepsi nauczyciele rozwijają swoją karierę razem z firmą, obejmując w niej r&oacute;wnież
                            funkcje biznesowe.
                        </li>
                        <li>Oferujemy dużo wyzwań, męczącą ale niezwykle wzbogacającą pracę, dobre wynagrodzenie i
                            środowisko promujące rozw&oacute;j własnych umiejętności.
                        </li>
                    </ul>
                </div>
                <div class="col6 work-offer">
                    <h4>Animator (ferie zimowe)</h4>

                    <p class="expectations">OCZEKUJEMY:</p>
                    <ul>
                        <li>Szukamy student&oacute;w lub absolwent&oacute;w studi&oacute;w wyższych uzupełnionych o
                            wykształcenie pedagogiczne lub udokumentowane doświadczenie w pracy z dziećmi; chętne do
                            kreatywnego spędzania czasu z dziećmi.
                        </li>
                        <li>Znajomość zestaw&oacute;w i program&oacute;w Lego Education WeDo i Mindstorms EV3 mile
                            widziana. Dla chętnych jest możliwość poznania program&oacute;w w naszym biurze przed
                            feriami.
                        </li>
                        <li>Praca w r&oacute;żnych dzielnicach Warszawy oraz J&oacute;zefosławiu obok Piaseczna; od
                            poniedziałku do piątku - cały lub p&oacute;ł dnia w trakcie warszawskich ferii zimowych
                            (19-31 stycznia). Możliwość przedłużenia wsp&oacute;łpracy po zakończeniu ferii.
                        </li>
                    </ul>
                    <p class="expectations">OFERUJEMY:</p>
                    <ul>
                        <li>Oferujemy pracę w zakresie prowadzenia zajęć dla dzieci w czasie ferii zimowych - &quot;Gry
                            i Zabawy Świata&quot; i / lub &quot;Programowanie robot&oacute;w Lego WeDo i Mindstorms&quot;
                            w naszych centrach edukacyjnych.
                        </li>
                        <li>Oferujemy pracę w nowatorskim projekcie, szkolenie, dużo wyzwań oraz niezwykle
                            wzbogacającą pracę, bardzo dobre wynagrodzenie i środowisko promujące rozw&oacute;j
                            własnych umiejętności.
                        </li>
                        <li>Praca w r&oacute;żnych dzielnicach Warszawy oraz J&oacute;zefosławiu obok Piaseczna; od
                            poniedziałku do piątku - cały lub p&oacute;ł dnia w trakcie warszawskich ferii zimowych
                            (19-31 stycznia). Możliwość przedłużenia wsp&oacute;łpracy po zakończeniu ferii.
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col12">
                <p>
                    W przypadku zainteresowania naszą ofertą, prosimy potencjalnych kandydatów o przesłanie swojego
                    CV oraz listu opisującego dlaczego zainteresowaliście się Państwo niniejszą ofertą oraz przebieg
                    dotychczasowej kariery zawodowej / studiów. Adres poczty elektronicznej: biuro@matplaneta.pl
                </p>
            </div>
        </div>
    </section>

<!-- get footer -->
<?php require("$root/inc/footer.php");?>