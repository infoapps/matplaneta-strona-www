<!-- get header -->
<?php require("inc/header.php");?>
<!-- get sidebar-->
<?php require("inc/sidebar.php");?>


        <section id="slider-section" class="wrapper" style="display: block;">
            <div class="container">
                <div class="section-header">
                    <h2>
                        Mówią o nas
                    </h2>
                    <img src="assets/images/border.png" alt="border">
                </div>
                <div class="col12">
                    <ul class="rslides">
                        <li>
                            
                            <div>
							<img src="assets/images/slider1-vimeo.png" alt="">
							
                                <h3>Matplaneta na vimeo</h3>
                                <p>
                                    W Matplanecie na bieżąco tworzymy krótkie filmy i animacje, które idealnie oddają to, czym jest Matplaneta oraz pokazują najlepsze wspomnienia z naszych zajęć, wakacji oraz ferii.
                                
								</p>
								<a class="zapisy" href="https://vimeo.com/125895045" target="_blank" style="float: right;">Zobacz</a>
								
                            </div>
							
                        </li>
                        <li>
							
							<div>
							<img src="assets/images/slider2-tvp.png" alt="">
                                <h3>Matplaneta w TVP ABC </h3>
                                <p>
                                    Rok temu rozpoczęliśmy współpracę z dziecięcym kanałem TVP ABC.</br>
Wspólnie nakręciliśmy kilka odcinków programu "Masz Wiadomość", który przybliża dzieciom i młodzieży tajniki świata komputerów i internetu. Pierwsze odcinki, w których brali udział nasi uczniowie opowiadały o zakładaniu poczty w internecie, a następne pokazywały, jak ciekawie obrobić swoje zdjęcia w dostępnych aplikacjach oraz jak się chronić przed zagrożeniami w sieci.
                               
								</p>
								 <a class="zapisy" href="http://abc.tvp.pl/18484250/odc-2" target="_blank" style="float: right;">Zobacz</a>
                            </div>
							
						</li>
						<li>
                            
                            <div>
							<img src="assets/images/slider3-digikids.png" alt="">
                                <h3>Matplaneta i Digikids</h3>
                                <p>
                                    Od zeszłego roku Matplaneta została partnerem DigiKids - programu, który ma na celu rozbudzić w dzieciach naukową pasję poprzez pokazanie, że technologia i nauki ścisłe to fascynująca przygoda.
W ramach współpracy prowadzimy inspirujące warsztaty, które przybliżają dzieciom tajemnice matematycznego świata.
                                
								</p>
								<a class="zapisy" href="https://www.youtube.com/watch?v=06Sqb5tNccw" target="_blank" style="float: right;">Zobacz</a>
                            </div>
							
                        </li>
                        <li>
							
							<div>
							<img src="assets/images/slider4-radiokolor.png" alt="">
                                <h3>Radio Kolor</h3>
                                <p>
                                    Kiedy dzieci uczą się matematyki najefektywniej?</br>
Czy wszystkie dzieci nie lubią matematyki?</br>
Na czym tak naprawdę polega nauka matematyki i dlaczego warto, by dzieci zdobyły Matplanetę?:)</br>
Na te pytania odpowiadają w Radiu Kolor Eksperci z Matplanety.</br>
Od 31 sierpnia do 4 września 2015r. można było nas usłyszeć na 103 Fm!
                                </p>
                            </div>
						</li>
                    </ul>
                </div>
            </div>
        </section>
 

<!-- get footer -->
<?php require("inc/footer.php");?>

<script src="/scripts/responsiveslides.min.js"></script>