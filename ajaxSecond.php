<?php

// We will be using this class to define each selectbox

class SelectBox{
	public $items = array();
	public $defaultText = '';
	public $title = '';
	
	public function __construct($title, $default){
		$this->defaultText = $default;
		$this->title = $title;
	}
	
	public function addItem($name, $connection = NULL){
		$this->items[$name] = $connection;
		return $this; 
	}
	
	public function toJSON(){
		return json_encode($this);
	}
}


/* Configuring the selectboxes */

// Product selectbox

$productSelect = new SelectBox('Wiek','Wybierz wiek dziecka');
$productSelect->addItem('4-5	 lat','fermatSelect')
			  ->addItem('6-7	 lat','pascalSelect')
			  ->addItem('8-9	 lat','pitagorasSelect')
			  ->addItem('10-11	 lat','eulerSelect')
			  ->addItem('12	 	 lat','gaussSelect');


//Fermat

$fermatSelect = new SelectBox('Lokalizacja','Wybierz lokalizację');
$fermatSelect->addItem('Ursynów		/al. KEN 95, kl. 18a','ursynowSelect')
			 ->addItem('Tarchomin		/ul. Myśliborska 98g','tarchominSelect')
			 ->addItem('Bemowo			/ul. Powst. Śląskich 108a','bemowoSelect')
			 ->addItem('Wilanów			/ul. Wandy Rutkiewicz 2','wilanowSelect')
			 ->addItem('Józefów			/ul. Zawiszy Czarnego 1 C','jozefowSelect')
			 ->addItem('Białołęka		/ul. Skarbka z Gór 142b','bialolekaSelect')
			 ->addItem('Ursus			/ul. gen. Sosnkowskiego 16','ursusSelect')
			 ->addItem('Kraków			/ul. Torfowa 4','krakowSelect')
			 ->addItem('Konstancin - Jeziorna			/ul. Sobieskiego 6','konstancinSelect')
			 ->addItem('Grodzisk			/ul. Westfala 3 ','grodziskSelect')
			 ->addItem('Słupsk			/ul. Filmowa 1','slupskSelect')
			 ->addItem('Józefosław			/ul. Julianowska 67A','jozefoslawSelect');

$jozefoslawSelect = new SelectBox('Data','Wybierz datę');
$jozefoslawSelect->addItem('05.09','jozefoslawFirstDateSelect');
//godziny slupsk
$jozefoslawFirstDateSelect = new SelectBox('Godzina','Wybierz godzinę');
$jozefoslawFirstDateSelect->addItem('11:00','jozefoslawFirstHourSelect')
						  ->addItem('13:00','jozefoslawSecondHourSelect');				 

$slupskSelect = new SelectBox('Data','Wybierz datę');
$slupskSelect->addItem('18.09','slupskFirstDateSelect');
//godziny slupsk
$slupskFirstDateSelect = new SelectBox('Godzina','Wybierz godzinę');
$slupskFirstDateSelect->addItem('16:00','slupskFirstHourSelect');	
			 
$ursynowSelect = new SelectBox('Data','Wybierz datę');
$ursynowSelect->addItem('08.09  BRAK WOLNYCH MIEJSC','ursynowFirstDateSelect')	
			  ->addItem('18.09  BRAK WOLNYCH MIEJSC','ursynowSecondDateSelect');	  

$tarchominSelect = new SelectBox('Data','Wybierz datę');
$tarchominSelect->addItem('09.09  BRAK WOLNYCH MIEJSC','tarchominFirstDateSelect');	

$bemowoSelect = new SelectBox('Data','Wybierz datę');
$bemowoSelect->addItem('11.09 BRAK WOLNYCH MIEJSC','bemowoFirstDateSelect');

$wilanowSelect = new SelectBox('Data','Wybierz datę');
$wilanowSelect->addItem('14.09','wilanowFirstDateSelect');	

$jozefowSelect = new SelectBox('Data','Wybierz datę');
$jozefowSelect->addItem('17.09 BRAK WOLNYCH MIEJSC','jozefowFirstDateSelect');	
			    	
$bialolekaSelect = new SelectBox('Data','Wybierz datę');
$bialolekaSelect->addItem('07.09','bialolekaFirstDateSelect');	

$ursusSelect = new SelectBox('Data','Wybierz datę');
$ursusSelect->addItem('13.09','ursusFirstDateSelect');	

$krakowSelect = new SelectBox('Data','Wybierz datę');
$krakowSelect->addItem('10.09 BRAK WOLNYCH MIEJSC','krakowFirstDateSelect')
			 ->addItem('12.09 BRAK WOLNYCH MIEJSC','krakowSecondDateSelect')
			 ->addItem('17.09','krakowthirdDateSelect');

			 
$konstancinSelect= new SelectBox('Data','Wybierz datę');
$konstancinSelect->addItem('12.09','konstancinFirstDateSelect');	

$grodziskSelect = new SelectBox('Data','Wybierz datę');
$grodziskSelect->addItem('19.09','grodziskFirstDateSelect');	

//godziny ursynow
$ursynowFirstDateSelect = new SelectBox('Godzina','Wybierz godzinę');
$ursynowFirstDateSelect->addItem('16:30 BRAK WOLNYCH MIEJSC','ursynowFirstHourSelect');

$ursynowSecondDateSelect = new SelectBox('Godzina','Wybierz godzinę');
$ursynowSecondDateSelect->addItem('18:20  BRAK WOLNYCH MIEJSC','ursynowSecondHourSelect');

//godziny tarchomin
$tarchominFirstDateSelect = new SelectBox('Godzina','Wybierz godzinę');
$tarchominFirstDateSelect->addItem('16:30  BRAK WOLNYCH MIEJSC','tarchominFirstHourSelect');

//godziny bemowo
$bemowoFirstDateSelect = new SelectBox('Godzina','Wybierz godzinę');
$bemowoFirstDateSelect->addItem('18:20 BRAK WOLNYCH MIEJSC','bemowoFirstHourSelect');

//godziny wilanow
$wilanowFirstDateSelect = new SelectBox('Godzina','Wybierz godzinę');
$wilanowFirstDateSelect->addItem('18:20','wilanowFirstHourSelect');

//godziny jozefow
$jozefowFirstDateSelect = new SelectBox('Godzina','Wybierz godzinę');
$jozefowFirstDateSelect->addItem('18:20  BRAK WOLNYCH MIEJSC','jozefowFirstHourSelect');

//godziny bialoleka
$bialolekaFirstDateSelect = new SelectBox('Godzina','Wybierz godzinę');
$bialolekaFirstDateSelect->addItem('18:20','bialolekaFirstHourSelect');
		    			    	
//godziny ursus
$ursusFirstDateSelect = new SelectBox('Godzina','Wybierz godzinę');
$ursusFirstDateSelect->addItem('10:00','ursusFirstHourSelect');

//godziny krakow
$krakowFirstDateSelect = new SelectBox('Godzina','Wybierz godzinę');
$krakowFirstDateSelect->addItem('18:20 BRAK WOLNYCH MIEJSC','krakowFirstHourSelect');

$krakowSecondDateSelect = new SelectBox('Godzina','Wybierz godzinę');
$krakowSecondDateSelect->addItem('09:30 BRAK WOLNYCH MIEJSC','krakowSecondHourSelect');

$krakowthirdDateSelect = new SelectBox('Godzina','Wybierz godzinę');
$krakowthirdDateSelect->addItem('18:20','krakowThirdHourSelect');


//godziny konstancin
$konstancinFirstDateSelect = new SelectBox('Godzina','Wybierz godzinę');
$konstancinFirstDateSelect->addItem('11:00','konstancinFirstHourSelect');

//godziny grodzisk
$grodziskFirstDateSelect = new SelectBox('Godzina','Wybierz godzinę');
$grodziskFirstDateSelect->addItem('11:00  BRAK WOLNYCH MIEJSC','grodziskFirstHourSelect')
						->addItem('12:15','grodziskSecoundHourSelect');

//PASCAL			 		    			    			    			    		  
			 
$pascalSelect = new SelectBox('Lokalizacja','Wybierz lokalizację');
$pascalSelect->addItem('Ursynów		/al. KEN 95, kl. 18a','ursynowPascalSelect')
			 ->addItem('Tarchomin		/ul. Myśliborska 98g','tarchominPascalSelect')
			 ->addItem('Bemowo			/ul. Powst. Śląskich 108a','bemowoPascalSelect')
			 ->addItem('Józefów			/ul. Zawiszy Czarnego 1 C','jozefowSelect')
			 ->addItem('Białołęka		/ul. Skarbka z Gór 142b','bialolekaPascalSelect')
			 ->addItem('Ursus			/ul. gen. Sosnkowskiego 16','ursusPascalSelect')
			 ->addItem('Kraków			/ul. Torfowa 4','krakowSelect')
			 ->addItem('Konstancin - Jeziorna			/ul. Sobieskiego 6','konstancinPascalSelect')
			 ->addItem('Grodzisk			/ul. Westfala 3 ','grodziskSelect')
			 ->addItem('Józefosław			/ul. Julianowska 67A','jozefoslawSelect');

$ursynowPascalSelect = new SelectBox('Data','Wybierz datę');
$ursynowPascalSelect->addItem('08.09 BRAK WOLNYCH MIEJSC','ursynowFirstDatePascalSelect')	
			  		->addItem('18.09 BRAK WOLNYCH MIEJSC','ursynowSecondDatePascalSelect');	  

$tarchominPascalSelect = new SelectBox('Data','Wybierz datę');
$tarchominPascalSelect->addItem('10.09 BRAK WOLNYCH MIEJSC','tarchominFirstDatePascalSelect');

$bemowoPascalSelect = new SelectBox('Data','Wybierz datę');
$bemowoPascalSelect->addItem('11.09 BRAK WOLNYCH MIEJSC','bemowoFirstDatePascalSelect');

$jozefowPascalSelect = new SelectBox('Data','Wybierz datę');
$jozefowPascalSelect->addItem('17.09','jozefowFirstDatePascalSelect');	

$bialolekaPascalSelect = new SelectBox('Data','Wybierz datę');
$bialolekaPascalSelect->addItem('07.09 BRAK WOLNYCH MIJESC','bialolekaFirstDatePascalSelect');

$ursusPascalSelect = new SelectBox('Data','Wybierz datę');
$ursusPascalSelect->addItem('13.09 BRAK WOLNYCH MIJESC','ursusFirstDatePascalSelect');	

$krakowPascalSelect = new SelectBox('Data','Wybierz datę');
$krakowPascalSelect->addItem('10.09','krakowFirstDatePascalSelect')
			 	   ->addItem('12.09','krakowSecondDatePascalSelect')
			 	   ->addItem('17.09','krakowthirdDatePascalSelect');

$konstancinPascalSelect= new SelectBox('Data','Wybierz datę');
$konstancinPascalSelect->addItem('12.09   BRAK WOLNYCH MIEJSC','konstancinFirstDatePascalSelect');	

$grodziskPascalSelect = new SelectBox('Data','Wybierz datę');
$grodziskPascalSelect->addItem('19.09','grodziskFirstDatePascalSelect');
				   
//godziny ursynow
$ursynowFirstDatePascalSelect = new SelectBox('Godzina','Wybierz godzinę');
$ursynowFirstDatePascalSelect->addItem('16:30  BRAK WOLNYCH MIEJSC','ursynowPascalFirstHourSelect');

$ursynowSecondDatePascalSelect = new SelectBox('Godzina','Wybierz godzinę');
$ursynowSecondDatePascalSelect->addItem('18:20 BRAK WOLNYCH MIEJSC','ursynowPascalSecondHourSelect');

//godziny tarchomin
$tarchominFirstDatePascalSelect = new SelectBox('Godzina','Wybierz godzinę');
$tarchominFirstDatePascalSelect->addItem('16:30  BRAK WOLNYCH MIEJSC','tarchominPascalFirstHourSelect');		    	

//godziny bemowo
$bemowoFirstDatePascalSelect = new SelectBox('Godzina','Wybierz godzinę');
$bemowoFirstDatePascalSelect->addItem('18:20  BRAK WOLNYCH MIEJSC','bemowoPascalFirstHourSelect');	

//godziny jozefow
$jozefowFirstDatePascalSelect = new SelectBox('Godzina','Wybierz godzinę');
$jozefowFirstDatePascalSelect->addItem('18:20','jozefowPascalFirstHourSelect');	

//godziny bialoleka
$bialolekaFirstDatePascalSelect = new SelectBox('Godzina','Wybierz godzinę');
$bialolekaFirstDatePascalSelect->addItem('18:20  BRAK WOLNYCH MIJESC','bialolekaPascalFirstHourSelect');	

//godziny ursus
$ursusFirstDatePascalSelect = new SelectBox('Godzina','Wybierz godzinę');
$ursusFirstDatePascalSelect->addItem('11:00  BRAK WOLNYCH MIJESC','ursusPascalFirstHourSelect');	

//godziny krakow
$krakowFirstDatePascalSelect = new SelectBox('Godzina','Wybierz godzinę');
$krakowFirstDatePascalSelect->addItem('18:20','krakowPascalFirstHourSelect');	


$krakowSecondDatePascalSelect = new SelectBox('Godzina','Wybierz godzinę');
$krakowSecondDatePascalSelect->addItem('09:30','krakowPascalSecondHourSelect');	


$krakowthirdDatePascalSelect = new SelectBox('Godzina','Wybierz godzinę');
$krakowthirdDatePascalSelect->addItem('18:20','krakowPascalThirdHourSelect');	

//godziny konstancin
$konstancinFirstDatePascalSelect = new SelectBox('Godzina','Wybierz godzinę');
$konstancinFirstDatePascalSelect->addItem('12:00  BRAK WOLNYCH MIEJSC','konstancinPascalFirstHourSelect');

//godziny grodzisk
$grodziskFirstDatePascalSelect = new SelectBox('Godzina','Wybierz godzinę');
$grodziskFirstDatePascalSelect->addItem('11:00','grodziskPascalFirstHourSelect');

//PITAGORAS
$pitagorasSelect = new SelectBox('Lokalizacja','Wybierz lokalizację');
$pitagorasSelect->addItem('Ursynów		/al. KEN 95, kl. 18a','ursynowPitagorasSelect')
			 ->addItem('Tarchomin		/ul. Myśliborska 98g','tarchominPitagorasSelect')
			 ->addItem('Bemowo			/ul. Powst. Śląskich 108a','bemowoPitagorasSelect')
			 ->addItem('Wilanów			/ul. Wandy Rutkiewicz 2','wilanowPitagorasSelect')
			 ->addItem('Józefów			/ul. Zawiszy Czarnego 1 C','jozefowSelect')
			 ->addItem('Białołęka		/ul. Skarbka z Gór 142b','bialolekaPitagorasSelect')
			 ->addItem('Ursus			/ul. gen. Sosnkowskiego 16','ursusPitagorasSelect')
			 ->addItem('Kraków			/ul. Torfowa 4','krakowSelect')
			 ->addItem('Konstancin - Jeziorna			/ul. Sobieskiego 6','konstancinPitagorasSelect')
			 ->addItem('Grodzisk			/ul. Westfala 3 ','grodziskSelect')
			 ->addItem('Słupsk			/ul. Filmowa 1','slupskPitagorasSelect')
			 ->addItem('Józefosław			/ul. Julianowska 67A','jozefoslawSelect');	

$slupskPitagorasSelect = new SelectBox('Data','Wybierz datę');
$slupskPitagorasSelect->addItem('18.09','slupskFirstDatePitagorasSelect');	
//godziny slupsk
$slupskFirstDatePitagorasSelect =new SelectBox('Godzina','Wybierz godzinę');
$slupskFirstDatePitagorasSelect->addItem('17:45','slupskPitagorasFirstHourSelect');
			 
$ursynowPitagorasSelect = new SelectBox('Data','Wybierz datę');
$ursynowPitagorasSelect->addItem('08.09 BRAK WOLNYCH MIEJSC','ursynowFirstDatePitagorasSelect')	
			  ->addItem('18.09 BRAK WOLNYCH MIEJSC','ursynowSecondDatePitagorasSelect');	  

$tarchominPitagorasSelect = new SelectBox('Data','Wybierz datę');
$tarchominPitagorasSelect->addItem('09.09  BRAK WOLNYCH MIEJSC','tarchominFirstDatePitagorasSelect');	
			    	

$bemowoPitagorasSelect = new SelectBox('Data','Wybierz datę');
$bemowoPitagorasSelect->addItem('16.09 BRAK WOLNYCH MIEJSC','bemowoFirstDatePitagorasSelect');	
			    

$wilanowPitagorasSelect = new SelectBox('Data','Wybierz datę');
$wilanowPitagorasSelect->addItem('15.09  BRAK WOLNYCH MIEJSC','wilanowFirstDatePitagorasSelect');
			    

$jozefowPitagorasSelect = new SelectBox('Data','Wybierz datę');
$jozefowPitagorasSelect->addItem('17.09','jozefowFirstDatePitagorasSelect');	

$bialolekaPitagorasSelect = new SelectBox('Data','Wybierz datę');
$bialolekaPitagorasSelect->addItem('07.09','bialolekaFirstDatePitagorasSelect');	

$ursusPitagorasSelect = new SelectBox('Data','Wybierz datę');
$ursusPitagorasSelect->addItem('13.09 BRAK WOLNYCH MIJESC - GRUPA DODATKOWA','ursusFirstDatePitagorasSelect');	
			    			    			    			 

$krakowPitagorasSelect = new SelectBox('Data','Wybierz datę');
$krakowPitagorasSelect->addItem('10.09','krakowFirstDatePitagorasSelect')
			 ->addItem('12.09','krakowSecondDatePitagorasSelect')
			 ->addItem('17.09','krakowthirdDatePitagorasSelect');	
			 
$konstancinPitagorasSelect= new SelectBox('Data','Wybierz datę');
$konstancinPitagorasSelect->addItem('12.09  BRAK WOLNYCH MIJESC','konstancinFirstDatePitagorasSelect');	

		 

//godziny Ursynow
$ursynowFirstDatePitagorasSelect =new SelectBox('Godzina','Wybierz godzinę');
$ursynowFirstDatePitagorasSelect->addItem('18:20 BRAK WOLNYCH MIEJSC','ursynowPitagorasFirstHourSelect');

$ursynowSecondDatePitagorasSelect =new SelectBox('Godzina','Wybierz godzinę');
$ursynowSecondDatePitagorasSelect->addItem('16:30  BRAK WOLNYCH MIEJSC','ursynowPitagorasSecondHourSelect');			 		    			    			    			    		  

//godziny Tarchomin
$tarchominFirstDatePitagorasSelect =new SelectBox('Godzina','Wybierz godzinę');
$tarchominFirstDatePitagorasSelect->addItem('18:20  BRAK WOLNYCH MIEJSC','tarchominPitagorasFirstHourSelect');

//godziny Bemowo
$bemowoFirstDatePitagorasSelect =new SelectBox('Godzina','Wybierz godzinę');
$bemowoFirstDatePitagorasSelect->addItem('18:20 BRAK WOLNYCH MIEJSC','bemowoPitagorasFirstHourSelect');

//godziny Wilanow
$wilanowFirstDatePitagorasSelect =new SelectBox('Godzina','Wybierz godzinę');
$wilanowFirstDatePitagorasSelect->addItem('18:20  BRAK WOLNYCH MIEJSC','wilanowPitagorasFirstHourSelect');

//godziny Jozefow
$jozefowFirstDatePitagorasSelect =new SelectBox('Godzina','Wybierz godzinę');
$jozefowFirstDatePitagorasSelect->addItem('18:20','jozefowPitagorasFirstHourSelect');

//godziny Bialoleka
$bialolekaFirstDatePitagorasSelect =new SelectBox('Godzina','Wybierz godzinę');
$bialolekaFirstDatePitagorasSelect->addItem('18:20','bialolekaPitagorasFirstHourSelect');

//godziny Ursus
$ursusFirstDatePitagorasSelect =new SelectBox('Godzina','Wybierz godzinę');
$ursusFirstDatePitagorasSelect->addItem('14:00 BRAK WOLNYCH MIJESC - GRUPA DODATKOWA','ursusPitagorasFirstHourSelect');

//godziny Krakow
$krakowFirstDatePitagorasSelect =new SelectBox('Godzina','Wybierz godzinę');
$krakowFirstDatePitagorasSelect->addItem('18:20','krakowPitagorasFirstHourSelect');

$krakowSecondDatePitagorasSelect =new SelectBox('Godzina','Wybierz godzinę');
$krakowSecondDatePitagorasSelect->addItem('09:30','krakowPitagorasSecondHourSelect');

$krakowthirdDatePitagorasSelect =new SelectBox('Godzina','Wybierz godzinę');
$krakowthirdDatePitagorasSelect->addItem('18:20','krakowPitagorasThirdHourSelect');

//godziny konstancin
$konstancinFirstDatePitagorasSelect = new SelectBox('Godzina','Wybierz godzinę');
$konstancinFirstDatePitagorasSelect->addItem('13:00  BRAK WOLNYCH MIJESC','konstancinPitagorasFirstHourSelect');



//Euler
$eulerSelect = new SelectBox('Lokalizacja','Wybierz lokalizację');
$eulerSelect->addItem('Józefów			/ul. Zawiszy Czarnego 1 C','jozefowSelect')
			 ->addItem('Białołęka		/ul. Skarbka z Gór 142b','bialolekaEulerSelect')
			 ->addItem('Ursus			/ul. gen. Sosnkowskiego 16','ursusEulerSelect')
			 ->addItem('Kraków			/ul. Torfowa 4','krakowSelect')
			 ->addItem('Konstancin - Jeziorna			/ul. Sobieskiego 6','konstancinEulerSelect')
			 ->addItem('Grodzisk			/ul. Westfala 3 ','grodziskSelect')
			 ->addItem('Józefosław			/ul. Julianowska 67A','jozefoslawSelect');		

$jozefowEulerSelect = new SelectBox('Data','Wybierz datę');
$jozefowEulerSelect->addItem('17.09','jozefowFirstDateEulerSelect');	

$bialolekaEulerSelect = new SelectBox('Data','Wybierz datę');
$bialolekaEulerSelect->addItem('07.09 BRAK WOLNYCH MIEJSC','bialolekaFirstDateEulerSelect');	

$ursusEulerSelect = new SelectBox('Data','Wybierz datę');
$ursusEulerSelect->addItem('13.09 BRAK WOLNYCH MIEJSC','ursusFirstDateEulerSelect');	
			    			    			    			 

$krakowEulerSelect = new SelectBox('Data','Wybierz datę');
$krakowEulerSelect->addItem('10.09','krakowFirstDateEulerSelect')
			 ->addItem('12.09','krakowSecondDateEulerSelect')
			 ->addItem('17.09','krakowthirdDateEulerSelect');
//Godziny Jozefow
$jozefowFirstDateEulerSelect = new SelectBox('Godzina','Wybierz godzinę');
$jozefowFirstDateEulerSelect->addItem('18:20','jozefowEulerFirstHourSelect');	

//Godziny Bialoleka
$bialolekaFirstDateEulerSelect = new SelectBox('Godzina','Wybierz godzinę');
$bialolekaFirstDateEulerSelect->addItem('18:20 BRAK WOLNYCH MIEJSC','bialolekaEulerFirstHourSelect');		 

//Godziny Ursus
$ursusFirstDateEulerSelect = new SelectBox('Godzina','Wybierz godzinę');
$ursusFirstDateEulerSelect->addItem('13:00 BRAK WOLNYCH MIEJSC','ursusEulerFirstHourSelect');	

//Godziny Krakow
$krakowFirstDateEulerSelect = new SelectBox('Godzina','Wybierz godzinę');
$krakowFirstDateEulerSelect->addItem('18:20','krakowEulerFirstHourSelect');	

$krakowSecondDateEulerSelect = new SelectBox('Godzina','Wybierz godzinę');
$krakowSecondDateEulerSelect->addItem('09:30','krakowEulerSecondHourSelect');	

$krakowthirdDateEulerSelect = new SelectBox('Godzina','Wybierz godzinę');
$krakowthirdDateEulerSelect->addItem('18:20','krakowEulerThirdHourSelect');	

$konstancinEulerSelect= new SelectBox('Data','Wybierz datę');
$konstancinEulerSelect->addItem('12.09  BRAK WOLNYCH MIEJSC','konstancinFirstDateEulerSelect');	

//godziny konstancin
$konstancinFirstDateEulerSelect = new SelectBox('Godzina','Wybierz godzinę');
$konstancinFirstDateEulerSelect->addItem('14:00  BRAK WOLNYCH MIEJSC','konstancinEulerFirstHourSelect');


//Gauss
$gaussSelect = new SelectBox('Lokalizacja','Wybierz lokalizację');
$gaussSelect->addItem('Józefów			/ul. Zawiszy Czarnego 1 C','jozefowSelect')
			 ->addItem('Białołęka		/ul. Skarbka z Gór 142b','bialolekaGaussSelect')
			 ->addItem('Ursus			/ul. gen. Sosnkowskiego 16','ursusGaussSelect')
			 ->addItem('Kraków			/ul. Torfowa 4','krakowSelect')
			 ->addItem('Grodzisk			/ul. Westfala 3 ','grodziskSelect')
			 ->addItem('Konstancin - Jeziorna			/ul. Sobieskiego 6','konstancinGaussSelect')
			 ->addItem('Józefosław			/ul. Julianowska 67A','jozefoslawSelect');
			 	


$jozefowGaussSelect = new SelectBox('Data','Wybierz datę');
$jozefowGaussSelect->addItem('17.09','jozefowFirstDateGaussSelect');	

$bialolekaGaussSelect = new SelectBox('Data','Wybierz datę');
$bialolekaGaussSelect->addItem('07.09','bialolekaFirstDateGaussSelect');	

$ursusGaussSelect = new SelectBox('Data','Wybierz datę');
$ursusGaussSelect->addItem('13.09','ursusFirstDateGaussSelect');	
			    			    			    			 

$krakowGaussSelect = new SelectBox('Data','Wybierz datę');
$krakowGaussSelect->addItem('10.09','krakowFirstDateGaussSelect')
			 ->addItem('12.09','krakowSecondDateGaussSelect')
			 ->addItem('17.09','krakowthirdDateGaussSelect');

//konstancin
$konstancinGaussSelect= new SelectBox('Data','Wybierz datę');
$konstancinGaussSelect->addItem('12.09','konstancinFirstDateGaussSelect');
//godziny konstancin
$konstancinFirstDateGaussSelect = new SelectBox('Godzina','Wybierz godzinę');
$konstancinFirstDateGaussSelect->addItem('15:00','konstancinGaussFirstHourSelect');	
			 
//Godziny Jozefow
$jozefowFirstDateGaussSelect = new SelectBox('Godzina','Wybierz godzinę');
$jozefowFirstDateGaussSelect->addItem('18:20','jozefowGaussFirstHourSelect');	

//Godziny Bialoleka
$bialolekaFirstDateGaussSelect = new SelectBox('Godzina','Wybierz godzinę');
$bialolekaFirstDateGaussSelect->addItem('18:20','bialolekaGaussFirstHourSelect');		 

//Godziny Ursus
//$ursusFirstDateGaussSelect = new SelectBox('Godzina','Wybierz godzinę');
//$ursusFirstDateGaussSelect->addItem('10:00','ursusGaussFirstHourSelect');	

//Godziny Krakow
$krakowFirstDateGaussSelect = new SelectBox('Godzina','Wybierz godzinę');
$krakowFirstDateGaussSelect->addItem('18:20','krakowGaussFirstHourSelect');	

$krakowSecondDateGaussSelect = new SelectBox('Godzina','Wybierz godzinę');
$krakowSecondDateGaussSelect->addItem('09:30','krakowGaussSecondHourSelect');	

$krakowthirdDateGaussSelect = new SelectBox('Godzina','Wybierz godzinę');
$krakowthirdDateGaussSelect->addItem('18:20','krakowGaussThirdHourSelect');				 				 		 	  

// Register all the select items in an array

$selects = array(
	'productSelect'		=> $productSelect,
	'fermatSelect'		=> $fermatSelect,
	'pascalSelect'		=> $pascalSelect,
	'pitagorasSelect'	=> $pitagorasSelect,
	'eulerSelect'		=> $eulerSelect,
	'gaussSelect'		=> $gaussSelect,
	'ursynowSelect'		=> $ursynowSelect,
	'tarchominSelect'	=> $tarchominSelect,
	'bemowoSelect'		=> $bemowoSelect,
	'wilanowSelect'		=> $wilanowSelect,
	'jozefowSelect'		=> $jozefowSelect,
	'bialolekaSelect'	=> $bialolekaSelect,
	'ursusSelect'       => $ursusSelect,
	'krakowSelect'      => $krakowSelect,
	'ursynowPascalSelect' 			 => $ursynowPascalSelect,
	'tarchominPascalSelect' 		 => $tarchominPascalSelect,
	'bemowoPascalSelect' 			 => $bemowoPascalSelect,
	'jozefowPascalSelect' 			 => $jozefowPascalSelect,
	'bialolekaPascalSelect' 		 => $bialolekaPascalSelect,
	'ursusPascalSelect' 			 => $ursusPascalSelect,
	'krakowPascalSelect' 			 => $krakowPascalSelect,
	'ursynowPitagorasSelect' 		 => $ursynowPitagorasSelect,
	'tarchominPitagorasSelect' 		 => $tarchominPitagorasSelect,
	'bemowoPitagorasSelect' 		 => $bemowoPitagorasSelect,
	'wilanowPitagorasSelect' 		 => $wilanowPitagorasSelect,
	'jozefowPitagorasSelect' 		 => $jozefowPitagorasSelect,
	'bialolekaPitagorasSelect' 		 => $bialolekaPitagorasSelect,
	'ursusPitagorasSelect' 			 => $ursusPitagorasSelect,
	'krakowPitagorasSelect' 		 => $krakowPitagorasSelect,
	'jozefowEulerSelect' 			 => $jozefowEulerSelect,
	'bialolekaEulerSelect' 			 => $bialolekaEulerSelect,
	'ursusEulerSelect' 				 => $ursusEulerSelect,
	'krakowEulerSelect' 			 => $krakowEulerSelect,
	'jozefowGaussSelect' 			 => $jozefowGaussSelect,
	'bialolekaGaussSelect' 			 => $bialolekaGaussSelect,
	'ursusGaussSelect' 				 => $ursusGaussSelect,
	'krakowGaussSelect' 		  	 => $krakowGaussSelect,
	'ursynowFirstDateSelect' 		 => $ursynowFirstDateSelect,
	'ursynowSecondDateSelect' 		 => $ursynowSecondDateSelect,
	'tarchominFirstDateSelect' 		 => $tarchominFirstDateSelect,
	'bemowoFirstDateSelect' 		 => $bemowoFirstDateSelect,
	'wilanowFirstDateSelect' 		 => $wilanowFirstDateSelect,
	'jozefowFirstDateSelect' 		 => $jozefowFirstDateSelect,
	'bialolekaFirstDateSelect' 		 => $bialolekaFirstDateSelect,
	'ursusFirstDateSelect' 			 => $ursusFirstDateSelect,
	'krakowFirstDateSelect' 		 => $krakowFirstDateSelect,
	'krakowSecondDateSelect' 		 => $krakowSecondDateSelect,
	'krakowthirdDateSelect' 		 => $krakowthirdDateSelect,
	'ursynowFirstDatePascalSelect'   => $ursynowFirstDatePascalSelect,
	'ursynowSecondDatePascalSelect'  => $ursynowSecondDatePascalSelect,
	'tarchominFirstDatePascalSelect' => $tarchominFirstDatePascalSelect,
	'bemowoFirstDatePascalSelect'    => $bemowoFirstDatePascalSelect,
	'jozefowFirstDatePascalSelect'   => $jozefowFirstDatePascalSelect,
	'bialolekaFirstDatePascalSelect' => $bialolekaFirstDatePascalSelect,
	'ursusFirstDatePascalSelect'   	 => $ursusFirstDatePascalSelect,
	'krakowFirstDatePascalSelect'    => $krakowFirstDatePascalSelect,
	'krakowSecondDatePascalSelect'   => $krakowSecondDatePascalSelect,
	'krakowthirdDatePascalSelect'    => $krakowthirdDatePascalSelect,
	'ursynowFirstDatePitagorasSelect'=> $ursynowFirstDatePitagorasSelect,
	'ursynowSecondDatePitagorasSelect' => $ursynowSecondDatePitagorasSelect,
	'tarchominFirstDatePitagorasSelect'=> $tarchominFirstDatePitagorasSelect,
	'bemowoFirstDatePitagorasSelect'   => $bemowoFirstDatePitagorasSelect,
	'wilanowFirstDatePitagorasSelect'  => $wilanowFirstDatePitagorasSelect,
	'jozefowFirstDatePitagorasSelect'  => $jozefowFirstDatePitagorasSelect,
	'bialolekaFirstDatePitagorasSelect'=> $bialolekaFirstDatePitagorasSelect,
	'ursusFirstDatePitagorasSelect'    => $ursusFirstDatePitagorasSelect,
	'krakowFirstDatePitagorasSelect'   => $krakowFirstDatePitagorasSelect,
	'krakowSecondDatePitagorasSelect'  => $krakowSecondDatePitagorasSelect,
	'krakowthirdDatePitagorasSelect'   => $krakowthirdDatePitagorasSelect,
	'jozefowFirstDateEulerSelect'	   => $jozefowFirstDateEulerSelect,
	'bialolekaFirstDateEulerSelect'	   => $bialolekaFirstDateEulerSelect,
	'ursusFirstDateEulerSelect'  	   => $ursusFirstDateEulerSelect,
	'krakowFirstDateEulerSelect'	   => $krakowFirstDateEulerSelect,
	'krakowSecondDateEulerSelect'	   => $krakowSecondDateEulerSelect,
	'krakowthirdDateEulerSelect'	   => $krakowthirdDateEulerSelect,
	'jozefowFirstDateGaussSelect'      => $jozefowFirstDateGaussSelect,
	'bialolekaFirstDateGaussSelect'	   => $bialolekaFirstDateGaussSelect,
	'ursusFirstDateGaussSelect'	       => $ursusFirstDateGaussSelect,
	'krakowFirstDateGaussSelect'	   => $krakowFirstDateGaussSelect,
	'krakowSecondDateGaussSelect'      => $krakowSecondDateGaussSelect,
	'krakowthirdDateGaussSelect'       => $krakowthirdDateGaussSelect,


'konstancinSelect' => $konstancinSelect,
'grodziskSelect' => $grodziskSelect,
'konstancinFirstDateSelect' => $konstancinFirstDateSelect,
'grodziskFirstDateSelect' => $grodziskFirstDateSelect,
'konstancinFirstHourSelect' => $konstancinFirstHourSelect,
'grodziskFirstHourSelect' => $grodziskFirstHourSelect,
	
'konstancinPascalSelect' => $konstancinPascalSelect,
'grodziskPascalSelect' => $grodziskPascalSelect,
'konstancinFirstDatePascalSelect' => $konstancinFirstDatePascalSelect,
'grodziskFirstDatePascalSelect' => $grodziskFirstDatePascalSelect,
'konstancinPascalFirstHourSelect' => $konstancinPascalFirstHourSelect,
'grodziskPascalFirstHourSelect' => $grodziskPascalFirstHourSelect,

'konstancinPitagorasSelect' => $konstancinPitagorasSelect,
'konstancinFirstDatePitagorasSelect' => $konstancinFirstDatePitagorasSelect,
'konstancinPitagorasFirstHourSelect' => $konstancinPitagorasFirstHourSelect,

'konstancinEulerSelect' => $konstancinEulerSelect,
'konstancinFirstDateEulerSelect' => $konstancinFirstDateEulerSelect,
'konstancinEulerFirstHourSelect' => $konstancinEulerFirstHourSelect,

'konstancinGaussSelect' => $konstancinGaussSelect,
'konstancinFirstDateGaussSelect' => $konstancinFirstDateGaussSelect,
'konstancinGaussFirstHourSelect' => $konstancinGaussFirstHourSelect,

'slupskSelect' => $slupskSelect,
'slupskFirstDateSelect' => $slupskFirstDateSelect,
'slupskFirstHourSelect' => $slupskFirstHourSelect,

'slupskPitagorasSelect' => $slupskPitagorasSelect,
'slupskFirstDatePitagorasSelect' => $slupskFirstDatePitagorasSelect,
'slupskPitagorasFirstHourSelect' => $slupskPitagorasFirstHourSelect,

'jozefoslawSelect' => $jozefoslawSelect,
'jozefoslawFirstDateSelect' => $jozefoslawFirstDateSelect,
'jozefoslawFirstHourSelect' => $jozefoslawFirstHourSelect,
'jozefoslawSecondHourSelect' => $jozefoslawSecondHourSelect,

);

// We look up this array and return a select object depending
// on the $_GET['key'] parameter passed by jQuery

// You can modify it to select results from a database instead

if(array_key_exists($_GET['key'],$selects)){
	header('Content-type: application/json');
	echo $selects[$_GET['key']]->toJSON();
}
else{
	header("HTTP/1.0 404 Not Found");
	header('Status: 404 Not Found');
}

?>