<?php namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
class UsersController extends Controller
{
    public function add(){
        $input = Input::json()->all();
     /*   dd($input);*/
        $user = User::findOrNew($input['id']);
        $user->name = $input['first_name'];
        $user->surname = $input['second_name'];
        $user->age = ($input['age'] == 0);
        $user->balls_points =  $input['balls_points'] ;
        $user->sudoku_points =  $input['sudoku_points'];
        $user->agent_points =  $input['agent_points'];
	$user->is_student=  $input['is_student'];

        if($user->save()){
            $response = [
                'code' => 200,
                'status' => 'succcess',
                'message' => 'ACC_CREATED',
                'data' => $user->id
            ];
            return $this->response($response);
        }
        else{
            $response = [
                'code' => 422,
                'status' => 'error',
                'message' => 'ACC_NOT_CREATED'
            ];
            return $this->response($response);
        }
    }

    public function get(){
        $users = User::all();
      return response()->json($users);

    }


    private function response($response){
        return response()->json($response, $response['code']);
    }
}
