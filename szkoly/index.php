<!-- get header -->
<?php $root = realpath($_SERVER["DOCUMENT_ROOT"]);
 require("$root/inc/header.php");?>

<!-- get sidebar-->
<?php require("$root/inc/sidebar.php");?>

    <!--school section -->
    <section id="szkoly" class="wrapper">
        <div class="container">
            <div class="section-header">
                <h2>
                    Szkoły i przedszkola
                </h2>
                <img src="/assets/images/border.png" alt="">
            </div><!-- end section-header -->
            <div id="school-info">
                <h5>W szkołach i przedszkolach prowadzimy  zajęcia w dwóch modelach:</h5>
                <p>
                    1. W ramach programu matematyki w edukacji wczesnoszkolnej, gdzie część godzin matematyki poświęcona jest na zajęcia Matplanety.
                    <br>
                    2. Jako zajęcia dodatkowe, które realizowane są po zajęciach szkolnych.
                    <br>
                    3. Jeśli są Państwo zainteresowani organizacją zajęć Matplanety w szkole lub przedszkolu, prosimy o kontakt z Dariuszem Pawelcem nr. tel. 730 880 780 lub z mailowo: biuro@matplaneta.pl
                </p>
                <br>
                <p class="underline">Prowadzimy zajęcia w:</p>
            </div><!-- end school info -->
            <!-- school list -->
            <div class="col12" id="school-div">
               <div class="col3 school-list">
                   <img src="/assets/images/image06.jpg" class="first-school-logo">
                   <p>
                       <span>
                           Polsko-Francuska Niepubliczna Szkoła Podstawowa La Fontaine
                       </span>
                       <br>
                       ul. Wandy Rutkiewicz 2, Warszawa - Wilanów
                   </p>
               </div><!-- end school-list -->
                <div class="col3 school-list">
                    <img src="/assets/images/image00.png" class="second-school-logo">
                    <p>
                       <span>
                           Społeczna Szkoła Podstawowa nr 1 im. Jana Nowaka-Jeziorańskiego STO
                       </span>
                        <br>
                        ul. Polinezyjska 10 a, Warszawa - Ursynów
                    </p>
                </div><!-- end school-list -->
                <div class="col3 school-list" id="school-break">
                    <img src="/assets/images/szkola-wilinowa.png" class="second-school-logo">
                    <p>
                       <span>
                           Szkoła Podstawowa nr  9 STO
                       </span>
                        <br>
                        ul. Wiolinowa 6, Warszawa - Ursynów
                    </p>
                </div><!-- end school-list -->
                <div class="col3 school-list">
                    <img src="/assets/images/logo.png" class="second-school-logo">
                    <p>
                       <span>
                           Szkoła Podstawowa nr 20 im. Jana Gutenberga Fundacji Szkolnej
                       </span>
                        <br>
                        ul. Obrzeźna 12 A, Warszawa - Służewiec
                    </p>
                </div><!-- end school-list -->
				<div class="col3 school-list">
                    <img src="/assets/images/image02.png" class="first-school-logo">
                    <p>
                        <span>
                            Niepubliczna Szkoła Podstawowa "Logica"
                        </span>
                        <br>
                         ul. Skarbka z Gór 142 B, Warszawa - Białołęka
                    </p>
                </div><!-- end school-list -->
                <!--<div class="col3 school-list">
                    <img src="/assets/images/image05.jpg" class="first-school-logo">
                    <p>
                       <span>
                           BABY CITY Niepubliczny Żłobek i Europejskie Przedszkole Ekologiczne
                       </span>
                        <br>
                        ul. Zielona 42a, Warszawa - Sadyba
                    </p>
                </div> end school-list -->
                <div class="col3 school-list">
                    <img src="/assets/images/image02.png" class="first-school-logo" style="visibility:hidden">
                    <p>
                       <span>
                           Szkoła Przyszłości Niepubliczna Szkoła Podstawowa
                       </span>
                        <br>
                        ul. Białołęcka 186 D, Warszawa – Białołęka
                    </p>
                </div><!-- end school-list -->
                <div class="col3 school-list">
                    <img src="/assets/images/image03.png" class="second-school-logo">
                    <p>
                       <span>
                           Francusko-Polskie Niepubliczne Przedszkole "LA FONTAINE”
                       </span>
                        <br>
                        ul. Rolna 177, Warszawa - Mokotów
                    </p>
                </div><!-- end school-list -->
                <div class="col3 school-list">
                    <img src="/assets/images/image03.png" class="second-school-logo">
                    <p>
                       <span>
                           Francusko-Polskie Niepubliczne Przedszkole „LA FONTAINE”
                       </span>
                        <br>
                        ul. Dygasińskiego 1, Warszawa - Żoliborz
                    </p>
                </div><!-- end school-list -->			
				
				<div class="col3 school-list">
                    <img src="/assets/images/primous.png" class="second-school-logo">
                    <p>
                        <span>
                            Prywatne Przedszkole Dwujęzyczne Primrose
                        </span>
                        <br>
                        ul. Bernardyńska 16A, Warszawa - Mokotów
                    </p>
                </div><!-- end school-list -->
									<div class="col3 school-list">
                    <img src="/assets/images/primus.png" class="second-school-logo">
                    <p>
                        <span>
                            Niepubliczna Szkoła Podstawowa nr 47 Fundacji Primus
                        </span>
                        <br>
                        ul. Pułkownika Zoltána Baló 102, Warszawa-Ursynów
                    </p>
                </div><!-- end school-list -->
				
				
				
                <div class="col3 school-list">
                    <img src="/assets/images/image05.jpg" class="first-school-logo">
                    <p>
                       <span>
                           BABY CITY Niepubliczny Żłobek i Europejskie Przedszkole Ekologiczne
                       </span>
                        <br>
                        ul. Zielona 42a, Warszawa - Sadyba
                    </p>
                </div><!-- end school-list -->
                <div class="col3 school-list" id="school-break-second">
                    <img src="/assets/images/image07.png" class="first-school-logo">
                    <p>
                       <span>
                           Wyspa Skarbów Przedszkole Niepubliczne
                       </span>
                        <br>
                        ul. Skarbka z Gór 142B, Warszawa - Białołęka
                    </p>
                </div><!-- end school-list -->
				<div class="col3 school-list" >
                    <img src="/assets/images/szkola-tagowek-torunska.gif" class="first-school-logo">
                    <p>
                        <span>
                            Społeczna Szkoła Podstawowa nr 11 STO
                        </span>
                        <br>
                        ul. Toruńska 23, Warszawa - Targówek
                    </p>
                </div><!-- end school-list -->
				<div class="col3 school-list" >
                    <img src="/assets/images/przedszkole-ursynow-dereniowa.png" class="first-school-logo">
                    <p>
                        <span>
                            Dwujęzyczne Przedszkole Sportowe "Mali Odkrywcy"
                        </span>
                        <br>
                       ul. Dereniowa 11, Warszawa - Ursynów
                    </p>
                </div><!-- end school-list -->
				<div class="col3 school-list" >
                    <img src="/assets/images/przedszkole-centrum-krochmalna.jpg" class="first-school-logo">
                    <p>
                        <span>
                            Academy International Centrum Dwujęzyczne Przedszkole i Żłobek
                        </span>
                        <br>
                        ul. Krochmalna 3, Warszawa - Centrum
                    </p>
                </div><!-- end school-list -->

 
				               <div class="col3 school-list">
                    <img src="/assets/images/image07.png" class="first-school-logo" style="visibility: hidden">
                    <p>
                        <span>
                            Waldorfska Szkoła Podstawowa i Gimnazjum im. Janusza Korczaka 
                        </span>
                        <br>
                        ul. Kazimierza Wielkiego 33, Kraków
                    </p>
                </div><!-- end school-list -->

				
                <div class="col3 school-list" id="school-break">
                    <img src="/assets/images/image07.png" class="first-school-logo" style="visibility: hidden">
                    <p>
                        <span>
                            Przedszkole Żyrafa
                        </span>
                        <br>
                        ul. Henryka Rodakowskiego 15, Kraków
                    </p>
                </div><!-- end school-list -->
								<div class="col3 school-list" >
                    <img src="/assets/images/image07.png" class="first-school-logo" style="visibility: hidden">
                    <p>
                        <span>
                            Przedszkole Groszki
                        </span>
                        <br>
                        ul.Czerwonego Krzyża 25a, Żary
                    </p>
                </div><!-- end school-list -->
				<div class="col3 school-list" >
                    <img src="/assets/images/przedszkole-goodstart.png" class="first-school-logo">
                    <p>
                        <span>
                            Przedszkole anglojęzyczne Goodstart
                        </span>
                        <br>
                        ul. Długa 7, Konstancin-Jeziorna
                    </p>
                </div><!-- end school-list -->
				<div class="col3 school-list" >
                    <img src="/assets/images/przedszkole-basniowy-dom.png" class="first-school-logo">
                    <p>
                        <span>
                            Przedszkole Językowe Baśniowy Dom
                        </span>
                        <br>
                        ul. Miłobędzka 2, Warszawa - Mokotów
                    </p>
                </div><!-- end school-list -->
				
                <!-- <div class="col3 school-list">
                    <img src="/assets/images/image02.png" class="first-school-logo">
                    <p>
                        <span>
                            Niepubliczna Szkoła Podstawowa "Logica"
                        </span>
                        <br>
                         ul. Skarbka z Gór 142 B, Warszawa - Białołęka
                    </p>
                </div> end school-list -->
            </div><!-- end col12 -->
        </div><!-- end container -->
    </section><!-- end szkoly -->

<!-- get footer -->
<?php require("$root/inc/footer.php");?>
