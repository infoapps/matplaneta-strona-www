Node.prototype.insertAfter = function(newNode) {
    if(this.nextSibling) { //jezeli dany element ma za soba jakis obiekt
        return this.parentNode.insertBefore(newNode, this.nextSibling); //to wstawiamy przed tym obiektem nasz element
    } else {
        return this.parentNode.appendChild(newNode); //inaczej
    }
}
 
Node.prototype.createError = function(msg, cl) {
    var cl = cl || 'pole-z-errorem'
        if (this.nextSibling && (this.nextSibling.nodeName.toLowerCase()=='span' && this.nextSibling.className.indexOf(cl)!=-1)) return false;
        var spanError = document.createElement('span');
        spanError.appendChild(document.createTextNode(msg));
        spanError.className = cl;
        this.insertAfter(spanError);
}
 
Node.prototype.removeError = function(cl) {
    var cl = cl || 'pole-z-errorem';
    if (this.nextSibling && (this.nextSibling.nodeName.toLowerCase()=='span' && this.nextSibling.className.indexOf(cl)!=-1)) {
        this.parentNode.removeChild(this.nextSibling);
    }
}

function validateEmail(email){
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email.value);
}

function validateTelephone(telephone){
	var regex = /^[0-9-+]+$/;
    return regex.test(telephone.value);
}

function validateLength(name, length){
	return name.value.length > length;
}
 
function test(element, type) {
    if (element.value.length < 3) {
        element.createError('Błędna wartość');
        return false;
    } else {
        element.removeError();
        return true;
    }
}
function validateforma(rata,calosc){
    return rata.checked || calosc.checked;
}
function validateZaplata(gotowka, przelew){
	return gotowka.checked || przelew.checked;
}

function validateRadioButton(radioButton){
	return radioButton.checked;
}
 
 window.onload = function(){
	var poleA = document.getElementById('nazwiskodzieckaform');
    var poleB = document.getElementById('nazwiskorodzicaform');
    var poleEmail = document.getElementById('emailform');
	var forma =document.getElementById('forma-label');
	var gotowka = document.getElementById('forma');
	var przelew = document.getElementById('forma-2');
	var regulamin=document.getElementById('regulamin');
	var akceptacja = document.getElementById('akceptacja');
	var zgoda = document.getElementById('zgoda');
	var data =document.getElementById('wiek');
        var adres=document.getElementById('adresform');

	//poleA.onblur = function() {
    //test(this, "length");
//}
var telephone = document.getElementById('telefonform');
	//telephone.onblur = function() {
    //test(this, "telephone");
//}
/*$(document).ready(  
    function()  
    {  
        $("form#form").submit (  
            function()  
            {  
                    if($('.platnosc').is(":checked") == false)  
                    {  
                            alert("Proszę wybrać przynajmniej jeden element !");  
                    return false;  
                    }  
                return true;  
                }  
            );      
    }  
);  */
function validateAllForm(){
	var poprNazw = validateLength(poleA, 3);
    var poprNazw1 = validateLength(poleB, 3);
	var poprTelefon = validateTelephone(telephone);
	var poprpoleEmail=validateEmail(poleEmail);
	var poprData=validateLength(data, 3);
	var poprawnaZaplata = validateZaplata(gotowka, przelew);
	var poprawnyRegulamin = validateRadioButton(akceptacja);
	var poprawnaZgoda = validateRadioButton(zgoda);
	var poprAdres=validateLength(adres, 3);
       
        if(!poprAdres){
		adres.createError("Bład! Wpisz poprawny adres! ");
	}else{
		adres.removeError();
	}
        if(!poprData){
		data.createError("Bład! Wpisz poprawną datę! ");
	}else{
		data.removeError();
	}
	if(!poprawnaZgoda){
		zgoda.createError("Bład! Wyraź zgode! ");
	}else{
		zgoda.removeError();
	}
	if(!poprawnyRegulamin){
		regulamin.createError("Bład! Zaakceptuj regulamin! ");
	}else{
		regulamin.removeError();
	}
	if(!poprpoleEmail){
		poleEmail.createError("Bład! Wpisz email! ");
	}else{
		poleEmail.removeError();
	}
	if(!poprNazw){
		poleA.createError("Bład! Wpisz Imię i nazwisko dziecka! ");
	}else{
		poleA.removeError();
	}
	if(!poprNazw1){
		poleB.createError("Bład! Wpisz Imię i nazwisko rodzica!");
	}else{
		poleB.removeError();
	}
	if(!poprTelefon){
		telephone.createError("Bład! Błedny telefon!");
	}else{
		telephone.removeError();
	}
	if(!poprawnaZaplata){
		forma.createError("Bład! Wybierz formę płatności!");
	}else{
		forma.removeError();
	}
	
	return   poprNazw && poprNazw1 && poprTelefon&& poprAdres && poprpoleEmail && poprData && poprawnaZaplata && poprawnyRegulamin && poprawnaZgoda;
}

var sendForm = document.getElementById('submit');
	sendForm.onclick = function(event){
		if(!validateAllForm()){
			alert("Wystąpiły błedy");
			event.preventDefault();
		}
		
	};
 };
 
