<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta charset="UTF-8">
    <title>Matplaneta - zajęcia pozaszkolne z zakresu matematyki i programowania komputerowego</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="InfoApps">
    <meta name="description" content="Matplaneta to instytucja edukacyjna, która prowadzi zajęcia pozaszkolne z zakresu matematyki i programowania komputerowego. Zajęcia przeznaczone są dla dziecie w wieku od 5 do 14 lat i mają na celu zwiększenie ich zdolności rozwiązywania problemów.">
    <meta name="keywords" content="koło matematyczne, matematyka, math circle, matplaneta, programowanie komputerów, warszawa, zajęcia dla dzieci, zajęcia komputerowe, zajęcia z matematyki">
    <link type="text/css" href="../css/main.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
        		<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-5445667-4', 'auto');
ga('send', 'pageview');

</script>
</head>

<body>
    <div id="mobile-nav">
        <div id="phone-logo">
            <a href="../index.html" title="Matplaneta, al. KEN 95, Warszawa"> <img src="../assets/images/matplaneta-phone.png" alt="Matplaneta zajęcia pozaszkolne"> </a>
        </div>
        <div id="menu-button"> <img src="../assets/images/navmenu.png" alt="Phone navigation button"> </div>
        <ul id="menu-list" style="display: none">
            <li> <a href="../index.html#about-section">O nas</a> </li>
            <li> <a href="../index.html#news-section">News</a> </li>
            <li> <a href="../index.html#lesson-section">Zajęcia</a> </li>
            <li> <a href="../index.html#gallery-section">Galeria</a> </li>
            <li> <a href="../index.html#location-section">Lokalizacje</a> </li>
            <li> <a href="../index.html#work-section">Praca</a> </li>
            <li> <a href="../index.html#contact-section">Kontakt</a> </li>
            <li> <a href="../zapisy.html">Zapisy</a> </li>
        </ul>
    </div>
    <div id="side-bar">
        <div id="matplaneta-logo">
            <a href="../index.html" title="Matplaneta, al. KEN 95, Warszawa"> <img src="../assets/images/matplaneta.jpg" alt="Matplaneta zajęcia pozaszkolne"> </a>
        </div>
        <nav id="main-nav">
            <ul>
                <li> <a href="../index.html">Start</a> </li>
                <li> <a href="../index.html#about-section">O nas</a> </li>
                <li> <a href="../index.html#news-section">Aktualności</a> </li>
                <li> <a href="../index.html#lesson-section">Zajęcia</a> </li>
                <li> <a href="../index.html#gallery-section">Galeria</a> </li>
                <li> <a href="../index.html#location-section">Lokalizacje</a> </li>
                <li> <a href="../index.html#work-section">Praca</a> </li>
                <li> <a href="../index.html#contact-section">Kontakt</a> </li>
            </ul>
        </nav>
        <div id="menu-buttons"> <a href="../zapisy.html">Zapisy</a> </div>
    </div>
    <div id="main-site">
       		<section id="form-message">
			<div id="message" class="container">
				<h1>Dziękujemy za  zapisanie dziecka na  zajęcia! Za chwile dostaniesz maila z potwierdzeniem.</h1>
				<a class="back-button" href="../zapisy.html">Wróć</a>
			</div>
		</section>
        <footer>
            <div id="main-footer" class="wrapper">
                <div class="main-border"></div>
                <div class="container">
                    <div class="col6 main-links">
                        <h6> Kontakt </h6>
                        <ul>
                            <li> <span class="icon-home"></span> <span>Al. Komisji Edukacji Narodowej 95, klatka 18a, 2 piętro<br> (obok stacji metra STOKŁOSY)</span> </li>
                            <li> <span class="icon-phone"></span> <span>+48 22 100 53 47</span> </li>
                            <li> <span class="icon-envelope"></span> <a href="mailto:biuro@e-matplaneta.pl">biuro@e-matplaneta.pl</a> </li>
                            <li> <span class="icon-earth"></span> <a href="../index.html">www.matplaneta.pl</a> </li>
                        </ul>
                    </div>
                    <div class="col6 footer-links">
                        <h6> Lokalizacje </h6>
                        <ul>
                            <li> <a href="../ursynow.html">Ursynów</a> </li>
                            <li> <a href="../bemowo.html">Bemowo</a> </li>
                            <li> <a href="../tarchomin.html">Tarchomin</a> </li>
                            <li> <a href="../jozefow.html">Józefów</a> </li>
                            <li> <a href="../jozefoslaw.html">Józefosław</a> </li>
                            <li> <a href="../zary.html">Żary</a> </li>
                            <li> <a href="../krakow.html">Kraków</a> </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="sub-footer">
                <div class="container">
                    <div class="col4"> <span id="copyright">Copyright © 2014 InfoApps</span> </div>
                    <div id="footer-nav" class="col8 footer-links">
                        <ul>
                            <li> <a href="../index.html#about-section">O nas</a> </li>
                            <li> <a href="../index.html#news-section">News</a> </li>
                            <li> <a href="../index.html#lesson-section">Zajęcia</a> </li>
                            <li> <a href="../index.html#gallery-section">Galeria</a> </li>
                            <li> <a href="../index.html#location-section">Lokalizacje</a> </li>
                            <li> <a href="../index.html#work-section">Praca</a> </li>
                            <li> <a href="../index.html#contact-section">Kontakt</a> </li>
                            <li> <a href="../zapisy.html">Zapisy</a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script src="../scripts/sub-page-script.js" type="text/javascript"></script>
</body>

</html>