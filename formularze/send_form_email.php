<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 2015-07-14
 * Time: 15:25
 */

if(isset($_POST['email'])){

    $email_to = "michal.ryciak@gmail.com";
    $email_subcjet = "zapisy na dzień otwarty";

    //error message

    function died($error) {
        echo "Przepraszamy, źle wypełniłeś formularz.";
        echo $error."<br /><br />";
        echo "Wróc i popraw swój formularz";

        die();
    }

    //validation

    if(!isset($_POST['first_name']) ||

        !isset($_POST['last_name']) ||

        !isset($_POST['email']) ||

        !isset($_POST['phone']) ||

        !isset($_POST['age']) ||

        !isset($_POST['hour']) ||

        !isset($_POST['calendar'])) {

        died('Przepraszamy źle wypełniłeś formularz.');

    }

    $imię_dziecka = $_POST['first_name']; // required
    $nazwisko_dziecka = $_POST['last_name']; // required
    $email_from = $_POST['email']; // required
    $telefon = $_POST['phone']; //required
    $wiek_dziecka = $_POST['age']; //required
    $godzina = $_POST['hour']; //required
    $data = $_POST['calendar']; //required

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
    if(!preg_match($email_exp,$email_from)) {
        $error_message .= 'Podany adres email jest nieprawidłowy.<br />';
    }

    $string_exp = "/^[A-Za-z .'-]+$/";
    if(!preg_match($string_exp,$imię_dziecka)) {
        $error_message .= 'Imię Twojego dziecka jest nieprawidłowe. <br />';
    }

    if(!preg_match($string_exp,$nazwisko_dziecka)) {
        $error_message .= 'Nazwisko Twoje dziecka jest niepoprawne. <br />';
    }

    if(strlen($error_message) > 0) {
        died($error_message);
    }

    $email_message = "Form details below.\n\n";



    function clean_string($string) {

        $bad = array("content-type","bcc:","to:","cc:","href");

        return str_replace($bad,"",$string);

    }


    $email_message .= "Imię dziecka: ".clean_string($imię_dziecka)."\n";

    $email_message .= "Nazwisko dziecka: ".clean_string($nazwisko_dziecka)."\n";

    $email_message .= "Email: ".clean_string($email_from)."\n";

    $email_message .= "Telefon: ".clean_string($telefon)."\n";

    $email_message .= "Wiek dziecka: ".clean_string($wiek_dziecka)."\n";

    $email_message .= "godzina: ".clean_string($godzina)."\n";

    $email_message .= "data: ".clean_string($data)."\n";


    //create email headers

    $headers = 'From: '.$email_from."\r\n".
        'Reply-To: '.$email_from."\r\n" .
        'X-Mailer: PHP/' . phpversion();

    @mail($email_to, $email_subcjet, $email_message, $headers);
    ?>



    <!-- include your own success html here -->



    Dziękujemy za zapis na dzień otwarty z w matplanecie!



<?php

}

?>