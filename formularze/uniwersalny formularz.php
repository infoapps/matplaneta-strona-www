<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta charset="UTF-8">
    <title>Matplaneta - zajęcia pozaszkolne z zakresu matematyki i programowania komputerowego</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="InfoApps">
    <meta name="description" content="Matplaneta to instytucja edukacyjna, która prowadzi zajęcia pozaszkolne z zakresu matematyki i programowania komputerowego. Zajęcia przeznaczone są dla dziecie w wieku od 5 do 14 lat i mają na celu zwiększenie ich zdolności rozwiązywania problemów.">
    <meta name="keywords" content="koło matematyczne, matematyka, math circle, matplaneta, programowanie komputerów, warszawa, zajęcia dla dzieci, zajęcia komputerowe, zajęcia z matematyki">
    <link type="text/css" href="../css/main.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-5445667-4', 'auto');
        ga('send', 'pageview');

    </script>
</head>

<body>
<div id="mobile-nav">
    <div id="phone-logo">
        <a href="../index.html" title="Matplaneta, al. KEN 95, Warszawa"> <img src="../assets/images/matplaneta-phone.png" alt="Matplaneta zajęcia pozaszkolne"> </a>
    </div>
    <div id="menu-button"> <img src="../assets/images/navmenu.png" alt="Phone navigation button"> </div>
    <ul id="menu-list" style="display: none">
        <li> <a href="../index.html#about-section">O nas</a> </li>
        <li> <a href="../index.html#news-section">News</a> </li>
        <li> <a href="../index.html#lesson-section">Zajęcia</a> </li>
        <li> <a href="../index.html#gallery-section">Galeria</a> </li>
        <li> <a href="../index.html#location-section">Lokalizacje</a> </li>
        <li> <a href="../index.html#work-section">Praca</a> </li>
        <li> <a href="../index.html#contact-section">Kontakt</a> </li>
        <li> <a href="../zapisy.html">Zapisy</a> </li>
    </ul>
</div>
<div id="side-bar">
    <div id="matplaneta-logo">
        <a href="../index.html" title="Matplaneta, al. KEN 95, Warszawa"> <img src="../assets/images/matplaneta.jpg" alt="Matplaneta zajęcia pozaszkolne"> </a>
    </div>
    <nav id="main-nav">
        <ul>
            <li> <a href="../index.html">Start</a> </li>
            <li> <a href="../index.html#about-section">O nas</a> </li>
            <li> <a href="../index.html#news-section">Aktualności</a> </li>
            <li> <a href="../index.html#lesson-section">Zajęcia</a> </li>
            <li> <a href="../index.html#gallery-section">Galeria</a> </li>
            <li> <a href="../index.html#location-section">Lokalizacje</a> </li>
            <li> <a href="../index.html#work-section">Praca</a> </li>
            <li> <a href="../index.html#contact-section">Kontakt</a> </li>
        </ul>
    </nav>
    <div id="menu-buttons"> <a href="../zapisy.html">Zapisy i ceny</a> </div>
</div>
<div id="main-site">
    <section id="formularz" class="wrapper">
        <div class="section-header">
            <h2>Warszawa Bemowo</h2> <img src="../assets/images/border.png" alt="border"> </div>
        <div class="container">
            <div class="regulamin">
                <h5>Lubimy, kiedy rzeczy są proste. Tak też ustalamy nasze ceny.</h5>
                <p>Cena obejmuje dwugodzinne spotkania 1 raz w tygodniu (2 godziny lekcyjne) przez okres jednego semestru. </p>
                <p>W semestrze odbywa się 17 spotkań (34 lekcje) w przypadku zajęć z Matematyki i Programowania komputerów oraz 10 spotkań (20 lekcji) w przypadku Programowania robotów.</p>
                <h5>Matematyka, Programowanie komputerów, Programowanie Robotów Lego Mindstorms EV3 - opłata za jeden semestr</h5>
                <p>- 980 zł w przypadku jednorazowej opłaty lub 5 rat po 199 zł</p>
                <h5>Programowanie Robotów Lego WeDo - opłata za jeden semestr:</h5>
                <p>- 580 zł w przypadku jednorazowej opłaty lub 3 raty po 199 zł</p>
                <h5>Płatności za zajęcia można wnosić gotówką w Centrum Edukacyjnym lub przelewem na konto:</h5>
                <p>Bank: IDEA BANK S.A. Nr konta: 38 1950 0001 2006 0889 8214 0002 Odbiorca: Matplaneta SA ul. Targowa 20 A, 03-731 Warszawa, NIP 113 287 75 16 W tytule przelewu prosimy o wpisanie: lokalizacja, grupa, dzień, imię i nazwisko dziecka.</p>
                <h5>Bezpieczne zapisy.</h5>
                <p>Oferujemy możliwość rezygnacji z zajęć po pierwszych dwóch spotkaniach za pełnym zwrotem wniesionych opłat.</p>
                <p>Rabat za rodzeństwo i za polecenie 15%</p>
                <p>Więcej o cenach znajdziesz <a class="program" href="../assets/download/zapisy_i_ceny.pdf" target="_blank">tutaj.</a> </p>
                <p>Regulamin Świadczenia Usług Edukacyjnych dostępny jest <a class="program" href="../assets/download/regulamin_swiadczenia_uslug_edukacyjnych.pdf" target="_blank">tutaj.</a> </p>
            </div>
            <form id="form" enctype="application/x-www-form-urlencoded" action="skrypt5.php" method="post">
                <dl class="zend_form"><dt id="form-label">&nbsp;</dt>
                    <dd id="form-element">
                        <fieldset id="fieldset-form">
                            <legend>Formularz uczestnictwa w zajęciach - Warszawa Bemowo</legend>
                            <dl>
                                <dd id="nazwiskodzieckaform-label">
                                    <label for="nazwiskodzieckaform" class="label required">Imię i nazwisko dziecka *</label>
                                </dd>
                                <dd id="nazwiskodzieckaform-element">
                                    <input type="text" name="nazwiskodzieckaform" id="nazwiskodzieckaform" value="" size="50" class="text ui-corner-all">
                                </dd>
                                <dd id="nazwiskorodzicaform-label">
                                    <label for="nazwiskorodzicaform" class="label required">Imię i nazwisko rodzica *</label>
                                </dd>
                                <dd id="nazwiskorodzicaform-element">
                                    <input type="text" name="nazwiskorodzicaform" id="nazwiskorodzicaform" value="" size="100" class="text ui-corner-all">
                                </dd>
                                <dd id="wiek-label">
                                    <label for="wiek" class="label required">Data urodzenia dziecka *</label>
                                </dd>
                                <dd id="wiek-element">
                                    <input type="text" name="wiek" id="wiek" value="" size="100" class="text ui-corner-all hasDatepicker">
                                </dd>
                                <dd id="telefonform-label">
                                    <label for="telefonform" class="label required">Telefon kontaktowy rodzica *</label>
                                </dd>
                                <dd id="telefonform-element">
                                    <input type="text" name="telefonform" id="telefonform" value="" size="50" class="text ui-corner-all">
                                </dd>
                                <dd id="emailform-label">
                                    <label for="emailform" class="label required">Adres E-mail *</label>
                                </dd>
                                <dd id="emailform-element">
                                    <input type="text" name="emailform" id="emailform" value="" class="text ui-corner-all" size="30">
                                </dd>
                                <dd id="adresform-label">
                                    <label for="adresform" class="label required">Adres zamieszkania *</label>
                                </dd>
                                <dd id="adresform-element">
                                    <textarea name="adresform" id="adresform" cols="40" class="text ui-corner-all" style="height:80px" rows="10"></textarea>
                                </dd>
                            </dl>
                        </fieldset>
                    </dd><dt id="matematyka-label">&nbsp;</dt>
                    <dd id="matematyka-element">
                        <fieldset id="fieldset-matematyka">
                            <legend>Grupa zajęć Matematyka</legend>
                            <dl>
                                <div class="checkbox-wrap">
                                    <label for="fermat" class="optional">Fermat (4-5 lat) I rok</label>
                                    <label>
                                        <input type="checkbox" name="fermat" id="fermat-Wtorek1630Powstańców Śląskich" value="Wtorek 16:30 (Powstańców Śląskich)">Wtorek: 16:30 (Powstańców Śląskich) </label>
                                    <label>
                                        <input type="checkbox" name="fermat" id="fermat-Czwartek1630Bolkowska" value="Czwartek 16:30 (Bolkowska)">Czwartek: 16:30 (Bolkowska) </label>

                                    <label>
                                        <input type="checkbox" name="fermat" id="fermat-Sobota0900Powstańców Śląskich" value="Sobota 09:00 (Powstańców Śląskich)">Sobota: 09:00 (Powstańców Śląskich) </label>
                                </div>
                                <div class="checkbox-wrap">
                                    <label for="pascal" class="optional">Pascal (6-7 lat) I rok</label>
                                    <label>
                                        <input type="checkbox" name="pascal" id="pascal-Poniedziałek1820Bolkowska" value="Poniedziałek 18.20 (Bolkowska)">Poniedziałek 18.20 (Bolkowska)</label>
                                    <label>
                                        <input type="checkbox" name="pascal" id="pascal-Czwartek16300Powstańców Śląskich" value="Piątek 16.30 (Powstanców Śląskich)">Piątek 16.30 (Powstańców Śląskich)</label>
                                    <label>
                                        <input type="checkbox" name="pascal" id="pascal-Piątek16300Bolkowska" value="Piątek 16.30 (Bolkowska)">Piątek 16.30 (Bolkowska)</label>
                                    <label>
                                        <input type="checkbox" name="pascal" id="pascal-Sobota0900Bolkowska" value="Sobota 09.00 (Bolkowska)">Sobota 09.00 (Bolkowska)</label>
                                </div>
                                <div class="checkbox-wrap">
                                    <label for="pascal2" class="optional">Pascal (6-7 lat) II rok</label>
                                    <label>
                                        <input type="checkbox" name="pascal2" id="pascal2-Środa1630Powstańców Śląskich" value="Środa 16.30 (Powstańców Śląskich)">Środa 16.30 (Powstańców Śląskich)</label>
                                    <label>
                                        <input type="checkbox" name="pascal2" id="pascal2-Piątek1820Bolkowska" value="Piątek 18.20 (Bolkowska)">Piątek 18.20 (Bolkowska)</label>
                                    <label>
                                        <input type="checkbox" name="pascal2" id="pascal2-Sobota1100Powstańców Śląskich" value="Sobota 11.00 (Powstańców Śląskich)">Sobota 11.00 (Powstańców Śląskich)</label>
                                </div>
                                <div class="checkbox-wrap">
                                    <label for="pitagoras" class="optional">Pitagoras (8-9 lat) I rok</label>
                                    <label>
                                        <input type="checkbox" name="pitagoras" id="pitagoras-Poniedziałek1630Bolkowska" value="Poniedziałek 16.30 (Bolkowska)">Poniedziałek 16.30 (Bolkowska)</label>
                                    <label>
                                        <input type="checkbox" name="pitagoras" id="pitagoras-Środa1820Powstańców Śląskich" value="Środa 18.20 (Powstańców Śląskich)">Środa 18.20 (Powstańców Śląskich)</label>
                                    <label>
                                        <input type="checkbox" name="pitagoras" id="pitagoras-Sobota1100Bolkowska" value="Sobota 11.00 (Bolkowska)">Sobota 11.00 (Bolkowska)</label>
                                </div>
                                <div class="checkbox-wrap">
                                    <label for="pitagoras2" class="optional">Pitagoras (8-9 lat) II rok</label>
                                    <label>
                                        <input type="checkbox" name="pitagoras2" id="pitagoras2-Czwartek1630Powstańców Śląskich" value="Czwartek 16.30 (Powstańców Śląskich)">Czwartek 16.30 (Powstańców Śląskich)</label>
                                    <label>
                                        <input type="checkbox" name="pitagoras2" id="pitagoras2-Sobota1255Powstańców Śląskich" value="Sobota 12.55 (Powstańców Śląskich)">Sobota 12.55 (Powstańców Śląskich)</label>
                                </div>
                                <div class="checkbox-wrap">
                                    <label for="Euler" class="optional">Euler (10-11 lat) I rok</label>
                                    <label>
                                        <input type="checkbox" name="euler" id="euler-Wtorek1820Powstańców Śląskich" value="Wtorek 18.20 (Powstańców Śląskich)">Wtorek 18.20 (Powstańców Śląskich)</label>
                                    <label>
                                        <input type="checkbox" name="euler" id="euler-Czwartek1820Powstańców Śląskich" value="Czwartek 18.20 (Powstańców Śląskich)">Czwartek 18.20 (Powstańców Śląskich)</label>
                                </div>
                            </dl>
                        </fieldset>
                    </dd><dt id="programowanie-label">&nbsp;</dt>
                    <dd id="programowanie-element">
                        <fieldset id="fieldset-programowanie">
                            <legend>Grupa Zajęć Programowanie Komputerów Bemowo</legend>
                            <dl>
                                <div class="checkbox-wrap2">
                                    <label for="scratch" class="optional">Scratch ( 8-9 lat ) I rok NOWOŚĆ!</label>
                                    <!--<label>
                                        <input type="checkbox" name="enter" id="enter-Wtorek1630" value="Wtorek
                                        16:30">Wtorek 16:30</label> -->
                                    <label>
                                        <input type="checkbox" name="scratch" id="scratch-Wtorek1820 Powstańców
                                             Śląskich"
                                               value="Wtorek
                                            18:20 (Powstańców
                                             Śląskich)">Wtorek 18:20 (Powstańców
                                        Śląskich)</label>
                                    <!-- <label>
                                         <input type="checkbox" name="enter" id="enter-Środa1820" value="Środa
                                         18:20">Środa 18:20</label> -->
                                    <label>
                                        <input type="checkbox" name="scratch" id="scratch-Środa1630Powstańców
                                             Śląskich"
                                               value="Środa 16:30 (Powstańców
                                             Śląskich)">Środa 16:30 (Powstańców
                                        Śląskich)</label>

                                    <label>
                                        <input type="checkbox" name="scratch" id="scratch-Sobota0900Powstańców
                                             Śląskich"
                                               value="Sobota
                                             09:00(Powstańców
                                             Śląskich) ">Sobota 09:00 (Powstańców
                                        Śląskich)</label>
                                </div>

                                <div class="checkbox-wrap2">
                                    <label for="enter_tarchomin" class="optional">Enter (9-11 lat) I rok</label>
                                    <label>
                                        <input type="checkbox" name="enter" id="enter_tarchomin-1820Środa"
                                               value="18:20 Środa (Powstańców Śląskich)">Środa 18:20
                                        (Powstańców Śląskich)</label>
                                    <label>
                                        <input type="checkbox" name="enter" id="enter_tarchomin-1630Czwartek"
                                               value="16:30 Czwartek (Powstańców Śląskich)">Czwartek 16:30
                                        (Powstańców Śląskich)</label>
                                    <label>
                                        <input type="checkbox" name="enter" id="enter_tarchomin-1100Sobota"
                                               value="11:00 Sobota (Powstańców Śląskich)">Sobota 11:00
                                        (Powstańców Śląskich)</label>
                                </div>

                                <div class="checkbox-wrap2">
                                    <label for="enterb_tarchomin" class="optional">Shift (12-14 lat) I rok</label>
                                    <label>
                                        <input type="checkbox" name="enterb"
                                               id="enterb_tarchomin-1630WtorekPowstańcow Śląskich"
                                               value="16:30 Wtorek (Powstańcow Śląskich)">16:30 Wtorek (Powstańców
                                        Śląskich)
                                    </label>
                                    <label>
                                        <input type="checkbox" name="enterb"
                                               id="enterb_tarchomin-1820CzwartekPowstańcow Śląskich"
                                               value="18:20 Czwartek (Powstańcow Śląskich)">18:20 Czwartek (Powstańców
                                        Śląskich)
                                    </label>
                                    <label>
                                        <input type="checkbox" name="enterb" id="enterb_tarchomin-1255SobotaPowstańcow Śląskich"
                                               value="12:55 Sobota (Powstańcow Śląskich)">12:55 Sobota (Powstańców
                                        Śląskich)
                                    </label>
                                </div>
                            </dl>
                        </fieldset>
                    </dd><dt id="fplatnosc-label">&nbsp;</dt>
                    <dd id="fplatnosc-element">
                        <fieldset id="fieldset-fplatnosc">
                            <legend>Płatność za zajęcia</legend>
                            <dl>
                                <div class="checkbox-wrap3">
                                    <dd id="platnosc-label">
                                        <label for="platnosc" class="required">Za zajęcia zapłacę *</label>
                                    </dd>
                                    <label>
                                        <input type="radio" name="platnosc" id="platnosc-1" value="Jednorazowo za semestr przed rozpoczęciem zajęć" class="radio">Jednorazowo za semestr przed rozpoczęciem zajęć</label>
                                    <label>
                                        <input type="radio" name="platnosc" id="platnosc-2" value="5 płatności miesięcznych" class="radio">5 płatności miesięcznych</label>
                                </div>
                                <div class="checkbox-wrap3">
                                    <dd id="forma-label">
                                        <label for="forma" class="required">Forma Płatności *</label>
                                    </dd>
                                    <label>
                                        <input type="radio" name="forma" id="forma" value="Gotówką w Centrum Edukacyjnym" class="radio">Gotówką w Centrum Edukacyjnym</label>
                                    <label>
                                        <input type="radio" name="forma" id="forma-2" value="Przelewem na konto 38 1950 0001 2006 0889 8214 0002 IDEA BANK S.A. (odbiorca: Matplaneta SA ul. Targowa 20 A, 03-731 Warszawa, NIP 113 287 75 16 ) W tytule przelewu prosimy o wpisanie: lokalizacja, grupa, dzień, imię i nazwisko dziecka" class="radio">Przelewem na konto 38 1950 0001 2006 0889 8214 0002 IDEA BANK S.A. (odbiorca: Matplaneta SA ul. Targowa 20 A, 03-731 Warszawa, NIP 113 287 75 16 ) W tytule przelewu prosimy o wpisanie: lokalizacja, grupa, dzień, imię i nazwisko dziecka</label>
                                </div>
                                <dd id="rodzenstwo-element">
                                    <input type="checkbox" name="rodzenstwo" <?php if(!empty($_POST)){if($_POST[ 'rodzenstwo']){echo "checked";}}else{if($_POST[ 'rodzenstwo']){echo "checked";}} ?> ></dd>
                                <label for="rodzenstwo" class="rodzenstwo optional">Przysługuje mi 15% rabatu za rodzeństwo</label>
                                <dd id="polecenie-element">
                                    <input type="checkbox" name="polecenie" <?php if(!empty($_POST)){if($_POST[ 'polecenie']){echo "checked";}}else{if($_POST[ 'polecenie']){echo "checked";}} ?> ></dd>
                                <label for="polecenie" class="rodzenstwo optional">Przysługuje mi rabat za polecenie</label>
                                <div class="grupa" id="regulamin">
                                    <input type="checkbox" name="akceptacja" id="akceptacja" <?php if(!empty($_POST)){if($_POST[ 'akceptacja']){echo "checked";}}else{if($_POST[ 'akceptacja']){echo "checked";}} ?> ></div>
                                <div id="akceptacja-label">
                                    <label for="akceptacja" class="rodzenstwo required">Akceptuję Regulamin *</label>
                                </div>
                                <div class="grupa">
                                    <input type="checkbox" name="zgoda" id="zgoda" <?php if(!empty($_POST)){if($_POST[ 'zgoda']){echo "checked";}}else{if($_POST[ 'zgoda']){echo "checked";}} ?> ></div>
                                <div id="zgoda-label">
                                    <label for="zgoda" class="rodzenstwo required">Wyrażam zgodę na przetwarzanie moich danych osobowych dla potrzeb niezbędnych do realizacji procesu rekrutacji (zgodnie z Ustawą z dnia 29.08.1997 roku o Ochronie Danych Osobowych; tekst jednolity: Dz. U. z 2002r. Nr 101, poz. 926 ze zm.). *</label>
                                </div>
                                <div class="grupa">
                                    <input type="checkbox" name="zgoda" id="zgoda" <?php if(!empty($_POST)){if($_POST[ 'zgoda']){echo "checked";}}else{if($_POST[ 'zgoda']){echo "checked";}} ?> ></div>
                                <div id="zgoda-label">
                                    <label for="zgoda" class="rodzenstwo required">Wyrażam zgodę na przetwarzanie i wykorzystywanie wizerunku mojego Dziecka
                                        oraz mojego poprzez zamieszczanie zdjęć na stronie internetowej przedszkola, tablicach
                                        informacyjnych, gazetkach, płytach CD ( udostępnianych także innym rodzicom),
                                        ich publikację prasową, a także publikowanie nagrań radiowych i telewizyjnych
                                        z udziałem mojego Dziecka i moim, w zakresie działalności opiekuńczo- wychowawczej
                                        i dydaktycznej przedszkola oraz w celach promocyjno- marketingowych, zgodnie
                                        z ustawą z dnia 4 lutego 1994r. o prawie autorskim i prawach pokrewnych ( Dz. U.
                                        z 2000r. , nr 80, poz. 904 z późń. zm.).
                                    </label>
                            </dl>
                        </fieldset>
                    </dd><dt id="subform-label">&nbsp;</dt>
                    <dd id="subform-element">
                        <fieldset id="fieldset-subform">
                            <dl><dt id="submit-label">&nbsp;</dt>
                                <dd id="submit-element">
                                    <input type="submit" name="send" id="submit" value="Wyślij" class="ui-button ui-widget ui-state-default ui-corner-all" role="button">
                                </dd><dt id="reset-label">&nbsp;</dt>
                                <dd id="reset-element">
                                    <input type="reset" name="reset" id="reset" value="Anuluj" class="ui-button ui-widget ui-state-default ui-corner-all" role="button">
                                </dd>
                            </dl>
                        </fieldset>
                    </dd>
                </dl>
            </form>
        </div>
    </section>
    <footer>
        <div id="main-footer" class="wrapper">
            <div class="main-border"></div>
            <div class="container">
                <div class="col6 main-links">
                    <h6> Kontakt </h6>
                    <ul>
                        <li> <span class="icon-home"></span> <span>Al. Komisji Edukacji Narodowej 95, klatka 18a, 2 piętro<br> (obok stacji metra STOKŁOSY)</span> </li>
                        <li> <span class="icon-phone"></span> <span>+48 22 100 53 47</span> </li>
                        <li> <span class="icon-envelope"></span> <a href="mailto:biuro@e-matplaneta.pl">biuro@e-matplaneta.pl</a> </li>
                        <li> <span class="icon-earth"></span> <a href="../index.html">www.matplaneta.pl</a> </li>
                    </ul>
                </div>
                <div class="col6 footer-links">
                    <h6> Lokalizacje </h6>
                    <ul>
                        <li><a href="ursynow.html">Ursynów</a></li>
                        <li><a href="bemowo.html">Bemowo</a></li>
                        <li><a href="tarchomin.html">Tarchomin</a></li>
                        <li><a href="ursus.html">Ursus</a></li>
                        <li><a href="jozefow.html">Józefów</a></li>
                        <li><a href="jozefoslaw.html">Józefosław</a></li>
                        <li><a href="zary.html">Żary</a></li>
                        <li><a href="krakow.html">Kraków</a></li>
                        <li><a href="gdansk.html">Gdańsk</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="sub-footer">
            <div class="container">
                <div class="col4"> <span id="copyright">Copyright © 2014 InfoApps</span> </div>
                <div id="footer-nav" class="col8 footer-links">
                    <ul>
                        <li> <a href="../index.html#about-section">O nas</a> </li>
                        <li> <a href="../index.html#news-section">News</a> </li>
                        <li> <a href="../index.html#lesson-section">Zajęcia</a> </li>
                        <li> <a href="../index.html#gallery-section">Galeria</a> </li>
                        <li> <a href="../index.html#location-section">Lokalizacje</a> </li>
                        <li> <a href="../index.html#work-section">Praca</a> </li>
                        <li> <a href="../index.html#contact-section">Kontakt</a> </li>
                        <li> <a href="../zapisy.html">Zapisy</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
</div>
<script src="../scripts/sub-page-script.js" type="text/javascript"></script>
<script src="validate.js"></script>
</body>

</html>