<!-- get header -->
<?php $root = realpath($_SERVER["DOCUMENT_ROOT"]);
 require("$root/inc/header.php");?>

<!-- get sidebar-->
<?php require("$root/inc/sidebar.php");?>

<!-- start location -->
            <section id="location" class="wrapper">
                <div class="container">
                    <div class="section-header">
                        <h2> Warszawa Tarchomin </h2> <img src="/assets/images/border.png" alt="border"></div>
                    <div class="col12"><a href="http://polygon.matplaneta.pl/Oferta.aspx?region=bialoleka" class="zapisy-button">Zapisy</a></div>
                    <div class="col12 row"></div>
                    <div class="col6"><img class="img-responsive" src="/assets/images/a.jpg"
                     alt="Zajęcia na Tarchominie">
                    </div>
                    <div class="col6">
                        <ul>
                            <li>
                                <h4> Centrum Edukacyjne Matplaneta Tarchomin </h4></li>
                            <li>
                                <p> ul. Myśliborska 98G
                                    <br>Tel. +48 22 747 16 85
                                    <br>Biuro obsługi klienta: +48 (22) 100 53 47 
                                    <br>mail: biuro@e-matplaneta.pl  
                                    <br>Godziny pracy: 
                                    <br>Pn - Czw: 15:00-20:00 
                                    <br>Pt: nieczynne 
                                    <br>Soboty: 8:00-16:00 
                                    <br></p>
                            </li>
                        </ul>
                    </div>
					<div class="col12">
                        <ul>
                            <li>
                                <h5> Podmiot świadczący usługi edukacyjne: </h5></li>
                            <li>
                                <p>Matplaneta SA, ul. Targowa 20a, 03-731 Warszawa, NIP 1132877516</p>
                            </li>
                            <li>
                                <h5> Nr konta bankowego:</h5></li>
                            <li>
                                <p>38 1950 0001 2006 0889 8214 0002 (Idea Bank)</p>
                            </li>							
                        </ul>
					</div>

                    <div class="col12 row"></div>
                    <div class="col12">
                        <iframe src="https://www.google.com/maps/d/embed?mid=zBFk1ypiCdAU.kCSXywcOT9qw&z=17" width="100%" height="400" class="maps">

                        </iframe>

                    </iframe>
                    </div>
					<div class="col12">
						<h4 class="news-headline">Wirtulalny spacer</h4>
						<p>Istnieje możliwość wirtulanego spaceru po centrum Matplanety na Tarchominie. Zapraszamy do obejrzenia przykładowych sal.</p>
							
						<div class="text-center" style="margin-top: 13px;">
							<iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1spl!2spl!4v1447254038576!6m8!1m7!1sGBFYkqxw86gAAAQzPlNOaw!2m2!1d52.32009084711552!2d20.95676240502394!3f28.570169278267763!4f-23.129538537544974!5f0.7820865974627469" width="700" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>
					
                    <?php require("$root/inc/harmonogram.php");?>
                    <div class="col12">
                        <div class="local-news-container">
                             <div class="phGallary">
                                 <ul id="gallary">
                                     <li>
									 <h4 class="news-headline"> Aktualności </h4>
										<div>
                                            <div class="local-news">
                                                <h6> 11-02-2016 </h6>
                                                <h5 class="news-header">GŁOSOWANIE!!!</h5>
												<p>Gorąco zachęcamy wszystkich do oddania głosu na Matplanetę w Plebiscycie na Statuetkę Warszawskiej Rodzinki Prudential.<br><br>
W konkursie walczymy o miano miejsca z najbardziej bogatą, atrakcyjną i wartościową ofertą skierowaną do dzieci i młodzieży:)<br><br>
Zagłosować należy przez stronę:<a href="http://www.warszawskarodzinka.pl/">http://www.warszawskarodzinka.pl/</a> <br><br>
Z góry dziękujemy za każdy oddany głos:)
									</p>   
                                        </div>
										<div>
                                            <div class="local-news">
                                                <h6> 10-02-2016 </h6>
                                                <h5 class="news-header">DNI OTWARTE</h5>
												<p>W Krakowie już 22 i 26 lutego odbędą się lekcje pokazowe Matplanety w szkołach.<br><br>
A na początku marca wszystkich zainteresowanych zajęciami w Matplanecie zapraszamy na bezpłatne Dni Otwarte w Centrach Edukacyjnych na Ursynowie, Tarchominie, Bemowie oraz w Krakowie.<br><br>
Terminy, godziny oraz szczegółowe informacje znajdziecie tutaj: <a href="http://matplaneta.pl/dni_zapisy/public/dni_otwarte">http://matplaneta.pl/dni_zapisy/public/dni_otwarte.</a> <br><br>
Zapraszamy do zapisów!!!
									</p>    
                                        </div>
										</div>
										<div>
                                            <div class="local-news">
                                                <h6> 01-02-2016 </h6>
                                                <h5 class="news-header">PRZERWA ZIMOWA</h5>
												<p>Informujemy, że w czasie Ferii Zimowych (1-14.02) zajęcia Matplanety nie odbywają się. W tym czasie organizujemy Ferie z Robotami Lego, Programowaniem Gier Komputerowych oraz Grafiką Komputerową.<br><br>
													Do zobaczenia 15.02:)
												</p>     
                                            </div>
                                        </div><!-- end con-div -->	
										<div>
                                            <div class="local-news">
                                                <h6> 11-01-2015 </h6>
                                                <h5 class="news-header">UWAGA!!! RABAT 15% NA FERIE Z MATPLANETĄ</h5>
												 <img style="max-height: 280px;" src="/assets/images/rabat.png">
												 <p>
Dla rodzeństw oraz uczniów naszych zajęć mamy specjalną ofertę!!!<br><br>
Wiedząc, że cena jednego turnusu Ferii w Matplanecie wynosi 680zł, rozwiążcie zagadkę ze zdjęcia:) Grafika komputerowa / Tworzenie gier komputerowych / Budowanie robotów mogą stać się pasją Twojego dziecka - Zapisz je już teraz: http://matplaneta.pl/zapisy-ferie-2016/
									</p>     
                                            </div>
                                        </div><!-- end con-div -->	
										<div>
                                            <div class="local-news">
                                                <h6> 05-01-2015 </h6>
                                                <h5 class="news-header">ŚWIĄTECZNE WIEŚCI Z MATPLANETY</h5>
												<p>
												FERIE Z MATPLANETĄ już za niecały miesiąc, a Wy wciąż możecie dopisać swoje dziecko na:<br><br>

												- ROBOTY LEGO WEDO<br>
												- GRAFIKĘ KOMPUTEROWĄ <br>
												- PROGRAMOWANIE GIER KOMPUTEROWYCH<br><br>

												W czasie naszych ferii dzieci rozwiną wyobraźnię i logiczne myślenie oraz przeżyją wspaniałą przygodę, którą zapamiętają na długo!!! Zapiszcie się już dziś!<br><br>

												Wystarczy wejść na stronę i wypełnić formularz: <a href="http://matplaneta.pl/zapisy-ferie-2016/">http://matplaneta.pl/zapisy-ferie-2016/</a><br><br>
												Specjalne rabaty dla rodzeństwa oraz uczestników zajęć Matplanety:)
												</p>     
                                            </div>
                                        </div><!-- end con-div -->	
										<div>
                                            <div class="local-news">
                                                <h6> 16-12-2015 </h6>
                                                <h5 class="news-header">ŚWIĄTECZNE WIEŚCI Z MATPLANETY</h5>
												<p>W grudniu ukazał się już 4 numer gazetki Matplanety!<br><br>
													A w nim:<br>
													- krótkie podsumowanie minionych miesięcy,<br>
													- zapowiedź wyczekiwanych przez wszystkich FERII ZIMOWYCH<br>
													- krótka wzmianka o świątecznych promocjach oraz o planowanych warsztatach dla rodziców,<br>
													- a przede wszystkim ZAGADKI, ZAGADKI, ZAGADKI!!!<br><br>

													W Święta nie pozwalamy się nudzić:)<br><br>

													<a target="_blank" href="http://matplaneta.pl/assets/download/gazetka_nr_4_grudzien.pdf">Pobierz gazetkę</a><br>
												</p>     
                                            </div>
                                        </div><!-- end con-div -->	
										<div>
                                            <div class="local-news">
                                                <h6> 12-12-2015 </h6>
                                                <h5 class="news-header">FERIE Z MATPLANETĄ - Najlepsza przygoda dla każdego dziecka!</h5>
												<p>Wszystkie dzieci, które zostają w mieście chociaż przez jeden tydzień ferii, zapraszamy na fantastyczne zajęcia w Matplanecie. Proponujemy całodniową opiekę, zajęcia w pracowni komputerowej, gry i zabawy nie tylko matematyczne, pyszne posiłki i mnóstwo zabawy.<br><br>

													Młodsze dzieci zapraszamy na spotkanie z robotami Lego.<br>
													Dla nieco starszych proponujemy w tym roku nowości: Projektowanie gier komputerowych oraz Grafikę komputerową (również dla Dziewczynek!).<br><br>

													Więcej informacji i zapisy: <a href="http://matplaneta.pl/zapisy-ferie-2016/">tutaj</a><br>
													Zarezerwuj miejsce dla swojego dziecka już dziś!
												</p>    
                                            </div>
                                        </div><!-- end con-div -->
										<div>
                                            <div class="local-news">
                                                <h6> 28-09-2015 </h6>
                                                <h5 class="news-header">Matplaneta - najbardziej rozwojową warszawską inicjatywą roku 2015 w kategorii LOGIKA.</h5>
												<p>Matplaneta - najbardziej rozwojową warszawską inicjatywą roku 2015 w kategorii LOGIKA. Złoty Słonecznik w naszych rękach!!!<br>
													Dziękujemy wszystkim za oddane głosy;)<br><br>
													Więcej informacji oraz zdjęcia: <a href="http://sloneczniki.czasdzieci.pl/aktualnosci/id,82-sloneczniki_2015_czyli.html" target="_blank">http://sloneczniki.czasdzieci.pl/aktualnosci/id,82-sloneczniki_2015_czyli.html</a>
												</p>     
                                            </div>
                                        </div><!-- end con-div -->	
										<div>
                                            <div class="local-news">
                                                <h6> 18-09-2015 </h6>
                                                <h5 class="news-header">Już jutro startujemy!</h5>
												<p>Już w tą sobotę (19.09) ruszamy z pierwszymi zajęciami w naszych placówkach.<br>
													Od samego rana grupy naszych starych oraz nowych uczniów rozpoczną przygodę z matematycznymi oraz logicznymi wyzwaniami. Będziemy programować pierwsze programy komputerowe oraz łamać głowy nad trudnymi zadaniami.<br>
													Wszyscy czekamy z niecierpliwością na jutrzejszy dzień i nie możemy się już doczekać!
												</p>    
                                            </div>
                                        </div><!-- end con-div -->	
									    <div>
                                            <div class="local-news">
                                                <h6> 26-08-2015 </h6>
                                                <h5 class="news-header">Matplaneta na Rowerowym Pikniku!</h5>
												<p>W sobotę 29. sierpnia 2015 r. w godzinach 11:00 – 18:00 w Centrum Handlowym Galerii Bemowo odbędzie się piknik na zakończenie wakacji.<br>
												Szukajcie nas na stanowisku z grami i zadaniami logicznymi.<br>
												Jak zawsze czeka na Was moc atrakcji i świetnej zabawy oraz konkurs, w którym można wygrać wspaniałe rowery miejskie.<br><br>
												Do zobaczenia:)<br>
												<a href="https://www.facebook.com/events/1482966322022055/" target="_blank">więcej</a></p>    
                                            </div>
                                        </div><!-- end con-div -->		
									 	<div>
                                            <div class="local-news">
                                                <h6> 21-08-2015 </h6>
                                                <h5 class="news-header">Miło nam poinformować, że w Matplanecie uruchomiliśmy nasz własny system komunikacji z klientami - Polygon.</h5>

												<p>W systemie są dostępne funkcjonalności pozwalające m.in. na:<br>
												•	zapisanie dziecka na zajęcia,<br>
												•	wysłanie maila do Nauczyciela lub Administratora oddziału,<br>
												•	sprawdzenie kwot i terminów opłat oraz przejrzenie uiszczonych wpłat,<br>
												•	aktualizację swoich danych kontaktowych.<br></p>
												<p>W menu „Rekomendacja” nasi stali Klienci znajda proponowaną grupę dla  dziecka na I semestr 2015/2016 i skrót do Zapisów.</p>
												<p>Pełna oferta zajęć znajduje się w menu „Zapisy”.</p>
												<p>Wierzymy, że system będzie przyjazny dla Państwa i uprości proces zapisu  oraz kontakty z biruem.</p>
												<p>Zapraszamy do rejestrowania się i zapisywania na zajęcia!</p>
												<p><a href="http://matplaneta.pl/assets/download/System%20Obs%C5%82ugi%20Klienta%20POLYGON.pdf" target="_blank">Krótka instrukcja obsługi</a></p>
												<p>Z pozdrowieniami<br>
												Zespół Matplanety</p>     
                                            </div>
                                        </div><!-- end con-div -->
                                        <div>
                                            
                                            <div class="local-news">
                                                <h6> 19-06-2014 </h6>
                                                <h5 class="news-header">Ruszyły zapisy na nowy semestr!</h5>

                                                <p> W placówkach Matplanety na Ursynowie, Tarchominie, i Bemowie rozpoczęły się zapisy na  Semestr Jesień 2015.                              Serdecznie zapraszamy do zapoznania się z harmonogramem zajęć na nowy rok szkolny, a w razie pytań zachęcamy do kontaktu.

                                                    <br>

                                                </p>
                                            </div><!-- end local-news -->
                                        </div><!-- end con-div -->
                                        <div>
                                            <div class="local-news">
                                                <h6> 25-05-2014 </h6>
                                                <h5 class="news-header">Jeszcze w tym semestrze możesz przyjść na Dzień Otwarty do Matplanety!</h5>

                                                <p> Zapraszamy wszystkich na Dzień Otwarty do Matplanety. Jest to świetna okazja, żeby spotkać się z nauczycielami w naszych oddziałach i dowiedzieć się, na czym polegają zajęcia.<br>
                                                    Ursynów - 10 czerwca, godz. 18:30<br>
                                                    Tarchomin - 12 czerwca, godz. 18:30<br>
                                                    Bemowo - 8 czerwca, godz. 18:30<br>
                                                    ZAPISY :  zapisy@e-matplaneta.pl
                                                </p>
                                            </div><!-- end local-news -->
                                        </div><!-- end con-div -->
                                        <div>
                                            <div class="local-news">
                                                <h6> 25-05-2014 </h6>
                                                <h5 class="news-header">Wakacje z Robotami Lego WeDo i Mindsotrms EV3</h5>

                                                <p> Jak co roku Matplaneta zaprasza dzieci w wieku 6-12 lat na Wakacje z Robotami Lego WeDo i Mindsotrms EV3 w naszych placówkach na Ursynowie i Tarchominie. Spotkania z robotami to wspaniała przygoda dla każdego młodego odkrywcy.
                                                    Zapisz się już dziś!<br>
                                                    Szczegółowe informacje i zapisy przez stronę lub telefonicznie. A dla niezdecydowanych, polecamy obejrzeć relację z Ferii z Robotami 2015: https://vimeo.com/125895045
                                                </p>
                                            </div><!-- end local-news -->
                                        </div><!-- end con-div -->
                                     </li><!-- end firt-page  -->
                                 </ul><!-- end gallary -->
                                 <center>
                                     <!-- pagination -->
                                     <span id="PageList" class="clear"></span>
                                 </center>
                            </div><!-- end phGallary -->
                        </div><!-- end lcoal-news-container -->
                    </div><!-- end col12 -->
                </div><!-- end container -->
            </section>

<!-- get footer -->
<?php require("$root/inc/footer.php");?>