//navigation
var menuButton = document.getElementById('menu-button');
var menuList = document.getElementById('menu-list');
menuButton.onclick = function() {
    if(menuList.style.display == "none"){
        menuList.style.display = "block";
    }
    else{
        menuList.style.display = "none";
    }
    var menuItem = menuList.getElementsByTagName('li');
    for(var i = 0; i < menuItem.length; i++){
        menuItem[i].onclick = function(){
            menuList.style.display = "none";
        }
    }
}