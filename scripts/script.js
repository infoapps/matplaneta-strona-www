//navigation
var menuButton = document.getElementById('menu-button');
var menuList = document.getElementById('menu-list');
menuButton.onclick = function () {
    if (menuList.style.display === "none") {
        menuList.style.display = "block";
    } else {
        menuList.style.display = "none";
    }
    var menuItem = menuList.getElementsByTagName('li'),
        i;
    for (i = 0; i < menuItem.length; i += 1) {
        menuItem[i].onclick = function () {
            menuList.style.display = "none";
        };
    }
};

//tabs
function removeActiveTab(zmienna) {
    var i;
    for (i = 0; i < zmienna.length; i += 1) {
        zmienna[i].removeAttribute('class');
    }
}
function removeActiveContent(zmienna) {
    var i;
    for (i = 0; i < zmienna.length; i += 1) {
        zmienna[i].getAttributeNode('class').value = 'tab';
    }
}
function tabClick() {
    var linkTabs = document.getElementById("tabs").getElementsByTagName('a'),
        i;
    for (i = 0; i < linkTabs.length; i += 1) {
        linkTabs[i].addEventListener("click", function (event) {
            event.preventDefault();
            removeActiveTab(document.getElementById('tabs').children);
            this.parentNode.setAttribute("class", "active");
            //content
            var currentAttrValue = this.getAttribute('href').slice(1, 6);
            removeActiveContent(document.getElementById('tab-content').children);
            document.getElementById(currentAttrValue).setAttribute("class", 'tab active');
        });
    }
}
tabClick();
function showMoreContent() {
    var showButton = document.getElementsByClassName("show-hidden"),
        i;
    for (i = 0; i < showButton.length; i += 1) {
        showButton[i].addEventListener("click", function(event){
            event.preventDefault();
            this.previousElementSibling.innerHTML += this.nextElementSibling.innerHTML;
            this.remove();
        });
    }
}
showMoreContent();

//cookie
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
}
function getCookie(cname) {
    var name = cname + "=",
        ca = document.cookie.split(';'),
        max = ca.length,
        i;
    for (i = 0; i < max; i +=1) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length); //zwraca wartość name 
        }
    }
    return "";
}
(function checkCookie() {
    if (getCookie('cookies_accepted') != 'T') {
        var message_container = document.createElement('div');
        message_container.id = 'cookies-message-container';
        var html_code = '<div id="cookies-message" style="padding: 10px 0px; font-size: 14px; line-height: 22px; border-top: 1px solid #D3D0D0; text-align: center; position: fixed; bottom: 0px; background-color: #EFEFEF; width: 100%; z-index: 999;">Ta strona używa ciasteczek (cookies), dzięki którym nasz serwis może działać lepiej. <a href="http://wszystkoociasteczkach.pl" target="_blank" style="font-size: 14px;">Dowiedz się więcej</a><a href="javascript:WHCloseCookiesWindow();" id="accept-cookies-checkbox" name="accept-cookies" style="background-color: #9DBB1D; font-size: 14px; padding: 5px 10px; color: #FFF; display: inline-block; margin-left: 10px; text-decoration: none; cursor: pointer;">Rozumiem</a></div>';
        message_container.innerHTML = html_code;
        document.body.appendChild(message_container);
    }
}());
function WHCloseCookiesWindow() {
    setCookie('cookies_accepted', 'T', 365);
    document.getElementById('cookies-message-container').removeChild(document.getElementById('cookies-message'));
}
