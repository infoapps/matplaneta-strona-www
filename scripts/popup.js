(function () {
    'use strict';
    var closeButton = document.getElementsByClassName('close-button'),
        max = closeButton.length,
        i;
    for (i = 0; i < max; i += 1) {
        closeButton[i].addEventListener('click', function () {
            this.parentNode.parentNode.removeChild(this.parentNode);
        });
    }
    
}());