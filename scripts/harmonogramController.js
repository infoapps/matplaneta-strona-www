var matApp = angular.module('matApp', []);

matApp.controller('HarmonogramController', function($scope, $http) {
    $http.get('/soap-harmonogram/harmonogram.php').then(function(res){
        $scope.harmonogram = res.data.aHarmonogramPozycja;
    });;

    $scope.location = window.location.href.split('/')[window.location.href.split('/').length - 2];

    $scope.getLocation = function (element) {
        if ($scope.location === 'bemowo') {
            if (element === 'Bemowo - Bolkowska, Ośrodek Kultury i Edukacji SM Wola, ul. Bolkowska 2c ') {
                return element;
            } else if (element === 'Bemowo - Powstańców Śląskich, Ośrodek Kultury i Edukacji SM Wola, ul. Powstańców Śląskich 108A') {
                return element;
            }
        } else if ($scope.location === 'tarchomin') {
            if (element === 'Tarchomin, ul. Myśliborska 98G, I piętro') {
                return element;
            }
        } else if ($scope.location === 'ursynow') {
            if (element === 'Ursynów, Aleja KEN 95, klatka 18A, piętro II') {
                return element;
            }
        } else if ($scope.location === 'ursus') {
            if (element === 'Ursus, Dom Kultury Kolorowa, ul. gen. K. Sosnkowskiego 16, parter, Warszawa - Ursus') {
                return element;
            }
        } else if ($scope.location === 'grodzisk') {
            if (element === 'Grodzisk Mazowiecki - Westfala, PAWILON KULTURY - ul. Westfala 3 05-825 Grodzisk Mazowiecki') {
                return element;
            } else if (element === 'Grodzisk Mazowiecki - Kopernika, Niepubliczna Szkola Tosi i Franka ul. Kopernika 12 05-827 Grodzisk Mazowiecki'){
                return element;
            }
        } else if ($scope.location === 'jozefoslaw') {
            if (element === 'Józefosław, ul. Julianowska 67A, Józefosław') {
                return element;
            }
        } else if ($scope.location === 'konstancin') {
            if (element === 'Konstancin-Jeziorna, ul. Sobieskiego 6, 05-510 Konstancin-Jeziorna') {
                return element;
            }
        } else if ($scope.location === 'gdansk') {
            if (element === 'Trójmiasto, ul. Warneńska 8c/3, 80-288 Gdańsk') {
                return element;
            }
		} else if ($scope.location === 'slupsk') {
            if (element === 'Słupsk, ul. Filmowa 1, Słupsk') {
                return element;
            }	
        } else if ($scope.location === 'krakow') {
            if (element === 'Kraków - Ruczaj, ul. Torfowa 4') {
                return element;
            } else if (element === 'Kraków - Dworek Białoprądnicki, ul. Papiernicza 2, Prądnik Biały, Kraków'){
                return element;
            } else if (element === 'Kraków- DK Mydlniki, ul. Balicka 289, Bronowice, Kraków'){
                return element;
            }
        }  else if ($scope.location === 'jozefow') {
            if (element === 'Józefów, ul. Zawiszy Czarnego 1C, Józefów') {
                return element;
            }	
        }	else if ($scope.location === 'mokotow') {
            if (element === 'Mokotów - Ene Due, Ene-Due Centrum Językowo-Kulturowe dla Dzieci, ul. Chodkiewicza 8, Osiedle Eko-Park ') {
                return element;
            }	
        }	else if ($scope.location === 'bialoleka') {
            if (element === 'Białołęka, Przedszkole Niepubliczne Wyspa Skarbów, ul. Skarbka z Gór 142B ') {
                return element;
            }	
        }	else if ($scope.location === 'wilanow') {
            if (element === 'Wilanów, Polsko-Francuska Szkoła Podstawowa, Ul. Wandy Rutkiewicz 2, Warszawa') {
                return element;
            }	
        }	else if ($scope.location === 'radzymin') {
            if (element === 'Radzymin - Nowe Załubice, Ul. Krucza 5 05-255 Nowe Załubice') {
                return element;
            }	
        }   
    }

    $scope.locationFilter = function (element) {
        if (element['aHarmonogramPozycja_Placowka'] === $scope.getLocation(element['aHarmonogramPozycja_Placowka'])) {
            return true;
        } else {
            return;
        }
    }
});
