<!-- start section -->
<section id="landing-page" class="wrapper">

    <!-- start container -->
    <div class="container">
        <div class="section-header">
            <h2 id="landing-pageHeader">Lekcje pokazowe Matplanety!</h2>
            <img src="assets/images/border.png" alt="border">
        </div>
        <!-- end section-header -->
        <div class="col12" id="landing-content">
        </div>
        <!-- end col12 -->
        <div class="col12 tabs" id="landingForm-tabs">
            <ul id="tabs" class="form-tabs">
                <li class="active">
                    <a href="#tab-1">Matematyka</a>
                </li>
                <li>
                    <a href="#tab-2">Programowanie</a>
                </li>
            </ul>
        </div>
        <div class="col12 landingForm-tab" id="tab-content">

            <div id="tab-1" class="tab active">
                <form action="dzien.php" name="landing-page-form" method="post" id="landing-page-form">
                    <input name="lesson" type="hidden" value="math"/>
                    <h6 class="form-header">Informacje o uczestniku</h6>
                    <input required="required" type="text" id="first-name" name="first_name" placeholder="Imię dziecka">
                    <br>
                    <input required="required" type="text" id="second-name" name="second_name" placeholder="Nazwisko dziecka">
                    <br>
                    <h6 class="form-header">Dzień otwarty <span><a
                                href="dzien.php#anchor-table">(harmonogram) </a></span></h6>
								<p style="width: 420px; font-size: 1.3em; color: rgb(255, 255, 255); padding-bottom: 5px;">Uprzejmie informujemy, że w przypadku zaznaczenia przez Państwa grupy oznaczonej jako pełnej, formularz staje się nieważny.</p>
                    <ul id="questionsSecond">

                    </ul>
                    <div id="preloader"></div>
                    <br>
                    <h6 class="form-header">Dane kontaktowe</h6>
                    <input required="required" type="email" id="mail" name="mail" placeholder="Twój email">
                    <br>
                    <input required="required" type="tel" id="phone" name="phone" placeholder="Telefon">
                    <br>
                    <input type="submit" value="Zapisz się!" name="submit" id="send">
                </form>
                <!-- end form -->

                <div class="col6" id="landing-text-section">
                    <h4>Już niebawem dzień otwarty w Matplanecie!</h4>
                    <h6>Nasze zajęcia rozpoczynają się od zadania praktycznego

                        problemu, na który odpowiedź może dać matematyka.

                        Problem jest analizowany w grupie, a dzieci same dochodzą

                        do rozwiązania. Liczy się przede wszystkim proces

                        myślowy, nie tylko jedynie słuszny wynik.</h6>
                    <img src="assets/images/dzine-otwarty2.jpg">
                </div>
                <!-- end col6 -->
                <div class="col12" id="harmonogram">
                    <h6 id="anchor-table"></h6><!-- empty h6 for anchor -->
                    <div class="col12">
                        <h4 class="table-header">Harmonogram</h4>
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border:none;}
.tg td{font-size:14px;padding:8px 17px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;}
.tg th{font-size:14px;font-weight:normal;padding:8px 17px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;}
.tg .tg-ewmi{background-color:#032c33;color:#97be3e}
@media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}</style>
<div class="tg-wrap"><table class="tg">
  <tr>
    <th class="tg-ewmi">Miejsce</th>
    <th class="tg-ewmi">Data</th>
    <th class="tg-ewmi">Godzina</th>
    <th class="tg-ewmi">Grupa</th>
  </tr>
  <tr>
    <td class="tg-ewmi">Ursynów</td>
    <td class="tg-ewmi">08.09</td>
    <td class="tg-ewmi">16:30</td>
    <td class="tg-ewmi">4-5 lat i 6-7 lat</td>
  </tr>
  <tr>
    <td class="tg-ewmi"><br></td>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi">18:20</td>
    <td class="tg-ewmi">8-9 lat</td>
  </tr>
  <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi">18.09</td>
    <td class="tg-ewmi">16:30</td>
    <td class="tg-ewmi">8-9 lat</td>
  </tr>
  <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi">18:20</td>
    <td class="tg-ewmi">4-5 lat i 6-7 lat</td>
  </tr>
  <tr>
    <td class="tg-ewmi" colspan="4"></td>
  </tr>
  <tr>
    <td class="tg-ewmi">Tarchomin</td>
    <td class="tg-ewmi">09.09</td>
    <td class="tg-ewmi">16:30</td>
    <td class="tg-ewmi">4-5 lat</td>
  </tr>
  <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi">18:20</td>
    <td class="tg-ewmi">8-9 lat</td>
  </tr>
  <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi">10.09</td>
    <td class="tg-ewmi">16:30</td>
    <td class="tg-ewmi">6-7 lat</td>
  </tr>
  <tr>
    <td class="tg-ewmi" colspan="4"></td>
  </tr>
  <tr>
    <td class="tg-ewmi">Bemowo</td>
    <td class="tg-ewmi">11.09</td>
    <td class="tg-ewmi">18:20</td>
    <td class="tg-ewmi">4-5 lat i 6-7 lat</td>
  </tr>
  <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi">16.09</td>
    <td class="tg-ewmi">18:20</td>
    <td class="tg-ewmi">8-9 lat</td>
  </tr>
  <tr>
    <td class="tg-ewmi" colspan="4"></td>
  </tr>
  <tr>
    <td class="tg-ewmi">Wilanów</td>
    <td class="tg-ewmi">14.09</td>
    <td class="tg-ewmi">18:20</td>
    <td class="tg-ewmi">4-5 lat</td>
  </tr>
  <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi">15.09</td>
    <td class="tg-ewmi">18:20</td>
    <td class="tg-ewmi">8-9 lat</td>
  </tr>
  <tr>
    <td class="tg-ewmi" colspan="4"></td>
  </tr>
  <tr>
    <td class="tg-ewmi">Józefów</td>
    <td class="tg-ewmi">17.09</td>
    <td class="tg-ewmi">18:20</td>
    <td class="tg-ewmi">wszystkie poziomy</td>
  </tr>
  <tr>
    <td class="tg-ewmi" colspan="4"></td>
  </tr>
  <tr>
    <td class="tg-ewmi">Białołęka</td>
    <td class="tg-ewmi">07.09</td>
    <td class="tg-ewmi">18:20</td>
    <td class="tg-ewmi">wszystkie poziomy</td>
  </tr>
  <tr>
    <td class="tg-ewmi" colspan="4"></td>
  </tr>
  <tr>
    <td class="tg-ewmi">Ursus</td>
    <td class="tg-ewmi">13.09</td>
    <td class="tg-ewmi">10:00</td>
    <td class="tg-ewmi">4-5 lat</td>
  </tr>
  <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi">11:00</td>
    <td class="tg-ewmi">6-7 lat</td>
  </tr>
 <!-- <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi">12:00</td>
    <td class="tg-ewmi">8-9 lat</td>
  </tr>-->
  <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi">13:00</td>
    <td class="tg-ewmi">10-11 lat</td>
  </tr>
    <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi">14:00</td>
    <td class="tg-ewmi">8-9 lat (dodatkowa)</td>
  </tr>
  <tr>
    <td class="tg-ewmi" colspan="4"></td>
  </tr>
  <tr>
    <td class="tg-ewmi">Konstancin<br></td>
    <td class="tg-ewmi">12.09</td>
    <td class="tg-ewmi">11:00</td>
    <td class="tg-ewmi">4-5 lat</td>
  </tr>
  <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi">12:00</td>
    <td class="tg-ewmi">6-7 lat</td>
  </tr>
  <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi">13:00</td>
    <td class="tg-ewmi">8-9 lat</td>
  </tr>
  <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi">14:00</td>
    <td class="tg-ewmi">10-11 lat</td>
  </tr>
    <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi">15:00</td>
    <td class="tg-ewmi">12-14 lat</td>
  </tr>
  <tr>
    <td class="tg-ewmi" colspan="4"></td>
  </tr>
  <tr>
    <td class="tg-ewmi">Grodzisk</td>
    <td class="tg-ewmi">19.09</td>
    <td class="tg-ewmi">11:00</td>
    <td class="tg-ewmi">wszystkie poziomy</td>
  </tr>
    <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
  </tr>
    <tr>
    <td class="tg-ewmi">Kraków</td>
    <td class="tg-ewmi">10.09</td>
    <td class="tg-ewmi">18:20</td>
    <td class="tg-ewmi">wszystkie poziomy</td>
  </tr>
      <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi">12.09</td>
    <td class="tg-ewmi">09:30</td>
    <td class="tg-ewmi">wszystkie poziomy</td>
  </tr>
      <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi">17.09</td>
    <td class="tg-ewmi">18:20</td>
    <td class="tg-ewmi">wszystkie poziomy</td>
  </tr>
      <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
  </tr>
    <tr>
    <td class="tg-ewmi">Słupsk</td>
    <td class="tg-ewmi">18.09</td>
    <td class="tg-ewmi">16:00</td>
    <td class="tg-ewmi"> 4-5 lat</td>
  </tr>
      <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi">17:45</td>
    <td class="tg-ewmi">8-9 lat</td>
  </tr>
      <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
  </tr>
    <tr>
    <td class="tg-ewmi">Józefosław</td>
    <td class="tg-ewmi">5.09</td>
    <td class="tg-ewmi">11.00</td>
    <td class="tg-ewmi">wszystkie poziomy</td>
  </tr>
      <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi">13.00</td>
    <td class="tg-ewmi">wszystkie poziomy</td>
  </tr>  
</table></div>
                    </div>
                    <!-- end col6 -->
                </div>
                <!-- end container-->
            </div>
            <!-- end first tab -->
            <div id="tab-2" class="tab">
                <form action="dzien.php" name="landing-page-form" method="post" id="landing-page-form">
                    <input name="lesson" type="hidden" value="programming"/>
                    <h6 class="form-header">Informacje o uczestniku</h6>
                    <input required="required" type="text" id="first-name" name="first_name" placeholder="Imię dziecka">
                    <br>
                    <input required="required" type="text" id="second-name" name="second_name" placeholder="Nazwisko dziecka">
                    <br>
                    <select required="required" class="slct" name="child-age-programming">
                        <option disabled selected value="" id="wiek-dziecka">Wiek dziecka</option>
                        <option value="4-5 lat" name="firstAge">4-5 lat</option>
                        <option value="6-7 lat" name="secondAge">6-7 lat</option>
                        <option value="8-9 lat" name="thirdAge">8-9 lat</option>
                        <option value="10-11 lat" name="fourthAge">10-11 lat</option>
                    </select>
                    <br>
                    <h6 class="form-header">Dzień otwarty <span><a
                                    href="dzien.php#anchor-table">(harmonogram) </a></span></h6>
					<p style="width: 420px; font-size: 1.3em; color: rgb(255, 255, 255); padding-bottom: 5px;">Uprzejmie informujemy, że w przypadku zaznaczenia przez Państwa grupy oznaczonej jako pełnej, formularz staje się nieważny.</p>

                    <ul id="questions">

                    </ul>

                    <div id="preloader"></div>
                    <br>
                    <h6 class="form-header">Dane kontaktowe</h6>
                    <input required="required" type="email" id="mail" name="mail" placeholder="Twój email">
                    <br>
                    <input required="required" type="tel" id="phone" name="phone" placeholder="Telefon">
                    <br>
                    <input type="submit" value="Zapisz się!" name="submit" id="send">
                </form>
                <!-- end form -->

                <div class="col6" id="landing-text-section">
                    <h4>Już niebawem dzień otwarty w Matplanecie!</h4>
                    <h6>Nasze zajęcia rozpoczynają się od zadania praktycznego

                        problemu, na który odpowiedź może dać matematyka.

                        Problem jest analizowany w grupie, a dzieci same dochodzą

                        do rozwiązania. Liczy się przede wszystkim proces

                        myślowy, nie tylko jedynie słuszny wynik.</h6>
                    <img src="assets/images/IMG_4372.JPG">
                </div>
                <!-- end col6 -->

                <div class="col12" id="harmonogram">
                    <h6 id="anchor-table"></h6><!-- empty h6 for anchor -->
                    <div class="col12">
                        <h4 class="table-header">Harmonogram</h4>
						
						
						<div class="tg-wrap"><table class="tg">
  <tr>
    <th class="tg-ewmi">Miejsce</th>
    <th class="tg-ewmi">Data</th>
    <th class="tg-ewmi">Godzina</th>
    <th class="tg-ewmi">Grupa</th>
  </tr>
  <tr>
    <td  class="tg-ewmi">Kraków</td>
    <td  class="tg-ewmi">10.09</td>
    <td  class="tg-ewmi">18:20</td>
    <td  class="tg-ewmi">wszystkie poziomy</td>
  </tr>
  <tr>
    <td  class="tg-ewmi"></td>
    <td  class="tg-ewmi">12.09</td>
    <td  class="tg-ewmi">09:30</td>
    <td  class="tg-ewmi">wszystkie poziomy</td>
  </tr>
  <tr>
    <td  class="tg-ewmi"></td>
    <td  class="tg-ewmi">17.09</td>
    <td  class="tg-ewmi">18:20</td>
    <td  class="tg-ewmi">wszystkie poziomy</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td  class="tg-ewmi">Ursynów</td>
    <td  class="tg-ewmi">08.09</td>
    <td  class="tg-ewmi">18:20</td>
    <td  class="tg-ewmi">wszystkie poziomy</td>
  </tr>
  <tr>
    <td  class="tg-ewmi"></td>
    <td  class="tg-ewmi">18.09</td>
    <td  class="tg-ewmi">16:30</td>
    <td  class="tg-ewmi">wszystkie poziomy</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td  class="tg-ewmi">Tarchomin</td>
    <td  class="tg-ewmi">10.09</td>
    <td  class="tg-ewmi">18:20</td>
    <td  class="tg-ewmi">wszystkie poziomy</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td  class="tg-ewmi">Bemowo</td>
    <td  class="tg-ewmi">16.09</td>
    <td  class="tg-ewmi">18:20</td>
    <td  class="tg-ewmi">wszystkie poziomy</td>
  </tr>
    <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td  class="tg-ewmi">Grodzisk</td>
    <td  class="tg-ewmi">19.09</td>
    <td  class="tg-ewmi">14:30</td>
    <td  class="tg-ewmi">wszystkie poziomy</td>
  </tr>
    <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td  class="tg-ewmi">Konstancin</td>
    <td  class="tg-ewmi">12.09</td>
    <td  class="tg-ewmi">14:00</td>
    <td  class="tg-ewmi">10-11 lat</td>
  </tr> 
  <tr>
    <td  class="tg-ewmi"></td>
    <td  class="tg-ewmi"></td>
    <td  class="tg-ewmi">15:00</td>
    <td  class="tg-ewmi">12-14 lat</td>
  </tr> 
  <tr>
    <td class="tg-ewmi">Józefosław</td>
    <td class="tg-ewmi">5.09</td>
    <td class="tg-ewmi">11.00</td>
    <td class="tg-ewmi">wszystkie poziomy</td>
  </tr>
      <tr>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi"></td>
    <td class="tg-ewmi">13.00</td>
    <td class="tg-ewmi">wszystkie poziomy</td>
  </tr>  
  
</table></div>
						
						
                        <!-- end list-table -->
                        <div><!-- end col12 -->
                        </div>
                    </div>
                    <!-- end col6 -->
                </div>
                <!-- end container-->
</section><!-- end section -->
<!-- get footer -->