<!-- get header -->
<?php require("inc/header.php");?>

<!-- get sidebar-->
<?php require("inc/sidebar.php");?>


    <!--news-section-->
    <section id="news-section" class="wrapper">
        <div class="container">
            <div class="section-header">
                <h2>
                    Aktualności
                </h2>
                <img src="assets/images/border.png" alt="border">
            </div>
            <div id="news-box" class="clear-row-3 cushycms">
                <div class="phGallary">
                    <ul id="gallary">
                        <li>
							<div>
                                <div class="col12 news" id="news7">                                   
                                    <h5 class="news-header">Matplaneta na Rowerowym Pikniku!</h5>
                                    <p>W sobotę 29. sierpnia 2015 r. w godzinach 11:00 – 18:00 w Centrum Handlowym Galerii Bemowo odbędzie się piknik na zakończenie wakacji.<br>
										Szukajcie nas na stanowisku z grami i zadaniami logicznymi.<br>
										Jak zawsze czeka na Was moc atrakcji i świetnej zabawy oraz konkurs, w którym można wygrać wspaniałe rowery miejskie.<br><br>
										Do zobaczenia:)</p>
									<p><a href="https://www.facebook.com/events/1482966322022055/" target="_blank">więcej:</a></p>    
                                    <p class="news-date always-bottom">2015-08-26</p>
								</div>
							</div>
						    <div>
                                <div class="col12 news" id="news7">                                   
                                    <h5 class="news-header">Miło nam poinformować, że w Matplanecie uruchomiliśmy nasz własny system  komunikacji z klientami - Polygon.</h5>
                                    <p>W systemie są dostępne funkcjonalności pozwalające m.in. na:<br>
									•	zapisanie dziecka na zajęcia,<br>
									•	wysłanie maila do Nauczyciela lub Administratora oddziału,<br>
									•	sprawdzenie kwot i terminów opłat oraz przejrzenie uiszczonych wpłat,<br>
									•	aktualizację swoich danych kontaktowych.<br></p>
									<p>W menu „Rekomendacja” nasi stali Klienci znajdą proponowaną grupę dla  dziecka na I semestr 2015/2016 i skrót do Zapisów.</p>
									<p>Pełna oferta zajęć znajduje się w menu „Zapisy”.</p>
									<p>Wierzymy, że system będzie przyjazny dla Państwa i uprości proces zapisu  oraz kontakty z biurem.</p>
									<p>Zapraszamy do rejestrowania się i zapisywania na zajęcia!</p>
									<p><a href="http://matplaneta.pl/assets/download/System%20Obs%C5%82ugi%20Klienta%20POLYGON.pdf" target="_blank">Krótka instrukcja obsługi</a></p>
									<p>Z pozdrowieniami<br>
									Zespół Matplanety</p>     
                                    <p class="news-date always-bottom">2015-08-21</p>
								</div>
							</div>
						    <div>
                                <div class="col12 news" id="news8">
                                    <img style="max-width: 540px;" alt="informacje na temat matplanety" src="assets/images/harmonogram2015-08-20.png"/>
                                    <h5 class="news-header" id="">Proponowany harmonogram bezpłatnych lekcji próbnych</h5>
                                    <p class="news-date always-bottom-two">2015-08-20</p>
                                </div>
                            </div>
                            <div>
                                <div class="col12 news" id="news7">
                                    <img alt="informacje na temat matplanety" src="assets/images/367306-scratch-from-mit.jpg">

                                    <h5 class="news-header">Zapraszamy na zajęcia z programowania dla dzieci w wieku 8-9 lat w systemie Scratch</h5>

                                    <p>Scratch jest graficznym językiem programowania stworzonym do uczenia dzieci  i młodzieży podstaw

                                    programowania. Programy tworzy się w wyjątkowo łatwy, intuicyjny i atrakcyjny sposób poprzez 

                                    odpowiednie łączenie ze sobą bloczków. Scratch został zaprojektowany przez Mitchela Resnicka 

                                    (z Massachusetts Institute of Technology), który był pomysłodawcą klocków Lego MindStorms, więc 

                                    jest mocno związany z ideologią konstrukcji Lego.

                                    <br>

                                    Dzięki wykorzystaniu klocków Lego dzieci bezboleśnie zapoznają się ze Scratchem i poprzez zabawę 

                                    poznają podstawy programowania. Będziemy też programować aplikacje, które wykonują zadania 

                                    matematyczne. Program pozwala budować instrukcje warunkowe 

                                    oraz pętle i dzięki temu tworzenie własnych zaawansowanych 

                                    programów przez dzieci.

                                    <br>

                                    Według wielu ekspertów zajmujących się nowoczesną edukacją 

                                    umiejętność programowania w niedalekiej przyszłości może być tak 

                                    samo istotna jak umiejętność czytania, pisania, czy znajomość języka 

                                    angielskiego.
                                    
                                    <br>

                                    <p class="bold-paragraph">Naszym głównym celem jest nauczenie dzieci umiejętności programowania oraz logicznego 

                                    i algorytmicznego myślenia. Na zajęciach będziemy rozwijać praktyczne umiejętności komunikowania z komputerem.         </p>

                                    </p>

                                    <p class="news-date always-bottom">2015-08-03</p>
                            </div>
                            <div>
                                <div class="col12 news" id="news6">
                                    <img alt="informacje na temat matplanety" src="assets/images/IMG_4372.JPG">

                                    <h5 class="news-header">Wolne miejsca</h5>

                                    <p>Mamy jeszcze wolne miejsca na wakacjach z Robotami Lego.</p>

                                    <p class="news-date always-bottom">2015-08-03</p>
                            </div>
                            <div>
                                <div class="col12 news" id="news1">
                                    <img alt="informacje na temat matplanety" src="assets/images/Matplaneta%2004.jpg">

                                    <h5 class="news-header">Zmiana godzin otwarcia biura na Ursynowie</h5>

                                    <p>Informujemy, że w przyszłym tygodniu (6-11.07) biuro Matplanety na Ursynowie będzie czynne w godz. 10.00 - 15.00.</p>

                                    <p class="news-date always-bottom">2015-07-05</p>
                                </div>
                            </div>
                            <div>
                                <div class="col12 news" id="news2">
                                    <img alt="informacje na temat matplanety" src="assets/images/FM150620-Matematyka102.jpg"/>
                                    <h5 class="news-header" id="">II Festiwal Matematyki za nami</h5>

                                    <p>Tylu matematycznych atrakcji w jednym miejscu chyba dawno nie było. W sobotę 20 czerwca odbyła się druga edycja Festiwalu Matematyki organizowanego przez Gazetę Wyborczą.Matplaneta przygotowała wiele intrygujących zadań i zagadek, a podczas warsztatów o ciągu Fibonacciego poznawaliśmy drzewo genealogiczne rodziny pszczół, zastanawialiśmy się, czym jest złota spirala, poznawaliśmy tajemnice huraganów. Tu nie było szkolnych ławek i żadne dziecko nie przysypiało:)

                                    </p>

                                    <p>Zapraszamy do przeczytania relacji z Festiwalu:
                                        <a href="http://wyborcza.pl/1,137662,18181300,Detektywi__roboty_i_ziemniaki__czyli_matematyka_na.html">link</a>
                                        <br>
                                        i obejrzenia zdjęć: <a href="https://www.facebook.com/media/set/?set=a.506739226143323.1073741833.108368555980394&type=3">link</a>
                                    </p>

                                    <p class="news-date always-bottom-two">2015-06-29</p>
                                </div>
                            </div>
                            <div>
                                <div class="col12 news" id="news3">
                                    <img alt="informacje na temat matplanety" src="assets/images/IMG_4372.JPG"/>
                                    <h5 class="news-header">Ostatni dzwonek do zapisów na wakacje z robotami!</h5>

                                    <p>To ostatni dzwonek aby zapisać dzieci na Wakacje z robotami Lego na: Ursynowie, Tarchominie, w Józefosławiu i w Krakowie. Wakacje już tuż, tuż a miejsc z każdym dniem ubywa, więc warto się pospieszyć:)
                                        <a href="http://matplaneta.pl/zapisy.html" target="_blank">zapisy</a>

                                    </p>

                                    <p>Kto jeszcze nie widział, niech się sam przekona, że warto!:)
                                        <a href="https://vimeo.com/125895045">link</a>
                                    </p>

                                    <p class="news-date always-bottom">2015-06-29</p>
                                </div>
                            </div>
                            <li>
                            <div>
                                <div class="col12 news" id="news4">
                                    <img alt="informacje na temat matplanety" src="assets/images/news-04-27.jpg"/>
                                    <h5 class="news-header ">DigiKids Fundacji FSCD</h5>

                                    <p>23 maja Matplaneta wzięła udział w projekcie DigiKids Fundacji FSCD, podczas którego Pan Antek poprowadził świetne warsztaty dla dzieci od 6 do 12 lat na temat Ciągu Fibonacciego. Uczestnicy zajęć szukali złotej proporcji w otaczającym nas świecie i dowiadywali się co wspólnego mają muszle albo pszczoły z liczbą Fi. Dzieciaki wychodziły z zajęć uśmiechnięte i zadowolone. A my liczymy na dalszą, owocną współpracę.
                                        <a href="https://www.facebook.com/matplanetaPL/posts/494986233985289" target="_blank">Więcej</a></p>

                                    <p class="news-date always-bottom-second">2015-05-23</p>
                                </div>
                            </div>
                             <div>
                                <div class="col12 news" id="news5">
                                    <img alt="informacje na temat matplanety" src="assets/images/23baa932a90cdffae9fe70f4aac6ede5.jpg"/>
                                    <h5 class="news-header">Wakacje z robotami</h5>

                                    <p>Podobnie jak w ubiegłych latach, chcielibyśmy gorąco zaprosić Państwa dzieci na Wakacje z
                                        Robotami, a dokładniej - Programowanie Robot&oacute;w Lego Wedo (dla młodszych dzieci) i
                                        Lego Mindstorms EV3 (dla dzieci starszych). Jak co roku oferujemy całodniową opiekę nad
                                        dziećmi (od 8:30 do 17:30) wraz z posiłkami (drugie śniadanie, obiad i podwieczorek).
                                        Terminy, lokalizacje oraz ceny turnus&oacute;w wakacyjnych znajdą Państwo <a
                                            href="http://matplaneta.pl/formularze/zapisy-roboty.php" target="_blank">tutaj</a>
                                    </p>

                                    <p class="news-date always-bottom-second">2015-03-27</p>
                                </div>
                            </div>
                            </li><!-- end secong page -->
                        </li><!-- end first page -->
                    </ul>

                        <center>
                        <!-- pagination -->
                        <span id="PageList" class="clear"></span>
                        </center>
                </div>

            </div>
        </div>
    </section>

    <!-- get footer -->
<?php require("inc/footer.php");?>