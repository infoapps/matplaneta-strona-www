<!--footer-->
<footer>
    <div id="main-footer" class="wrapper">
        <div class="main-border"></div>
        <div class="container">
            <div class="col6 main-links">
                <h6>
                    Kontakt
                </h6>
                <ul>
                    <li>
                        <span class="icon-home"></span> <span>Al. Komisji Edukacji Narodowej 95, klatka 18a, 2 piętro<br> (obok stacji metra STOKŁOSY)</span>
                    </li>
                    <li>
                        <span class="icon-phone"></span> <span>+48 22 100 53 47 <br>Godziny pracy: Pn-Pt: 15:00-20:00; Soboty: 8:00-15:00</span>
                    </li>
                    <li>
                        <span class="icon-envelope"></span> <a href="mailto:biuro@e-matplaneta.pl">biuro@e-matplaneta.pl</a>
                    </li>
                    <li>
                        <span class="icon-earth"></span> <a href="">www.matplaneta.pl</a>
                    </li>
					<li>
                        <span class="icon-facebook"></span> <a href="https://www.facebook.com/matplanetaPL">Facebook</a>
                    </li>
					<li>
                        <span class="icon-youtube"></span> <a href="https://www.youtube.com/channel/UCcRz2mlREkdixm1WaXMoTqQ">YouTube</a>
                    </li>		
					<li>
                        <span class="icon-linkedin"></span> <a href="https://www.linkedin.com/company/4863251?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A4863251%2Cidx%3A2-1-2%2CtarId%3A1441209633906%2Ctas%3Amatpl">LinkedIn</a>
                    </li>					
                </ul>
            </div>
            <div class="col6 footer-links">
                <h6>
                    Lokalizacje
                </h6>
                <ul>
                    <h6 class="location-footer-name">Warszawa</h6>
                    <li>
                        <a href="/bemowo/">Bemowo</a>
                    </li>
					<li>
                        <a href="/bialoleka/">Białołęka</a>
                    </li>
                    <li>
                        <a href="/mokotow/">Mokotów</a>
                    </li>
                    <li>
                        <a href="/tarchomin/">Tarchomin</a>
                    </li>
                    <li>
                        <a href="/ursus/">Ursus</a>
                    </li>
                    <li>
                        <a href="/ursynow/">Ursynów</a>
                    </li>
                    <li>
                        <a href="/wilanow/">Wilanów</a>
                    </li>

                    <br>
                    <br>
					<br>
					<br>
                    <h6 class="location-footer-name">Okolice Warszawy</h6>
                    <li>
                        <a href="/grodzisk/">Grodzisk Maz.</a>
                    </li>
                    <li>
                        <a href="/jozefoslaw/">Józefosław</a>
                    </li>
                    <li>
                        <a href="/jozefow/">Józefów</a>
                    </li>
                    <li>
                        <a href="/konstancin/">Konstancin</a>
                    </li>
					<li>
                        <a href="/radzymin/">Radzymin</a>
                    </li>


                    <br>
                    <br>
                    <h6 class="location-footer-name">Inne miasta</h6>
                    <li>
                        <a href="/gdansk/">Gdańsk</a>
                    </li>
                    <li>
                        <a href="/krakow/">Kraków</a>
                    </li>	
					<li>
                        <a href="/slupsk/">Słupsk</a>
                    </li>
                    <li>
                        <a href="/zary/">Żary</a>
                    </li>					
                </ul>
            </div>
        </div>
    </div>
    <div id="sub-footer">
        <div class="container">
            <div class="col4">
                <span id="copyright">Copyright © 2014 <a href="http://infoapps.pl/">InfoApps</a></span>
            </div>
            <div id="footer-nav" class="col8 footer-links">
                <ul>
                    <li>
                        <a href="/index.php#about-section">O nas</a>
                    </li>
                    <li>
                        <a href="/index.php#gallery-section">Galeria</a>
                    </li>
                    <li>
                        <a href="/index.php#lesson-section">Zajęcia</a>
                    </li>
                    <li>
                        <a href="/index.php#contact-section">Kontakt</a>
                    </li>
                    <li>
                        <a href="/aktualnosci/">News</a>
                    </li>
                    <li>
                        <a href="/sukcesy/">Sukcesy</a>
                    </li>
                    <li>
                        <a href="/lokalizacje/">Lokalizacje</a>
                    </li>
                    <li>
                        <a href="/szkoly/">Szkoły</a>
                    </li>
                    <li>
                        <a href="/praca/">Praca</a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</footer>
</div>
<script src="/scripts/script.js" type="text/javascript"></script>
<script src="/scripts/popup.js"></script>
<script src="/scripts/Pagination.js"></script>
<script type="text/javascript" src="/scripts/dropmenu.js"></script>
<script src="/assets/js/chosen/chosen.jquery.min.js"></script>
<script src="/assets/js/script.js"></script>
<script src="/assets/js/scriptSecond.js"></script>
<script src="/scripts/vendors/angular.min.js"></script>
<script src="/scripts/harmonogramController.js"></script>

</body>
</html>