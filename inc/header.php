<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta charset="UTF-8">
    <title>Matplaneta - zajęcia pozaszkolne z zakresu matematyki i programowania komputerowego</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="InfoApps">
    <meta name="description"
          content="Matplaneta to instytucja edukacyjna, która prowadzi zajęcia pozaszkolne z zakresu matematyki i programowania komputerowego. Zajęcia przeznaczone są dla dziecie w wieku od 5 do 14 lat i mają na celu zwiększenie ich zdolności rozwiązywania problemów.">
    <meta name="keywords"
          content="koło matematyczne, matematyka, math circle, matplaneta, programowanie komputerów, warszawa, zajęcia dla dzieci, zajęcia komputerowe, zajęcia z matematyki">
    <link type="text/css" href="/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/js/chosen/chosen.css" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,latin-ext" rel="stylesheet"
          type="text/css">
    <meta name="google-site-verification" content="9NGOo3kKTgl1GyOsn0EiX6J8sAuJlysIZ9Ozaqrvc9s" />
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-5445667-4', 'auto');
        ga('send', 'pageview');
    </script>
    
    <script src="//code.jquery.com/jquery-1.6.3.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
     <script type="text/javascript">
        $(document).ready(function() {
            $("#gallary").Pagination();
        });
    </script>
</head>
<body>