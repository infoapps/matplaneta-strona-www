<div class="location-harmonogram">
    <div ng-app="matApp">

        <div ng-controller="HarmonogramController">
            <div class="col12 text-center">
                <h3 class="section-header">Harmonogram</h3>
                <select ng-model="search['aHarmonogramPozycja_DzienTygodnia']">
                    <option value="">Dzień zajęć</option>
                    <option value="Poniedziałek">Poniedziałek</option>
                    <option value="Wtorek">Wtorek</option>
                    <option value="Środa">Środa</option>
                    <option value="Czwartek">Czwartek</option>
                    <option value="Piątek">Piątek</option>
                    <option value="Sobota">Sobota</option>
                    <option value="Niedziela">Niedziela</option>
                </select>

                <select ng-model="search['aHarmonogramPozycja_NazwaZajec']">
                    <option value="">Nazwa zajęć</option>
                    <option value="Matematyka">Matematyka</option>
                    <option value="Programowanie Komputerów">Programowanie Komputerów</option>
                </select>

                <select ng-model="search['aHarmonogramPozycja_GrupaWiekowa']">
                    <option value="">Grupa wiekowa</option>
                    <option value="Przedszkole">Przedszkole</option>
                    <option value="Podstawówka (6-7 lat)">Podstawówka (6-7 lat)</option>
                    <option value="Podstawówka (8-9 lat)">Podstawówka (8-9 lat)</option>
                    <option value="Podstawówka (10-11 lat)">Podstawówka (10-11 lat)</option>
                    <option value="Podstawówka/Gimnazjum (12-13 lat)">Podstawówka/Gimnazjum (12-13 lat)</option>
                </select>

                <select ng-model="search['aHarmonogramPozycja_GodzinaZajec']">
                    <option value="">Godzina zajęć</option>
                    <option value="08:">8</option>
                    <option value="09:">9</option>
                    <option value="10:">10</option>
                    <option value="11:">11</option>
                    <option value="12:">12</option>
                    <option value="13:">13</option>
                    <option value="14:">14</option>
                    <option value="15:">15</option>
                    <option value="16:">16</option>
                    <option value="17:">17</option>
                    <option value="18:">18</option>
                    <option value="19:">19</option>
                    <option value="20:">20</option>
                </select>
            </div>
            <div class="col6 harmonogram-table" ng-repeat="place in harmonogram | filter:locationFilter | filter:search">
                <table>
                    <tr>
                        <td>Dzień tygodnia:</td>
                        <td ng-bind="place['aHarmonogramPozycja_DzienTygodnia']"></td>
                    </tr>
                    <tr>
                        <td>Godzina zajęć:</td>
                        <td ng-bind="place['aHarmonogramPozycja_GodzinaZajec']"></td>
                    </tr>
                    <tr>
                        <td>Grupa wiekowa:</td>
                        <td ng-bind="place['aHarmonogramPozycja_GrupaWiekowa']"></td>
                    </tr>
                    <tr>
                        <td>Nazwa grupy:</td>
                        <td ng-bind="place['aHarmonogramPozycja_NazwaGrupy']"></td>
                    </tr>
                    <tr>
                        <td>Nazwa zajęć:</td>
                        <td ng-bind="place['aHarmonogramPozycja_NazwaZajec']"></td>
                    </tr>
                    <tr>
                        <td>Placówka:</td>
                        <td ng-bind="place['aHarmonogramPozycja_Placowka']"></td>
                    </tr>
                    <tr>
                        <td>Semestr:</td>
                        <td ng-bind="place['aHarmonogramPozycja_Semestr']"></td>
                    </tr>
                </table>
            </div>
            <p class="text-center" ng-show="(harmonogram | filter:locationFilter | filter:search).length === 0">Nic
                nie znaleziono</p>
        </div>
    </div>
</div>