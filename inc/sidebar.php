
<div id="mobile-nav">
    <div id="phone-logo">
      
        <a href="index.php" title="Matplaneta, al. KEN 95, Warszawa"> <img src="/assets/images/matplaneta-phone.png"
                                                                  alt="Matplaneta zaj�cia pozaszkolne">
        </a>
    </div>
    <div id="menu-button">
        <img src="/assets/images/navmenu.png" alt="Phone navigation button">
    </div>
    <ul id="menu-list" style="display: none">
        <li class="dropdown">
            <a href="/index.php">Start</a>
        </li>
        <li>
            <a href="/aktualnosci/">Atkualności</a>
        </li>
        <li>
            <a href="/sukcesy/">Sukcesy</a>
        </li>
        <li>
            <a href="/lokalizacje/">Lokalizacje</a>
        </li>
        <li>
            <a href="/szkoly/">Szkoły</a>
        </li>
        <li>
            <a href="/praca/">Praca</a>
        </li>
        <li>
            <a href="/index.php#contact-section">Kontakt</a>
        </li>
        <li>
            <a href="/zapisy.php">Zapisy</a>
        </li>
W
    </ul>
</div>

<div id="side-bar">
    <div id="matplaneta-logo">
   
        <a href="/index.php" title="Matplaneta, al. KEN 95, Warszawa"> <img src="/assets/images/matplaneta.jpg"
                                                                           alt="Matplaneta zaj�cia pozaszkolne">
        </a>
    </div>
    <nav id="main-nav">
        <ul>
            <li>
                <a href="/index.php">Start</a>
                <ul class="first-submenu" style="overflow: visible!important;">
                    <li><a href="/index.php#about-section">O nas</a></li>
					<li><a href="/index.php#slider-section">Mówią o nas</a></li>
                    <li><a href="/index.php#gallery-section">Galeria</a></li>
                    <li><a href="/index.php#lesson-section">Zajęcia</a></li>
                </ul>
            </li>
            <li><a href="/aktualnosci/">Aktualności</a></li>
            <li><a href="/sukcesy/">Sukcesy</a></li>
            <li>
                <a href="/lokalizacje/">Lokalizacje</a>
                <ul class="second-submenu" style="overflow: visible!important;">
                    <li>
                        <a href="/lokalizacje.php#anchor1">Warszawa</a>
                        <ul class="last-submenu" style="overflow: visible!important;">
                            <li><a href="/bemowo/">Bemowo</a></li>
							<li><a href="/bialoleka/">Białołęka</a></li>
                            <li><a href="/mokotow/">Mokotów</a></li>
                            <li><a href="/tarchomin/">Tarchomin</a></li>
                            <li><a href="/ursus/">Ursus</a></li>
                            <li><a href="/ursynow/">Ursynów</a></li>
                            <li><a href="/wilanow/">Wilanów</a></li>							
                        </ul>
                    </li>
                    <li>
                        <a href="/lokalizacje.php#anchor2">Okolice Warszawy</a>
                        <ul class="last-submenu" style="overflow: visible!important;" id="second-last-submenu">
                            <li><a href="/grodzisk/">Grodzisk Maz.</a></li>
                            <li><a href="/jozefoslaw/">Józefosław</a></li>
                            <li><a href="/jozefow/">Józefów</a></li>
                            <li><a href="/konstancin/">Konstancin - Jeziorna</a></li>
							<li><a href="/radzymin/">Radzymin</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="/lokalizacje.php#anchor3">Inne miasta</a>
                        <ul class="last-submenu" style="overflow: visible!important;" id="third-last-submenu">
                            <li><a href="/gdansk/">Gdańsk</a></li>
                            <li><a href="/krakow/">Kraków</a></li>
							<li><a href="/slupsk/">Słupsk</a></li>							
                            <li><a href="/zary/">Żary</a></li>

                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="/szkoly/">Szkoły i Przedszkola</a></li>
            <li><a href="/praca/">Praca</a></li>
            <li><a href="/index.php#contact-section">Kontakt</a></li>
			<li><a href="http://polygon.matplaneta.pl/Login.aspx">Logowanie dla klientów</a></li>	        
        </ul>
    </nav>
    <div class="aside-button">
        <a href="/zapisy/">Zapisy i ceny</a>
    </div>
</div>
<div id="main-site">