<!-- get header -->
<?php require("inc/header.php");?>

<!-- get sidebar-->
<?php require("inc/sidebar.php");?>

<!--location-section-->
    <section id="location-section" class="wrapper">
        <div class="container">
            <div class="section-header">
                <h2>
                    Lokalizacje
                </h2>
                <img src="assets/images/border.png" alt="border">
            </div>

            <div class="col12">
                <h4 class="news-headline" id="anchor1">Warszawa</h4>
                <!-- Warsaw Location -->
                <ul>

                    <li>
                        <h6>
                            CE Matplaneta Bemowo
                        </h6>

                        <div>
                            Ośrodek Kultury i Edukacji SM Wola, ul. Powstańców Śląskich 108A,
                            <br>
                            Ośrodek Kultury i Edukacji SM Wola, ul. Bolkowska 2c 
                            <br>
                            Tel. +48 22 100 53 47
                            <br>
                        </div>
                        <a class="location-button" href="bemowo.php">Zapisy</a>
                    </li>

                    <li>
                        <h6>
                            CE Matplaneta Mokotów
                        </h6>

                        <div>
                            
                            Ene-Due Centrum Językowo-Kulturowe dla Dzieci,
                            Ul. Chodkiewicza 8,
                            </br>
                            Warszawa Osiedle Eko-Park 
                            <br>
                            Tel. +48 22 646 59 97
                        </div>
                        <a class="location-button" href="mokotow.php">Zapisy</a>
                    </li>

                    <li>
                        <h6>
                            CE Matplaneta Tarchomin
                        </h6>

                        <div>
                            
                            ul. Myśliborska 98G, 
                            <br>
                            Tel. +48 22 747 16 85
                        </div>
                        <a class="location-button" href="tarchomin.php">Zapisy</a>
                    </li>

                    <li>
                        <h6>
                            CE Matplaneta Ursus
                        </h6>

                        <div>
                            
                            Dom Kultury Kolorowa, ul. gen. K. Sosnkowskiego 16, parter
                            <br>
                            Tel. +48 501 489 849
                        </div>
                        <a class="location-button" href="ursus.php">Zapisy</a>
                    </li>

                    <li>
                        <h6>
                            CE Matplaneta Ursynów
                        </h6>

                        <div>
                            
                            Al. Komisji Edukacji Narodowej 95, 
                            <br>
                            tel. +48 22 100 53 47
                        </div>
                        <a class="location-button" href="ursynow.php">Zapisy</a>
                    </li>

                </ul><!-- end Warsaw Location -->
            </div>

            <div class="col12">
                <h4 class="news-headline" id="anchor2">Okolice Warszawy</h4>
                <!-- start near Warsaw -->
                <ul>

                    <li>
                        <h6>
                            CE Matplaneta Grodzisk Mazowiecki
                        </h6>

                        <div>
                            
                            PAWILON KULTURY - ul. Westfala 3
                            <br>
                            05-825 Grodzisk Mazowiecki 
                            <br>
                            Tel.+48 22 100 53 47

                        </div>
                        <a class="location-button" href="grodzisk.php">Zapisy</a>
                    </li>

                    <li>
                        <h6>
                            CE Matplaneta Józefosław
                        </h6>

                        <div>
                            
                            ul. Julianowska 67A,
                            <br>+48 514 727 753
                        </div>
                        <a class="location-button" href="jozefoslaw.php">Zapisy</a>
                    </li>

                    <li>
                        <h6>
                            CE Matplaneta Józefów
                        </h6>

                        <div>
                            
                            ul.Wawerska 49,
                            <br>
                            Tel +48 600 920 129
                        </div>
                        <a class="location-button" href="jozefow.php">Zapisy</a>
                    </li>

                    <li>
                        <h6>
                            CE Matplaneta Konstancin Jeziorna
                        </h6>

                        <div>
                            
                            ul. Sobieskiego 6, 05-510 Konstancin-Jeziorna
                            <br>
                            Tel.+48 22 100 53 47
                        </div>
                        <a class="location-button" href="konstancin.php">Zapisy</a>
                    </li>

                </ul><!-- end near warsaw -->
            </div>
            <div class="col12">
                <h4 class="news-headline" id="anchor3">Pozostałe miasta</h4>
                <!-- start other location -->
                <ul>

                    <li>
                        <h6>
                            CE Matplaneta Trójmiasto
                        </h6>

                        <div>
                            
                            ul. Warneńska 8c/3, 80-288 Gdańsk 
                            <br>
                            Tel.+48 508 129 042
                        </div>
                        <a class="location-button" href="gdansk.php">Zapisy</a>
                    </li>

                    <li>
                        <h6>
                            CE Matplaneta Kraków
                        </h6>

                        <div>
                            
                                ul. Torfowa 4,Ruczaj, Gdańsk,
                            <br>Dworek Białoprądnicki, ul. Papiernicza 2, Prądnik Biały,
                            <br>Klub Kultury "Mydlniki", ul. Balicka 289, Bronowice,
                            <br>Tel. +48 22 100 53 47
                        </div>
                        <a class="location-button bottom-button" href="krakow.php">Zapisy</a>
                    </li>

                    <li>
                        <h6>
                            CE Matplaneta Żary
                        </h6>

                        <div>
                            
                            Przy Katolickiej Szkole Podstawowej, Kalinowskiego 15,
                            <br>Tel.+48 608 318 744
                        </div>
                        <a class="location-button" href="zary.php">Zapisy</a>
                    </li>

                </ul><!-- end other location -->
            </div>
    </section><!-- end location-section -->

<!-- get footer -->
<?php require("inc/footer.php");?>