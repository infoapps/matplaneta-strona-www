<!-- get header -->
<?php 
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require("$root/inc/header.php");?>

<!-- get sidebar-->
<?php require("$root/inc/sidebar.php");?>

<!-- start location -->
            <section id="location" class="wrapper">
                <div class="container">
                    <div class="section-header">
                        <h2> Gdańsk </h2>
                        <img src="/assets/images/border.png" alt="border">
                    </div>
                    <div class="col12">
                        <a href="http://polygon.matplaneta.pl/Oferta.aspx?region=trojmiasto" class="zapisy-button">Zapisy</a>
                    </div>
                    <div class="col12 row"></div>
                    <div class="col6">
                        <img class="img-responsive" src="/assets/images/szyld.jpeg" alt="Zajęcia w Gdańsku">
                    </div>
                    <div class="col6">
                        <ul>
                            <li>
                                <h4> Centrum Edukacyjne Matplaneta Trójmiasto </h4></li>
                            <li>
                                <p> ul. Warneńska 8c/3, Gdańsk - Morena

                                    <br>Tel. +48 508 129 042 
                                    <br>Biuro obsługi klienta: +48 (22) 100 53 47 
                                    <br>mail: gdansk@e-matplaneta.pl  
                                    <br>mail: biuro@matplaneta.pl 
                                </p>
                            </li>
                        </ul>
                    </div>
					<div class="col12">
                        <ul>
                            <li>
                                <h5> Podmiot świadczący usługi edukacyjne: </h5></li>
                            <li>
                                <p>Matplaneta Trójmiasto Sp. z o.o., ul. Warnieńska 8C, lok. 3, 80-288 Gdańsk, NIP 9571079086</p>
                            </li>
                            <li>
                                <h5> Nr konta bankowego:</h5></li>
                            <li>
                                <p>11 1300 0000 2969 3333 6000 0001 (Meritum Bank)</p>
                            </li>							
                        </ul>
					</div>

                    <div class="col12 row"></div>
                    <div class="col12">
                        <iframe src="https://www.google.com/maps/d/embed?mid=zdFV8ouvvHtY.kpoT9cW5IuCA&z=17" width="100%" height="400"></iframe>
                    </div>
                    <?php require("$root/inc/harmonogram.php");?>
                    <div class="col12">
                        <h4 class="news-headline"> Aktualności </h4>
                        <div class="local-news-container">
                            <div class="phGallary">
                                <ul id="gallary">
                                    <li>
										<div>
                                            <div class="local-news">
                                                <h6> 18-09-2015 </h6>
                                                <h5 class="news-header">Już jutro startujemy!</h5>
												<p>Już w tą sobotę (19.09) ruszamy z pierwszymi zajęciami w naszych placówkach.<br>
													Od samego rana grupy naszych starych oraz nowych uczniów rozpoczną przygodę z matematycznymi oraz logicznymi wyzwaniami. Będziemy programować pierwsze programy komputerowe oraz łamać głowy nad trudnymi zadaniami.<br>
													Wszyscy czekamy z niecierpliwością na jutrzejszy dzień i nie możemy się już doczekać!
												</p>    
                                            </div>
                                        </div><!-- end con-div -->	
										<div>
                                            <div class="local-news">
                                                <h6> 09-07-2015 </h6>
                                                <h5 class="news-header">Dni Otwarte 11 września</h5>
                                                <p class="second-news">Szanowni Rodzice!</p>
												<p>Z przyjemnością zapraszamy na Dni Otwarte naszej placówki. Do wyboru są dwa terminy: 11 września (piątek) oraz 16 września (środa), oba o godzinie 17:30. Podczas spotkania będą Państwo mogli obejrzeć naszą siedzibę, porozmawiać z zespołem oraz uzyskać odpowiedzi na wszystkie pytania związane z zajęciami w Matplanecie. Jednocześnie będą Państwo mogli skonsultować terminy zajęć i zapisać Wasze dziecko do odpowiedniej grupy.</p>
<p>Zapraszamy do zarezerwowania miejsca telefonicznie pod numerem 508 129 042 lub mailowo pod adresem gdansk@e-matplaneta.pl</p>
<p>Do zobaczenia!</p>
                                            </div><!-- end local-news -->
                                        </div><!-- end con-div -->
									    <div>
                                            <div class="local-news">
                                                <h6> 01-09-2015 </h6>
                                                <h5 class="news-header">Matplaneta na Festynie Rodzinnym</h5>
												
												<p>W ubiegłą sobotę Matplaneta gościła na Pikniku Rodzinnym na Morenie, w sąsiedztwie swojej siedziby. Dzieci i młodzież mogły skorzystać z cudownej pogody i w naszym specjalnym namiocie wziąć udział w zagadkach oraz grach matematyczno-logicznych. Wszystko w duchu Matplanety "Myślę, liczę, wiem!" </p>
												<p>Zapraszamy do obejrzenia zdjęć w naszym albumie na profilu <a href="https://www.facebook.com/media/set/?set=a.464886177022820.1073741831.429281780583260&type=3" >Facebook</a></p>     
                                            </div>
                                        </div><!-- end con-div -->											
									    <div>
                                            <div class="local-news">
                                                <h6> 21-08-2015 </h6>
                                                <h5 class="news-header">Miło nam poinformować, że w Matplanecie uruchomiliśmy nasz własny system komunikacji z klientami - Polygon.</h5>

												<p>W systemie są dostępne funkcjonalności pozwalające m.in. na:<br>
												•	zapisanie dziecka na zajęcia,<br>
												•	wysłanie maila do Nauczyciela lub Administratora oddziału,<br>
												•	sprawdzenie kwot i terminów opłat oraz przejrzenie uiszczonych wpłat,<br>
												•	aktualizację swoich danych kontaktowych.<br></p>
												<p>W menu „Rekomendacja” nasi stali Klienci znajda proponowaną grupę dla  dziecka na I semestr 2015/2016 i skrót do Zapisów.</p>
												<p>Pełna oferta zajęć znajduje się w menu „Zapisy”.</p>
												<p>Wierzymy, że system będzie przyjazny dla Państwa i uprości proces zapisu  oraz kontakty z biruem.</p>
												<p>Zapraszamy do rejestrowania się i zapisywania na zajęcia!</p>
												<p><a href="http://matplaneta.pl/assets/download/System%20Obs%C5%82ugi%20Klienta%20POLYGON.pdf" target="_blank">Krótka instrukcja obsługi</a></p>
												<p>Z pozdrowieniami<br>
												Zespół Matplanety</p>     
                                            </div>
                                        </div><!-- end con-div -->									
                                        <div>
                                            <div class="local-news">
                                                <h6> 19-06-2015 </h6>
                                                <h5 class="news-header">Jesteśmy w trakcie przygotowania planu zajęć Matplanety na rok szkolny 2015-2016. </h5>
                                                <p class="second-news">Zapraszamy do zapisów w drugiej połowie sierpnia.
                                                    Ale już teraz można wypełnić  formularz zgłoszenia chęci udziału dziecka w zajęciach Matplanety w roku 2015-2016.
                                                    Dzięki temu będziemy mogli stworzyć harmonogram zajęć najlepiej odpowiadający Państwa potrzebom.</p>
                                            </div><!-- end local-news -->
                                        </div><!-- end con-div -->
                                        <div>
                                            <div class="local-news">
                                                <h6> 02-06-2015 </h6>
                                                <h5 class="news-header">W miniony piątek (29 maja) w SP nr 1 w Gdańsku odbył się wspaniały Festyn Rodzinny!</h5>
                                                <p class="second-news">Również Matplaneta miała tam swoje stoisko. Była to pierwsza, a zarazem niebywale udana akcja nowopowstałej filii naszej Szkoły.
                                                    Dzieciaki na Pomorzu odkryły, że sudoku z figurami, tajne kody agentów czy gry planszowe na logikę i bystrość mogą dostarczać masę wrażeń i niezapomnianych emocji.
                                                    Mamy nadzieję, że dzięki Festynowi Matplaneta już od września przywita całe grupy młodych Pascali, Fermatów, czy Eulerów chętnych samodzielnie odkrywać niesamowity Świat Matematyki!</p>
                                            </div><!-- end local-news -->
                                        </div><!-- end con-div -->
                                        <div>
                                            <div class="local-news">
                                                <h6> 01-06-2015 </h6>
                                                <h5 class="news-header">W nowym roku szkolnym Matplaneta podbije Gdańsk!</h5>

                                                <p> Po wakacjach w Gdańsku rozpocznie swoją działalność pierwsza na Pomorzu placówka Matplanety. Zapraszamy do zapisów pod adresem gdansk@e-matplaneta.pl</p>
                                            </div><!-- end local-news -->
                                        </div><!-- end con-div -->
                                    </li><!-- end first-page -->
                                </ul><!-- end gallary -->
                                <center>
                                    <span id="PageList" class="clear"></span>
                                </center>
                            </div><!-- end phGallary -->
                        </div><!-- end local-news-container -->
                    </div><!-- end col12 -->
                </div><!-- end container -->
            </section>

<!-- get footer -->
<?php require("$root/inc/footer.php");?>