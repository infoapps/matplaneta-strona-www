<!-- get header -->
<?php $root = realpath($_SERVER["DOCUMENT_ROOT"]);
require("$root/inc/header.php");?>

<!-- get sidebar-->
<?php require("$root/inc/sidebar.php");?>


    <!--news-section-->
    <section id="news-section" class="wrapper">
        <div class="container">
            <div class="section-header">
                <h2>
                    Aktualności
                </h2>
                <img src="/assets/images/border.png" alt="border">
            </div>
            <div id="news-box" class="clear-row-3 cushycms">
                <div class="phGallary">
                    <ul id="gallary">
                        <li>
							<div>
                                <div class="col12 news" id="news7">  
									<img src="/assets/images/attachment6577.jpeg">								
                                    <h5 class="news-header">Wydrukuj własny świat w Matplanecie - Projektowanie 3D</h5>
									<p>
									Podczas kursu uczestnicy będą mieli możliwość zaprojektować i stworzyć, czyli wydrukować na drukarce 3D, własne wymyślone miasto. <br><br>
									Inżynier Konstruktor, Artysta Postaci, Architekt, Projektant Postaci do gry komputerowej i/albo animacji komputerowej - te wszystkie zawody wykorzystują narzędzie do projektowania - modeler. Modelowanie 3D to proces tworzenia obiektów trójwymiarowych za pomocą programu komputerowego.<br><br>
									Poznaj moc takich programów do modelowania jak Wings 3D, Sculptris, Tinkercad i Blender! Nauczysz się różnych podejść do modelowania oraz dowiesz się, jak używać Slicer'ów (Cura, Sli3er...). Slicer to program do dzielenia obiektu na warstwy i generowania kodu zrozumiałego przez drukarkę 3D. <br> 
									Proponujemy 8 spotkań po 90 minut. <br>
									Cena kursu : 480 zł lub w 2 ratach po 250 zł. Dla naszych słuchaczy zniżka 15%.<br>
									Terminy: 1 grupa - wtorki 18.20 , 2 grupa - czwartki 16.30 <br>
									Lokalizacja: Centrum Edukacyjne Matplaneta Kraków - Ruczaj, ul. Torfowa 4. Dla dzieci i młodzieży od 8 lat, tel.  <br>
									Zaczynamy 8 marca!
									</p> 
                                    <p class="news-date always-bottom">2016-02-29</p>
								</div>
							</div>
							<div>
                                <div class="col12 news" id="news7">  
									<img src="/assets/images/STATUETKA_BANER_2016.jpg">								
                                    <h5 class="news-header">GŁOSOWANIE!!!</h5>
									<p>Gorąco zachęcamy wszystkich do oddania głosu na Matplanetę w Plebiscycie na Statuetkę Warszawskiej Rodzinki Prudential.<br><br>
W konkursie walczymy o miano miejsca z najbardziej bogatą, atrakcyjną i wartościową ofertą skierowaną do dzieci i młodzieży:)<br><br>
Zagłosować należy przez stronę:<a href="http://www.warszawskarodzinka.pl/">http://www.warszawskarodzinka.pl/</a> <br><br>
Z góry dziękujemy za każdy oddany głos:)


									</p> 
                                    <p class="news-date always-bottom">2016-02-11</p>
								</div>
							</div>
							<div>
                                <div class="col12 news" id="news7">  
									<img src="/assets/images/FM150620-Matematyka269.jpg">								
                                    <h5 class="news-header">DNI OTWARTE</h5>
									<p>W Krakowie już 22 i 26 lutego odbędą się lekcje pokazowe Matplanety w szkołach.<br><br>
A na początku marca wszystkich zainteresowanych zajęciami w Matplanecie zapraszamy na bezpłatne Dni Otwarte w Centrach Edukacyjnych na Ursynowie, Tarchominie, Bemowie oraz w Krakowie.<br><br>
Terminy, godziny oraz szczegółowe informacje znajdziecie tutaj: <a href="http://matplaneta.pl/dni_zapisy/public/dni_otwarte">http://matplaneta.pl/dni_zapisy/public/dni_otwarte.</a> <br><br>
Zapraszamy do zapisów!!!
									</p> 
                                    <p class="news-date always-bottom">2016-02-10</p>
								</div>
							</div>
							<div>
                                <div class="col12 news" id="news7">  
									<img src="/assets/images/12631365_582238058593439_5656559635220966633_n.jpg">								
                                    <h5 class="news-header">PRZERWA ZIMOWA</h5>
									<p>Informujemy, że w czasie Ferii Zimowych (1-14.02) zajęcia Matplanety nie odbywają się. W tym czasie organizujemy Ferie z Robotami Lego, Programowaniem Gier Komputerowych oraz Grafiką Komputerową.<br><br>
									Do zobaczenia 15.02:)</br></br>
									W Krakowie natomiast zapraszamy na zajęcia już 1 lutego!
									</p> 
                                    <p class="news-date always-bottom">2016-02-01</p>
								</div>
							</div>
							<div>
                                <div class="col12 news" id="news7">  
									<img src="/assets/images/IMG_7072.JPG">								
                                    <h5 class="news-header">WARSZTATY DLA RODZICÓW</h5>
                                    <p>
Zapraszamy na warsztaty dla Rodziców. Pierwsze spotkanie p.t. „Jak odrabiać z dzieckiem pracę domową?” organizujemy w naszym Centrum na Ursynowie w dniach 20 stycznia o godz. 18.20 oraz 28 stycznia o godzinie 16.30. Koszt spotkania 90 min – 30 zł. Zgłoszenie wybranego terminu prosimy przekazać do naszego biura pod nr telefonu 22 100 5347.
W razie, jakichkolwiek pytań prosimy o kontakt pod nr telefonu 22 100 5347, 730 660 770.
									</p> 
                                    <p class="news-date always-bottom">2016-01-13</p>
								</div>
							</div>
							<div>
                                <div class="col12 news" id="news7">    
                                    <h5 class="news-header">DNI OTWARTE W KRAKOWIE!!!</h5>
                                    <p>
Już 30 stycznia 2016 od godz. 9:00 zapraszamy wszystkie dzieci na  lekcję pokazową do Matplanety w Centrum Ruczaj
(ul. Torfowa 4 - przecznica Kobierzyńskiej).<br><br>
A najbliższe lekcje pokazowe z Matplanetą w krakowskich szkołach odbędą się już w lutym!<br><br>

Szczegółowe informacje będą zamieszczane na stronie internetowej;)
									</p> 
                                    <p class="news-date always-bottom">2016-01-11</p>
								</div>
							</div>
							<div>
                                <div class="col12 news" id="news7">    
									<img src="/assets/images/rabat.png">
                                    <h5 class="news-header">UWAGA!!! RABAT 15% NA FERIE Z MATPLANETĄ</h5>
                                    <p>
Dla rodzeństw oraz uczniów naszych zajęć mamy specjalną ofertę!!!<br><br>
Wiedząc, że cena jednego turnusu Ferii w Matplanecie wynosi 680zł, rozwiążcie zagadkę ze zdjęcia:) Grafika komputerowa / Tworzenie gier komputerowych / Budowanie robotów mogą stać się pasją Twojego dziecka - Zapisz je już teraz: http://matplaneta.pl/zapisy-ferie-2016/
									</p> 
                                    <p class="news-date always-bottom">2016-01-11</p>
								</div>
							</div>
							<div>
                                <div class="col12 news" id="news7">    
									<img src="/assets/images/programowanie-ferie.jpg">
                                    <h5 class="news-header">FERIE Z MATPLANETĄ - ZAPISZ SIĘ!</h5>
                                    <p>
FERIE Z MATPLANETĄ już za niecały miesiąc, a Wy wciąż możecie dopisać swoje dziecko na:<br><br>

- ROBOTY LEGO WEDO<br>
- GRAFIKĘ KOMPUTEROWĄ <br>
- PROGRAMOWANIE GIER KOMPUTEROWYCH<br><br>

W czasie naszych ferii dzieci rozwiną wyobraźnię i logiczne myślenie oraz przeżyją wspaniałą przygodę, którą zapamiętają na długo!!! Zapiszcie się już dziś!<br><br>

Wystarczy wejść na stronę i wypełnić formularz: <a href="http://matplaneta.pl/zapisy-ferie-2016/">http://matplaneta.pl/zapisy-ferie-2016/</a><br><br>
Specjalne rabaty dla rodzeństwa oraz uczestników zajęć Matplanety:)
									</p> 
                                    <p class="news-date always-bottom">2016-01-05</p>
								</div>
							</div>
							<div>
                                <div class="col12 news" id="news7">    
                                    <h5 class="news-header">Przerwa świąteczna</h5>
                                    <p>Uprzejmie informujemy, że w dniach 24 grudnia 2015 - 6 stycznia 2016 mamy  przerwę świąteczną w Matplanecie. Na zajęcia zapraszamy 7 stycznia.<br><br> Miłego  odpoczynku!<br> Zespół Matplanety.
									</p> 
                                    <p class="news-date always-bottom">2015-12-29</p>
								</div>
							</div>
							<div>
                                <div class="col12 news" id="news7">    
									<img alt="informacje na temat matplanety" src="/assets/images/Lucek zimowy-02.png" style="max-width: 130px;">
                                    <h5 class="news-header">ŚWIĄTECZNE WIEŚCI Z MATPLANETY</h5>
                                    <p>W grudniu ukazał się już 4 numer gazetki Matplanety!<br><br>
A w nim:<br>
- krótkie podsumowanie minionych miesięcy,<br>
- zapowiedź wyczekiwanych przez wszystkich FERII ZIMOWYCH<br>
- krótka wzmianka o świątecznych promocjach oraz o planowanych warsztatach dla rodziców,<br>
- a przede wszystkim ZAGADKI, ZAGADKI, ZAGADKI!!!<br><br>

W Święta nie pozwalamy się nudzić:)<br><br>

<a target="_blank" href="http://matplaneta.pl/assets/download/gazetka_nr_4_grudzien.pdf">Pobierz gazetkę</a><br>
									</p> 
                                    <p class="news-date always-bottom">2015-12-16</p>
								</div>
							</div>
							<div>
                                <div class="col12 news" id="news7">    
									<img alt="informacje na temat matplanety" src="/assets/images/Lucek zimowy-02.png" style="max-width: 130px;">
                                    <h5 class="news-header">FERIE Z MATPLANETĄ</h5>
                                    <p>Najlepsza przygoda dla każdego dziecka!<br><br>
Wszystkie dzieci, które zostają w mieście chociaż przez jeden tydzień ferii, zapraszamy na fantastyczne zajęcia w Matplanecie. Proponujemy całodniową opiekę, zajęcia w pracowni komputerowej, gry i zabawy nie tylko matematyczne, pyszne posiłki i mnóstwo zabawy.<br><br>

Młodsze dzieci zapraszamy na spotkanie z robotami Lego.<br>
Dla nieco starszych proponujemy w tym roku nowości: Projektowanie gier komputerowych oraz Grafikę komputerową (również dla Dziewczynek!).<br><br>

Więcej informacji i zapisy: <a href="http://matplaneta.pl/zapisy-ferie-2016/">tutaj</a><br>
Zarezerwuj miejsce dla swojego dziecka już dziś!
									</p> 
                                    <p class="news-date always-bottom">2015-12-12</p>
								</div>
							</div>
							<div>
                                <div class="col12 news" id="news7">    
									<img alt="informacje na temat matplanety" src="/assets/images/big_Prezent-Com-Mk01.gif" style="max-width: 162px;">
                                    <h5 class="news-header">WIELKA ŚWIĄTECZNA PROMOCJA:)</h5>
                                    <p>Mamy dla Was pomysł na super prezent dla całej rodziny:<br><br>
- Dużo wspólnej zabawy! <br>
- Przyjemne z pożytecznym!<br>
- Rozwijają pasje i zainteresowania! <br><br>
Wiecie już co? <br><br>
Książki przybliżające świat matematyki i programowania.<br>
Rodzinna Matematyka (39zł), Python dla Dzieci (49zł), Javascript dla dzieci (49zł)<br>
Do dostania w placówkach Matplanety!!!
									</p> 
                                    <p class="news-date always-bottom">2015-12-12</p>
								</div>
							</div>
							<div>
                                <div class="col12 news" id="news7">    
									<img alt="informacje na temat matplanety" src="/assets/images/dzis-premiera-15.jpg">
                                    <h5 class="news-header">APLIKACJA MATPLANETY DO ŚCIĄGNIĘCIA</h5>
                                    <p>Uwaga, uwaga!!!<br><br>
Mamy zaszczyt ogłosić, że od dziś na każdym urządzeniu z Androidem może się wyświetlić nowa ikonka aplikacji: MATPLANETA!!!<br><br>
Wystarczy wpisać Matplaneta! Zainstalować! I grać!!!<br><br>
Zobaczymy, kto jeszcze dziś wieczorem będzie na podium?
									</p> 
                                    <p class="news-date always-bottom">2015-11-16</p>
								</div>
							</div>						
							<div>
                                <div class="col12 news" id="news7">    
									<img alt="informacje na temat matplanety" src="/assets/images/premiera-13.jpg">
                                    <h5 class="news-header">PREMIERA APLIKACJI "MATPLANETA"</h5>
                                    <p>16 listopada odbędzie się PREMIERA APLIKACJI MATPLANETY!.<br><br>
										A w niej czeka nas mnóstwo logicznych zadań i emocjonujących wyzwań zarówno dla 5,6 jak i 12-latków. Chociaż zapewne rodzice też mogą mieć z nimi niemały problem;) Wystarczy za darmo ściągnąć aplikację i cieszyć się kolejnymi matematycznymi zwycięstwami! Jesteśmy przekonani, że aplikacja dostarczy dzieciom wielu wrażeń i emocji, a przy okazji rozwinie u nich logiczne myślenie!!!

<br><br>Główkuj! Licz! Wygrywaj!
									</p> 
                                    <p class="news-date always-bottom">2015-11-04</p>
								</div>
							</div>
							<div>
                                <div class="col12 news" id="news7">    
									<img alt="informacje na temat matplanety" src="/assets/images/Matplaneta%2004.jpg">
                                    <h5 class="news-header">31.10 - Matplaneta otwarta</h5>
                                    <p>W dniu 31.10 zajęcia w Matplanecie odbywają zgodnie z planem.<br>
										W razie pytań zachęcamy do kontaktu z biurem: biuro@matplaneta.pl.
									</p> 
                                    <p class="news-date always-bottom">2015-10-20</p>
								</div>
							</div>
							<div>
                                <div class="col12 news" id="news7">    
									<img style="width: 200px;" src="/assets/images/Rodzinna-matematyka_Kamila-Lyczek,images_product,15,978-83-01-18306-6.jpg">
                                    <h5 class="news-header">Rodzinna matematyka. Łamigłówki, które rozwijają i bawią</h5>
                                    <p>Drodzy Rodzice przedstawiamy Wam wspaniałą książkę, która jest odpowiedzią na nasze odwieczne pytania:
									<br><br>"Jak spędzić z naszymi dziećmi czas wartościowo, bawiąc się i ucząc? Jaka jest nasza rola w rozwiązywaniu zadań z dziećmi?". Książka RODZINNA MATEMATYKA napisana przez nauczycielkę Matplanety - Kamilę Łyczek jest już do kupienia w księgarniach oraz Centrach Edukacyjnych Matplanety.
									<br>Serdecznie zachęcamy do nabycia!
									<br><br>A więcej o samej książce i jej idei znajdziecie tutaj: <a href="http://abc.tvp.pl/21492249/rodzinna-matematyka-lamiglowki-ktore-rozwijaja-i-bawia" target="_blank">http://abc.tvp.pl/21492249/rodzinna-matematyka-lamiglowki-ktore-rozwijaja-i-bawia</a>
									</p> 
                                    <p class="news-date always-bottom">2015-10-18</p>
								</div>
							</div>
							<div>
                                <div class="col12 news" id="news7">    
									<img style="height: 200px; width: 200px;" src="/assets/images/actus.jpg">
                                    <h5 class="news-header">Dzień Otwarty w Świetlicy ACTUS</h5>
                                    <p>Już w najbliższą środę (14.10) świętujemy Dzień Nauczyciela razem ze Świetlicą dla dzieci ACTUS.<br>
									<a href="https://www.facebook.com/Actus.Swietlica?fref=ts" target="_blank">https://www.facebook.com/Actus.Swietlica?fref=ts</a><br><br>
									Zapraszamy na lekcję pokazową Matplanety tym razem na Kabaty - ul.Pod Lipą 4/6!<br>
									*środa o 17:00<br>
									*dzieci w wieku 6-7 oraz 8-9 lat <br><br>
									ZAPRASZAMY!!! <a href="https://www.facebook.com/events/908706639197793/" target="_blank">https://www.facebook.com/events/908706639197793/</a>
									</p> 
                                    <p class="news-date always-bottom">2015-10-09</p>
								</div>
							</div>
							<div>
                                <div class="col12 news" id="news7">    
									<img src="/assets/images/słoneczniki.jpg">
                                    <h5 class="news-header">Matplaneta - najbardziej rozwojową warszawską inicjatywą roku 2015 w kategorii LOGIKA.</h5>
                                    <p>Matplaneta - najbardziej rozwojową warszawską inicjatywą roku 2015 w kategorii LOGIKA. Złoty Słonecznik w naszych rękach!!!<br>
Dziękujemy wszystkim za oddane głosy;)<br><br>
Więcej informacji oraz zdjęcia:<a href="http://sloneczniki.czasdzieci.pl/aktualnosci/id,82-sloneczniki_2015_czyli.html" target="_blank"> http://sloneczniki.czasdzieci.pl/aktualnosci/id,82-sloneczniki_2015_czyli.html</a>

									</p> 
                                    <p class="news-date always-bottom">2015-09-28</p>
								</div>
							</div>
							<div>
                                <div class="col12 news" id="news7">    
                                    <h5 class="news-header">Już jutro startujemy!</h5>
                                    <p>Już w tą sobotę (19.09) ruszamy z pierwszymi zajęciami w naszych placówkach.<br>
Od samego rana grupy naszych starych oraz nowych uczniów rozpoczną przygodę z matematycznymi oraz logicznymi wyzwaniami. Będziemy programować pierwsze programy komputerowe oraz łamać głowy nad trudnymi zadaniami.<br>
Wszyscy czekamy z niecierpliwością na jutrzejszy dzień i nie możemy się już doczekać!
									</p> 
                                    <p class="news-date always-bottom">2015-09-18</p>
								</div>
							</div>
							<div>
                                <div class="col12 news" id="news7">    
                                    <h5 class="news-header">Złoty Słonecznik dla krakowskiej Matplanety!</h5>
                                    <p>W miniony piątek odebraliśmy w Krakowie wielkie wyróżnienie:) Podczas Gali zwieńczającej piątą edycję Festiwalu Słoneczniki - za najbardziej rozwojowe inicjatywy dla dzieci, został nam przyznany przez zespół ekspertów ZŁOTY SŁONECZNIK - w kategorii LOGIKA!!!<br>
									Gratulujemy!<br><br>
									więcej: <a href="http://sloneczniki.czasdzieci.pl/aktualnosci/id,81-sloneczniki_2015_czyli.html" target="_blank">http://sloneczniki.czasdzieci.pl/aktualnosci/id,81-sloneczniki_2015_czyli.html</a>
									</p> 
                                    <p class="news-date always-bottom">2015-09-10</p>
								</div>
							</div>
							<div>
                                <div class="col12 news" id="news7">    
									<img alt="informacje na temat matplanety" src="/assets/images/WDR_prudential_logo_2015.JPG">
                                    <h5 class="news-header">Matplaneta po raz kolejny na WDRach!</h5>
                                    <p>Z przyjemnością informujemy że w dniach 12-13 września  zapraszamy na darmowe zajęcia pokazowe w ramach Warszawskich Dni Rodzinnych Prudential!<br>
Matplaneta otwiera drzwi młodym naukowcom, matematykom, programistom i wszystkim dla których nauka jest wspaniałą przygodą i źródłem wspaniałych odkryć. Będziemy bawić się w agentów łamiących szyfry, wyścigi samochodow zagramy w nasze ulubione gry logiczne. Młodzi programiści zbudują roboty z klocków Lego i zaprogramują je na komputerze.<br>
Zapisów dokonujemy przez stronę: <a href="http://www.ezapisywdr.pl/" target="_blank">http://www.ezapisywdr.pl/</a><br>
W tych dniach będą otwarte nasze placówki na Ursynowie, Tarchominie oraz Bemowie (tylko 12.09).<br>
Do zobaczenia!</p> 
                                    <p class="news-date always-bottom">2015-09-03</p>
								</div>
							</div>
							<div>
                                <div class="col12 news" id="news7">                                   
                                    <h5 class="news-header">Matplaneta na Rowerowym Pikniku!</h5>
                                    <p>W sobotę 29. sierpnia 2015 r. w godzinach 11:00 – 18:00 w Centrum Handlowym Galerii Bemowo odbędzie się piknik na zakończenie wakacji.<br>
										Szukajcie nas na stanowisku z grami i zadaniami logicznymi.<br>
										Jak zawsze czeka na Was moc atrakcji i świetnej zabawy oraz konkurs, w którym można wygrać wspaniałe rowery miejskie.<br><br>
										Do zobaczenia:)</p>
									<p><a href="https://www.facebook.com/events/1482966322022055/" target="_blank">więcej:</a></p>    
                                    <p class="news-date always-bottom">2015-08-26</p>
								</div>
							</div>
						    <div>
                                <div class="col12 news" id="news7">                                   
                                    <h5 class="news-header">Miło nam poinformować, że w Matplanecie uruchomiliśmy nasz własny system  komunikacji z klientami - Polygon.</h5>
                                    <p>W systemie są dostępne funkcjonalności pozwalające m.in. na:<br>
									•	zapisanie dziecka na zajęcia,<br>
									•	wysłanie maila do Nauczyciela lub Administratora oddziału,<br>
									•	sprawdzenie kwot i terminów opłat oraz przejrzenie uiszczonych wpłat,<br>
									•	aktualizację swoich danych kontaktowych.<br></p>
									<p>W menu „Rekomendacja” nasi stali Klienci znajdą proponowaną grupę dla  dziecka na I semestr 2015/2016 i skrót do Zapisów.</p>
									<p>Pełna oferta zajęć znajduje się w menu „Zapisy”.</p>
									<p>Wierzymy, że system będzie przyjazny dla Państwa i uprości proces zapisu  oraz kontakty z biurem.</p>
									<p>Zapraszamy do rejestrowania się i zapisywania na zajęcia!</p>
									<p><a href="http://matplaneta.pl/assets/download/System%20Obs%C5%82ugi%20Klienta%20POLYGON.pdf" target="_blank">Krótka instrukcja obsługi</a></p>
									<p>Z pozdrowieniami<br>
									Zespół Matplanety</p>     
                                    <p class="news-date always-bottom">2015-08-21</p>
								</div>
							</div>
						    <div>
                                <div class="col12 news" id="news8">
                                    <img style="max-width: 540px;" alt="informacje na temat matplanety" src="/assets/images/harmonogram2015-08-20.png"/>
                                    <h5 class="news-header" id="">Proponowany harmonogram bezpłatnych lekcji próbnych</h5>
                                    <p class="news-date always-bottom-two">2015-08-20</p>
                                </div>
                            </div>
                            <div>
                                <div class="col12 news" id="news7">
                                    <img alt="informacje na temat matplanety" src="/assets/images/367306-scratch-from-mit.jpg">

                                    <h5 class="news-header">Zapraszamy na zajęcia z programowania dla dzieci w wieku 8-9 lat w systemie Scratch</h5>

                                    <p>Scratch jest graficznym językiem programowania stworzonym do uczenia dzieci  i młodzieży podstaw

                                    programowania. Programy tworzy się w wyjątkowo łatwy, intuicyjny i atrakcyjny sposób poprzez 

                                    odpowiednie łączenie ze sobą bloczków. Scratch został zaprojektowany przez Mitchela Resnicka 

                                    (z Massachusetts Institute of Technology), który był pomysłodawcą klocków Lego MindStorms, więc 

                                    jest mocno związany z ideologią konstrukcji Lego.

                                    <br>

                                    Dzięki wykorzystaniu klocków Lego dzieci bezboleśnie zapoznają się ze Scratchem i poprzez zabawę 

                                    poznają podstawy programowania. Będziemy też programować aplikacje, które wykonują zadania 

                                    matematyczne. Program pozwala budować instrukcje warunkowe 

                                    oraz pętle i dzięki temu tworzenie własnych zaawansowanych 

                                    programów przez dzieci.

                                    <br>

                                    Według wielu ekspertów zajmujących się nowoczesną edukacją 

                                    umiejętność programowania w niedalekiej przyszłości może być tak 

                                    samo istotna jak umiejętność czytania, pisania, czy znajomość języka 

                                    angielskiego.
                                    
                                    <br>

                                    <p class="bold-paragraph">Naszym głównym celem jest nauczenie dzieci umiejętności programowania oraz logicznego 

                                    i algorytmicznego myślenia. Na zajęciach będziemy rozwijać praktyczne umiejętności komunikowania z komputerem.         </p>

                                    </p>

                                    <p class="news-date always-bottom">2015-08-03</p>
                            </div>
                            <div>
                                <div class="col12 news" id="news6">
                                    <img alt="informacje na temat matplanety" src="/assets/images/IMG_4372.JPG">

                                    <h5 class="news-header">Wolne miejsca</h5>

                                    <p>Mamy jeszcze wolne miejsca na wakacjach z Robotami Lego.</p>

                                    <p class="news-date always-bottom">2015-08-03</p>
                            </div>
                            <div>
                                <div class="col12 news" id="news1">
                                    <img alt="informacje na temat matplanety" src="/assets/images/Matplaneta%2004.jpg">

                                    <h5 class="news-header">Zmiana godzin otwarcia biura na Ursynowie</h5>

                                    <p>Informujemy, że w przyszłym tygodniu (6-11.07) biuro Matplanety na Ursynowie będzie czynne w godz. 10.00 - 15.00.</p>

                                    <p class="news-date always-bottom">2015-07-05</p>
                                </div>
                            </div>
                            <div>
                                <div class="col12 news" id="news2">
                                    <img alt="informacje na temat matplanety" src="/assets/images/FM150620-Matematyka102.jpg"/>
                                    <h5 class="news-header" id="">II Festiwal Matematyki za nami</h5>

                                    <p>Tylu matematycznych atrakcji w jednym miejscu chyba dawno nie było. W sobotę 20 czerwca odbyła się druga edycja Festiwalu Matematyki organizowanego przez Gazetę Wyborczą.Matplaneta przygotowała wiele intrygujących zadań i zagadek, a podczas warsztatów o ciągu Fibonacciego poznawaliśmy drzewo genealogiczne rodziny pszczół, zastanawialiśmy się, czym jest złota spirala, poznawaliśmy tajemnice huraganów. Tu nie było szkolnych ławek i żadne dziecko nie przysypiało:)

                                    </p>

                                    <p>Zapraszamy do przeczytania relacji z Festiwalu:
                                        <a href="http://wyborcza.pl/1,137662,18181300,Detektywi__roboty_i_ziemniaki__czyli_matematyka_na.html">link</a>
                                        <br>
                                        i obejrzenia zdjęć: <a href="https://www.facebook.com/media/set/?set=a.506739226143323.1073741833.108368555980394&type=3">link</a>
                                    </p>

                                    <p class="news-date always-bottom-two">2015-06-29</p>
                                </div>
                            </div>
                            <div>
                                <div class="col12 news" id="news3">
                                    <img alt="informacje na temat matplanety" src="/assets/images/IMG_4372.JPG"/>
                                    <h5 class="news-header">Ostatni dzwonek do zapisów na wakacje z robotami!</h5>

                                    <p>To ostatni dzwonek aby zapisać dzieci na Wakacje z robotami Lego na: Ursynowie, Tarchominie, w Józefosławiu i w Krakowie. Wakacje już tuż, tuż a miejsc z każdym dniem ubywa, więc warto się pospieszyć:)
                                        <a href="http://matplaneta.pl/zapisy.html" target="_blank">zapisy</a>

                                    </p>

                                    <p>Kto jeszcze nie widział, niech się sam przekona, że warto!:)
                                        <a href="https://vimeo.com/125895045">link</a>
                                    </p>

                                    <p class="news-date always-bottom">2015-06-29</p>
                                </div>
                            </div>
                            <li>
                            <div>
                                <div class="col12 news" id="news4">
                                    <img alt="informacje na temat matplanety" src="/assets/images/news-04-27.jpg"/>
                                    <h5 class="news-header ">DigiKids Fundacji FSCD</h5>

                                    <p>23 maja Matplaneta wzięła udział w projekcie DigiKids Fundacji FSCD, podczas którego Pan Antek poprowadził świetne warsztaty dla dzieci od 6 do 12 lat na temat Ciągu Fibonacciego. Uczestnicy zajęć szukali złotej proporcji w otaczającym nas świecie i dowiadywali się co wspólnego mają muszle albo pszczoły z liczbą Fi. Dzieciaki wychodziły z zajęć uśmiechnięte i zadowolone. A my liczymy na dalszą, owocną współpracę.
                                        <a href="https://www.facebook.com/matplanetaPL/posts/494986233985289" target="_blank">Więcej</a></p>

                                    <p class="news-date always-bottom-second">2015-05-23</p>
                                </div>
                            </div>
                             <div>
                                <div class="col12 news" id="news5">
                                    <img alt="informacje na temat matplanety" src="/assets/images/23baa932a90cdffae9fe70f4aac6ede5.jpg"/>
                                    <h5 class="news-header">Wakacje z robotami</h5>

                                    <p>Podobnie jak w ubiegłych latach, chcielibyśmy gorąco zaprosić Państwa dzieci na Wakacje z
                                        Robotami, a dokładniej - Programowanie Robot&oacute;w Lego Wedo (dla młodszych dzieci) i
                                        Lego Mindstorms EV3 (dla dzieci starszych). Jak co roku oferujemy całodniową opiekę nad
                                        dziećmi (od 8:30 do 17:30) wraz z posiłkami (drugie śniadanie, obiad i podwieczorek).
                                        Terminy, lokalizacje oraz ceny turnus&oacute;w wakacyjnych znajdą Państwo <a
                                            href="http://matplaneta.pl/formularze/zapisy-roboty.php" target="_blank">tutaj</a>
                                    </p>

                                    <p class="news-date always-bottom-second">2015-03-27</p>
                                </div>
                            </div>
                            </li><!-- end secong page -->
                        </li><!-- end first page -->
                    </ul>

                        <center>
                        <!-- pagination -->
                        <span id="PageList" class="clear"></span>
                        </center>
                </div>

            </div>
        </div>
    </section>

    <!-- get footer -->
<?php require("$root/inc/footer.php");?>