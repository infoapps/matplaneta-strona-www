<!-- get header -->
<?php 
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require("$root/inc/header.php");?>

<!-- get sidebar-->
<?php require("$root/inc/sidebar.php");?>

<!-- start location -->
            <section id="location" class="wrapper">
                <div class="container">
                    <div class="section-header">
                        <h2> Warszawa Józefów </h2>
                        <img src="/assets/images/border.png" alt="border">
                    </div>
                    <div class="col12">
                        <a href="http://polygon.matplaneta.pl/Oferta.aspx" class="zapisy-button">Zapisy</a>
                    </div>
                    <div class="col12 row"></div>
                    <div class="col6">
                        <img class="img-responsive" src="/assets/images/a.jpg" alt="Zajęcia w Józefowie">
                    </div>
                    <div class="col6">
                        <ul>
                            <li>
                                <h4> Centrum Edukacyjne Matplaneta Józefów </h4></li>
                            <li>
                                <p> ul. Zawiszy Czarnego 1c
									<br>Partner regionalny - Magdalena Nasiłowska
                                    <br>Tel. +48 600 920 129 
                                    <br>Biuro obsługi klienta: +48 (22) 100 53 47 
                                    <br>mail: jozefow@e-matplaneta.pl  
                                    <br>mail: biuro@e-matplaneta.pl
                                </p>
                            </li>
                        </ul>
                    </div>
					<div class="col12">
								<ul>
									<li>
										<h5> Podmiot świadczący usługi edukacyjne: </h5></li>
									<li>
										<p>Matplaneta SA, ul. Targowa 20a, 03-731 Warszawa, NIP 1132877516</p>
									</li>
									<li>
										<h5> Nr konta bankowego:</h5></li>
									<li>
										<p>38 1950 0001 2006 0889 8214 0002 (Idea Bank)</p>
									</li>							
								</ul>
					</div>

                    <div class="col12 row"></div>
                    <!-- maps -->
                    <div class="col12 maps">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2448.4091251455575!2d21.220239000000003!3d52.14506900000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47192b37d08cfa3f%3A0x1cd35e6a987bcc08!2sZawiszy+Czarnego+1%2C+J%C3%B3zef%C3%B3w!5e0!3m2!1spl!2spl!4v1442490692215" width="100%" height="400" >

                        </iframe>
                    </div>
					<?php require("$root/inc/harmonogram.php") ?>
                    <div class="col12">
                        <h4 class="news-headline"> Aktualności </h4>
                        <div class="local-news-container">
                            <div class="phGallary">
                                <ul id="gallary">
                                    <li>
										<div>
                                            <div class="local-news">
                                                <h6> 11-02-2016 </h6>
                                                <h5 class="news-header">GŁOSOWANIE!!!</h5>
												<p>Gorąco zachęcamy wszystkich do oddania głosu na Matplanetę w Plebiscycie na Statuetkę Warszawskiej Rodzinki Prudential.<br><br>
W konkursie walczymy o miano miejsca z najbardziej bogatą, atrakcyjną i wartościową ofertą skierowaną do dzieci i młodzieży:)<br><br>
Zagłosować należy przez stronę:<a href="http://www.warszawskarodzinka.pl/">http://www.warszawskarodzinka.pl/</a> <br><br>
Z góry dziękujemy za każdy oddany głos:)
									</p>   
                                        </div>
										<div>
                                            <div class="local-news">
                                                <h6> 01-02-2016 </h6>
                                                <h5 class="news-header">PRZERWA ZIMOWA</h5>
												<p>Informujemy, że w czasie Ferii Zimowych (1-14.02) zajęcia Matplanety nie odbywają się. W tym czasie organizujemy Ferie z Robotami Lego, Programowaniem Gier Komputerowych oraz Grafiką Komputerową.<br><br>
													Do zobaczenia 15.02:)
												</p>     
                                            </div>
                                        </div><!-- end con-div -->	
										<div>
                                            <div class="local-news">
                                                <h6> 16-12-2015 </h6>
                                                <h5 class="news-header">ŚWIĄTECZNE WIEŚCI Z MATPLANETY</h5>
												<p>W grudniu ukazał się już 4 numer gazetki Matplanety!<br><br>
													A w nim:<br>
													- krótkie podsumowanie minionych miesięcy,<br>
													- zapowiedź wyczekiwanych przez wszystkich FERII ZIMOWYCH<br>
													- krótka wzmianka o świątecznych promocjach oraz o planowanych warsztatach dla rodziców,<br>
													- a przede wszystkim ZAGADKI, ZAGADKI, ZAGADKI!!!<br><br>

													W Święta nie pozwalamy się nudzić:)<br><br>

													<a target="_blank" href="http://matplaneta.pl/assets/download/gazetka_nr_4_grudzien.pdf">Pobierz gazetkę</a><br>
												</p>     
                                            </div>
                                        </div><!-- end con-div -->	
										<div>
                                            <div class="local-news">
                                                <h6> 18-09-2015 </h6>
                                                <h5 class="news-header">Już jutro startujemy!</h5>
												<p>Już w tą sobotę (19.09) ruszamy z pierwszymi zajęciami w naszych placówkach.<br>
													Od samego rana grupy naszych starych oraz nowych uczniów rozpoczną przygodę z matematycznymi oraz logicznymi wyzwaniami. Będziemy programować pierwsze programy komputerowe oraz łamać głowy nad trudnymi zadaniami.<br>
													Wszyscy czekamy z niecierpliwością na jutrzejszy dzień i nie możemy się już doczekać!
												</p>    
                                            </div>
                                        </div><!-- end con-div -->	
										<div>
                                            <div class="local-news">
                                                <h6> 21-08-2015 </h6>
                                                <h5 class="news-header">Miło nam poinformować, że w Matplanecie uruchomiliśmy nasz własny system komunikacji z klientami - Polygon.</h5>

												<p>W systemie są dostępne funkcjonalności pozwalające m.in. na:<br>
												•	zapisanie dziecka na zajęcia,<br>
												•	wysłanie maila do Nauczyciela lub Administratora oddziału,<br>
												•	sprawdzenie kwot i terminów opłat oraz przejrzenie uiszczonych wpłat,<br>
												•	aktualizację swoich danych kontaktowych.<br></p>
												<p>W menu „Rekomendacja” nasi stali Klienci znajda proponowaną grupę dla  dziecka na I semestr 2015/2016 i skrót do Zapisów.</p>
												<p>Pełna oferta zajęć znajduje się w menu „Zapisy”.</p>
												<p>Wierzymy, że system będzie przyjazny dla Państwa i uprości proces zapisu  oraz kontakty z biruem.</p>
												<p>Zapraszamy do rejestrowania się i zapisywania na zajęcia!</p>
												<p><a href="http://matplaneta.pl/assets/download/System%20Obs%C5%82ugi%20Klienta%20POLYGON.pdf" target="_blank">Krótka instrukcja obsługi</a></p>
												<p>Z pozdrowieniami<br>
												Zespół Matplanety</p>     
                                            </div>
                                        </div><!-- end con-div -->
                                        <div>
                                            <div class="local-news">
                                                <h6> 19-06-2015 </h6>
                                                <h5 class="news-header">Jesteśmy w trakcie przygotowania planu zajęć Matplanety na rok szkolny 2015-2016. </h5>
                                                <p class="second-news">Zapraszamy do zapisów w drugiej połowie sierpnia.
                                                    Ale już teraz można wypełnić  formularz zgłoszenia chęci udziału dziecka w zajęciach Matplanety w roku 2015-2016.
                                                    Dzięki temu będziemy mogli stworzyć harmonogram zajęć najlepiej odpowiadający Państwa potrzebom.</p>
                                            </div><!-- end local-news -->
                                        </div><!-- end con-div -->
                                        <div>
                                            <div class="local-news">
                                                <h6> 25-05-2014 </h6>
                                                <h5 class="news-header">Jeszcze w tym semestrze możesz przyjść na Dzień Otwarty do Matplanety!</h5>

                                                <p> Zapraszamy wszystkich na Dzień Otwarty do Matplanety. Jest to świetna okazja, żeby spotkać się z nauczycielami w naszych oddziałach i dowiedzieć się, na czym polegają zajęcia.<br>
                                                    Ursynów - 10 czerwca, godz. 18:30<br>
                                                    Tarchomin - 12 czerwca, godz. 18:30<br>
                                                    Bemowo - 8 czerwca, godz. 18:30<br>
                                                    ZAPISY :  zapisy@e-matplaneta.pl
                                                </p>
                                            </div><!-- end local-news -->
                                        </div><!-- end con-div -->
                                        <div>
                                            <div class="local-news">
                                                <h6> 11-01-2014<br></h6>
                                                <h5 class="news-header"> Nowy semestr zajęć w Matplanecie rusza już 13 lutego! </h5>

                                                <p> Można jeszcze dopisywać dzieci niemalże do wszystkich grup w naszych placówkach, a
                                                    więc zapraszamy do zapoznania się z HARMONOGRAMEM.
                                                    <br>Dzieci, które chodziły na nasze zajęcia w tym semestrze, są automatycznie
                                                    przenoszone na kolejny.
                                                    <br>
                                                    <br>
                                                </p>
                                            </div><!-- end local-news -->
                                        </div><!-- end con-div -->
                                    </li><!-- end first-page -->
                                </ul><!-- end gallary -->
                                <center>
                                    <span id="PageList" class="clear"></span>
                                </center>
                            </div><!-- end phGallary -->
                        </div><!-- end local-news-container -->
                    </div><!-- end col12 -->
                </div><!-- end container -->
            </section>


<!-- get footer -->
<?php require("$root/inc/footer.php");?>