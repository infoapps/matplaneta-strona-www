<!-- get header -->
<?php require("inc/header.php");?>
<!-- get sidebar-->
<?php require("inc/sidebar.php");?>


            <!--start-section-->
            <div id="jumbotron">
                <div class="container">
                    <h1>
                        <strong>Uczymy dzieci myśleć oraz wierzyć,</strong> że są w stanie samodzielnie rozwiązywać problemy.
                    </h1>
                </div>
                <div class="main-border"></div>
            </div>
            <section id="start-section" class="wrapper">
                <div class="container">
					<div class="section-header" style="margin-bottom: 20px;">
						<h2>
							Zapisz się na
						</h2>
						<img src="assets/images/border.png" alt="border">
					</div>
					<div class="col4">
					<a href="http://polygon.matplaneta.pl/Oferta.aspx" class="zapisy" style="margin: 20px auto; height: 90px; font-weight: 900;" >Zajęcia z matematyki i programowania </a>
					
					</div>
					<div class="col4">
					<a href="http://matplaneta.pl/dni_zapisy/public/dni_otwarte" class="zapisy" style="margin: 20px auto; height: 90px; background: #FFA202; padding-top: 33px; font-weight: 900;">Dni otwarte </a>
					
					</div>
					<div class="col4">
					<a href="http://matplaneta.pl/zapisy-ferie-2016/" class="zapisy" style="margin: 20px auto; height: 90px; background: #53A881; padding-top: 33px; font-weight: 900;">Ferie z Matplanetą </a>
					
					</div>
					<div class="section-header">
						<h2>
							Dostępne zajęcia
						</h2>
						<img src="assets/images/border.png" alt="border">
					</div>
					
                    <div class="col4">
                        <span class="icon-heart"></span>
                        <h4>
                            <a href="#lesson-section">Matematyka </a>
</h4>

                        <p>
Poprzez matematykę uczymy dzieci samodzielnie myśleć, dostrzegać regularności oraz
                            rozwiązywać problemy!
                        </p>
                    </div>
                    <div class="col4">
                        <span class="icon-heart"></span>
                        <h4>
                            <a href="#lesson-section">Programowanie </a>
                        </h4>

                        <p>
Programując komputery dzieci zaczynają rozumieć pracę maszyny oraz uczą się formułowania
                            myśli w skrajnie logiczny sposób.
                        </p>
                    </div>
                    <div class="col4">
                        <span class="icon-heart"></span>
                        <h4>
                            <a href="#lesson-section">Roboty Lego </a>
                        </h4>

                        <p>
Projektowanie i programowanie robotów to wspaniała przygoda dla młodych naukowców oraz
                            wszystkich dzieci lubiących kreatywne zajęcia!
                        </p>
                    </div>
					<div class="col12">
						<h5 style="text-align: center; margin-top: 30px;">
						Gwarancja zdobycia 90% punktów z matematyki na egzaminie 6-klasisty dla naszych uczniów.
						</h5>
						<a href="/gwarancja/" class="zapisy" style="margin: 20px auto;">Gwarancja</a>
					</div>
                <!--</div>-->
                </div>
            </section>
            <!--about-section-->
            <section id="about-section" class="wrapper">
                <div class="container">
                    <div class="section-header">
                        <h2>
O nas
</h2>
                        <img src="assets/images/border.png" alt="border">
                    </div>
                    <div class="box">
                        <div id="box-1" class="box-mask small-hide">
                            <h3>
O firmie
</h3>
                        </div>
                        <div class="small-box">
                            <h4>
O firmie
</h4>

                            <p>
Naszym celem jest wykształcenie i rozwinięcie zdolności matematycznych u dzieci. Nie
                                stawiamy żadnych wymagań wejściowych do Matplanety.
                                <br>
                                <br>Są to zajęcia przeznaczone absolutnie dla wszystkich, którzy chcą na nie uczęszczać.
Nie są to zajęcia tylko dla osób uzdolnionych matematycznie, bo predyspozycje mają
                                wszystkie dzieci. Jeżeli tylko chcą iść w tym kierunku - zapraszamy serdecznie dzieci w
                                wieku 4-14 lat.
                                <br>
                                <br>Dla starszych dzieci oferujemy również zajęcia z programowania komputerowego i
                                budowania robotów.
                            </p>
                        </div>
                    </div>
                    <div class="box">
                    <div id="box-2" class="box-mask small-hide">
                        <h3>
O nauczycielach
</h3>
                    </div>
                    <div class="small-box">
                        <h4>
O nauczycielach
</h4>

                        <p>
Zespół Matplanety składa się z nauczycieli, administratorów oraz pozostałych
                            współpracowników, którzy wspólnie dbają o to, żeby codziennie zapewniać naszym uczniom
                            najlepsze warunki do efektywnej nauki i rozwoju!
                            <br>
                            <br>Grupę nauczycieli tworzą studenci lub absolwenci najlepszych polskich uczelni (m.in.
    Uniwersytet Warszawski, Politechnika Warszawska) z wykształceniem z zakresu nauk ścisłych.
Są to osoby otwarte, pełne pomysłów, odpowiedzialne i bardzo komunikatywne. Dzięki
                            wcześniejszym doświadczeniom w pracy z dziećmi mają świetne podejście do naszych uczniów i
                            potrafią ich zaciekawić. Każdego roku sami biorą również udział w różnego rodzaju
                            warsztatach oraz szkoleniach, pogłębiając swoją wiedzę i umiejętności.
                        </p>
                    </div>
                </div>
                <div class="box">
                <div id="box-3" class="box-mask small-hide">
                    <h3>
Franczyza
                    </h3>
                </div>
                <div class="small-box">
                    <h4>
Franczyza
                    </h4>

                    <p>
Chcesz mieć swój własny biznes, ale nie chcesz zaczynać kompletnie od zera? Czy też
                        zauważyłeś/zauważyłaś niedostatki systemu edukacji i chciałbyś/chciałabyś zaangażować się w
                        rozwój dzieci? A może masz własne dzieci i chcesz poświęcić im więcej czasu jednocześnie robiąc
                        coś co ma sens?
                        <br>
                        <br>Zapraszamy do przyłączenia się do nas!
                        <br>
                        <br>Otwórz swoją szkołę Matplanety. Pomożemy w wyborze lokalizacji, dobierzemy i wyszkolimy
                        Ciebie i pracowników, zapewnimy marketing, ale szefem będziesz Ty.
                        <br>
                        <br>Kontakt: biuro@e-matplaneta.pl
</p>
                </div>
        </div>
        <div class="box">
        <div id="box-4" class="box-mask small-hide">
            <h3>
Dla szkół i przedszkoli
</h3>
        </div>
        <div class="small-box">
            <h4>
Dla szkół i przedszkoli
</h4>

            <div>
                <p>
Nasze zajęcia umożliwiają rozwijanie wrodzonej u dzieci intuicji matematycznej. Klasyczny program
                    szkolny nie jest nakierowany na jej rozbudzenie - nasze zajęcia pozwalają dzieciom uwierzyć w siebie
                    i zrozumieć, że matematyka nie jest sztuką prowadzenia żmudnych rachunków, a jest sztuką
                    rozwiązywania problemów.
                    <br>
                    <br>W szkołach i przedszkolach prowadzimy zajęcia w dwóch modelach:
                </p>
                <ol>
                    <li>
W ramach programu matematyki w edukacji wczesnoszkolnej, gdzie część godzin matematyki
                        poświęcona jest na zajęcia Matplanety
</li>
                    <li>
Jako zajęcia dodatkowe, które realizowane są po zajęciach szkolnych
</li>
                </ol>
                <p>
Jeżeli jesteście Państwo zainteresowani wprowadzeniem zajęć Matplanety do Państwa szkoły, prosimy o
                    kontakt: biuro@e-matplaneta.pl
</p>
            </div>
        </div>
        </div>
        </div>
        </section>

		        <section id="slider-section" class="wrapper" style="display: block;">
            <div class="container">
                <div class="section-header">
                    <h2>
                        Mówią o nas
                    </h2>
                    <img src="assets/images/border.png" alt="border">
                </div>
                <div class="col12">
                    <ul class="rslides">
                        <li>
                            
                            <div>
							<img src="assets/images/slider1-vimeo.png" alt="">
							
                                <h3>Matplaneta na vimeo</h3>
                                <p>
                                    W Matplanecie na bieżąco tworzymy krótkie filmy i animacje, które idealnie oddają to, czym jest Matplaneta oraz pokazują najlepsze wspomnienia z naszych zajęć, wakacji oraz ferii.
                                
								</p>
								<a class="zapisy" href="https://vimeo.com/125895045" target="_blank" style="float: right;">Zobacz</a>
								
                            </div>
							
                        </li>
                        <li>
							
							<div>
							<img src="assets/images/slider2-tvp.png" alt="">
                                <h3>Matplaneta w TVP ABC </h3>
                                <p>
                                    Rok temu rozpoczęliśmy współpracę z dziecięcym kanałem TVP ABC.</br>
Wspólnie nakręciliśmy kilka odcinków programu "Masz Wiadomość", który przybliża dzieciom i młodzieży tajniki świata komputerów i internetu. Pierwsze odcinki, w których brali udział nasi uczniowie opowiadały o zakładaniu poczty w internecie, a następne pokazywały, jak ciekawie obrobić swoje zdjęcia w dostępnych aplikacjach oraz jak się chronić przed zagrożeniami w sieci.
                               
								</p>
								 <a class="zapisy" href="http://abc.tvp.pl/18484250/odc-2" target="_blank" style="float: right;">Zobacz</a>
                            </div>
							
						</li>
						<li>
                            
                            <div>
							<img src="assets/images/slider3-digikids.png" alt="">
                                <h3>Matplaneta i Digikids</h3>
                                <p>
                                    Od zeszłego roku Matplaneta została partnerem DigiKids - programu, który ma na celu rozbudzić w dzieciach naukową pasję poprzez pokazanie, że technologia i nauki ścisłe to fascynująca przygoda.
W ramach współpracy prowadzimy inspirujące warsztaty, które przybliżają dzieciom tajemnice matematycznego świata.
                                
								</p>
								<a class="zapisy" href="https://www.youtube.com/watch?v=06Sqb5tNccw" target="_blank" style="float: right;">Zobacz</a>
                            </div>
							
                        </li>
                        <li>
							
							<div>
							<img src="assets/images/slider4-radiokolor.png" alt="">
                                <h3>Radio Kolor</h3>
                                <p>
                                    Kiedy dzieci uczą się matematyki najefektywniej?</br>
Czy wszystkie dzieci nie lubią matematyki?</br>
Na czym tak naprawdę polega nauka matematyki i dlaczego warto, by dzieci zdobyły Matplanetę?:)</br>
Na te pytania odpowiadają w Radiu Kolor Eksperci z Matplanety.</br>
Od 31 sierpnia do 4 września 2015r. można było nas usłyszeć na 103 Fm!
                                </p>
								<a class="zapisy" href="http://radiokolor.pl/on-off-air-tresc/3795/matplaneta-centrum-rozwijajace-umiejetnosci-matematyczne-/" target="_blank" style="float: right;">Posłuchaj</a>
                            </div>
						</li>
                    </ul>
                </div>
            </div>
        </section>
        <!--lesson-section-->
        <section id="lesson-section" class="wrapper">
            <div class="container">
                <div class="section-header">
                    <h2>
Zajęcia
                    </h2>
                    <img src="assets/images/border.png" alt="border">
                </div>
                <div class="col12 tabs">
                    <ul id="tabs">
                        <li class="active">
                            <a href="#tab-1">Matematyka</a>
                        </li>
                        <li>
                            <a href="#tab-2">Programowanie</a>
                        </li>
                        <li>
                            <a href="#tab-3">Ferie z Matplanetą</a>
                        </li>
                    </ul>
                </div>
                <div id="tab-content" class="col12">
                    <div id="tab-1" class="tab active">
                        <img src="assets/images/b.jpg" alt="Zajęcia z matematyki">

                        <p class="visible">
Proponujemy kreatywne spotkania z matematyką i logiką prowadzone metodą twórczego
                            rozwiązywania problemów na podstawie programów Math Circle z Uniwersytetu Harvarda w USA,
                            których inicjatorami byli Ellen i Robert Kaplanowie. Na naszych zajęciach wyeliminowaliśmy
                            presję czasu oraz konkurencji, w zamian zaproponowaliśmy dzieciom zagadnienia z obszarów
                            matematyki, które są w niewystarczający sposób przerabiane w szkołach.
                            <br>
                            <br>Zajęcia rozpoczynają się od zadania praktycznego problemu, na który odpowiedź może dać
                            matematyka. Problem jest analizowany w grupie, a dzieci same mają dojść do sposobu
                            rozwiązania go. Każde dziecko ma prawo się wypowiedzieć.
                        </p>
                        <a href="" class="show-hidden">Więcej...</a>

                        <div class="hidden">
Prowadzący zajęcia jest mentorem, który ma za zadanie kierować myśli dzieci w odpowiednim
                            kierunku, podpowiadać i zapisywać ważne spostrzeżenia grupy. Prowadzący wspomaga również
                            zapisywanie matematyczne uzyskiwanego rozwiązania, dzięki czemu dzieci uczą się, w jaki
                            sposób zapis ułatwia wyrażenie rozwiązania problemu. Zapis i operacje matematyczne nie są
                            jednak celem, a tylko narzędziem w rozwiązaniu. Dzięki temu, że nie potrzebujemy formalnego
                            zapisu matematycznego, potrafimy z dziećmi intuicyjnie przerobić zagadnienia, które w szkole
                            pojawią się dużo później (nota bene, kiedy dziecko zatraci już intuicję...). Kształcimy
                            myślenie w ustrukturyzowany sposób, identyfikowanie regularności i prawideł rządzących
                            liczbami i zjawiskami, dzięki czemu poszerzamy horyzonty i powodujemy przyswajanie
                            naturalnych mechanizmów "wiem, jak się nad tym zastanowić", "wiem jak zadać pytanie" itp.
                            <br>
                            <br>Podstawowe korzyści jakie dzieci odniosą z zajęć matematyki w Matplanecie to:
                            <br>
                            <br>
                            <ul>
                                <li>
dzieci uczą się samodzielnie myśleć, dostrzegać regularności i rozwiązywać problemy;
                                </li>
                                <li>
dzieci lepiej uczą się matematyki szkolnej, ponieważ znają zasady ogólne i wiedzą
                                    skąd się one wywodzą;
                                </li>
                                <li>
dzieci umieją więcej; są w stanie ogarnąć znacznie szerszy program niż w szkole,
                                    dzięki temu, że niektóre proste koncepcje mogą zrozumieć bez ich mozolnego i
                                    skomplikowanego obliczania, które jest wymagane w szkole (np. całkowanie, liczby
                                    zespolone);
                                </li>
                                <li>
dzieci lepiej rozumieją matematykę, co ułatwi im zdawanie egzaminów i naukę na
                                    wyższych poziomach edukacji;
                                </li>
                                <li>
dzieci są przygotowane do rozwiązywania problemów niestandardowych nieomawianych w
                                    szkole, co daje im możliwość wzięcia udziału w konkursach matematycznych i
                                    olimpiadach.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div id="tab-2" class="tab">
                        <img src="assets/images/j.jpg" alt="Zajęcia z programowania">

                        <p class="visible">
Dla starszych dzieci oferujemy również zajęcia z programowania komputerowego – kolejnej
                            nauki, którą traktujemy jak narzędzie do nauki myślenia. Uczymy sztuki skutecznej
                            komunikacji z komputerem, tworzenia własnych aplikacji i gier. Komunikacja z komputerem uczy
                            skrajnej logiki, komputer nie wykona żadnej czynności, dopóki mu bardzo dokładnie i
                            logicznie nie wytłumaczymy, o co nam chodzi i do tego w języku zrozumiałym dla niego. Jest
                            to obszar, w którym spotykamy młodych geniuszy informatycznych. Dzieci tworzą własne
                            autorskie programy, w czym je wspomagamy merytorycznie, jak również dając do dyspozycji
                            świetnie wyposażone nowoczesne sale komputerowe.
                            <br>
                            <br>Podstawowe korzyści z naszych zajęć programowania komputerów to:
                            <br>
                            <br>
                        </p>
                        <a href="" class="show-hidden">Więcej...</a>

                        <div class="hidden">
                            <ul>
                                <li>
nauka formułowania problemu w sposób, który będzie zrozumiały dla maszyny i zamiana
                                    problemu w algorytm;
                                </li>
                                <li>d
                                    pozyskanie praktycznych umiejętności pisania aplikacji oraz tworzenia i publikowania
                                    stron internetowych;
                                </li>
                                <li>
nauka konsekwencji działania, wyrażania myśli przez zestaw komend danego języka
                                    programowania;
                                </li>
                                <li>
nauka logiki przez dzielenie zagadnienia na wyrażenia [jeżeli...to...w przeciwnym
                                    wypadku], tworzenie różnych rodzajów pętli i warunków;
                                </li>
                                <li>
nauka programowania obiektowego i graficznego, poznanie najnowszych dostępnych
                                    narzędzi programowania i systemów operacyjnych;
                                </li>
                                <li>
zdobycie umiejętności konfiguracji komputerów i wydobycia z nich nieznanych dotąd
                                    funkcji
                                    </li>
                            </ul>
                        </div>
                    </div>
                    <div id="tab-3" class="tab">
                        <img src="assets/images/i.jpg" alt="Ferie z Matplanetą">

                        <p class="visible">
<b>Ferie z Matplanetą to połączenie niezwykłych warsztatów komputerowych z bogatym programem matematycznych oraz ruchowych gier i zabaw!</b><br><br>
Zapewniamy świetną zabawę, miłą atmosferę, komfortowe warunki i pyszne posiłki.<br><br>

Młodsze dzieci zapraszamy na spotkanie z robotami Lego.<br>
Dla nieco starszych proponujemy w tym roku nowości: Projektowanie gier komputerowych oraz Grafikę komputerową (również dla Dziewczynek!).<br><br>

							
                        </p>
                        <a href="" class="show-hidden">Więcej...</a>

                        <div class="hidden">
<b>Dla dzieci 6-9 lat<br>
ROBOTY LEGO WEDO<br></b>
Lego WeDo to wspaniała okazja do wejścia w świat programowania! Dzieci same budują roboty, poznając przy tym zasady fizyki i matematyki. Ale zbudowanie robota to dopiero początek! Później dzieci go programują, aby w określonych warunkach wykonywał różne czynności, np. kopał piłkę czy grał na bębnach!<br><br>

<b>Dla dzieci od 10 lat<br>
PROGRAMOWANIE GIER KOMPUTEROWYCH<br></b>
Korzystając z ogólnodostępnych narzędzi, wspólnie poznamy cały proces powstawania gier – od projektowania przez programowanie aż do ich testowania i dystrybucji. Efektem końcowym zajęć, będzie własna, w pełni funkcjonalna gra
komputerowa!<br><br>

<b>GRAFIKA KOMPUTEROWA<br>
Podczas warsztatów nauczymy się podstawowej obsługi programu do tworzenia<br></b>
grafiki wektorowej Inkscape. Każdego dnia poznamy nowe narzędzia, dzięki którym dzieci same stworzą własne, niepowtarzalne gadżety, które każdy będzie mógł zabrać do domu!<br><br>

Dalsze szczegóły w zakładce  Zapisy i Ceny. <br>
<a class="zapisy" href="http://matplaneta.pl/zapisy-ferie-2016/">Zapisy na Ferie z Matplanetą</a>

                        </div>
                    </div>
                </div>
                <div class="col12">
                    <h4>
						Szczegółowy program zajęć dostępny <a class="program" href="assets/download/programy zajęć matematyka programowanie.pdf" target="_blank">tutaj</a>
                    </h4>
					<a href="http://matplaneta.pl/zapisy/" class="zapisy">Zapisy i ceny</a>
                </div>
            </div>
        </section>
        <!--gallery-section-->
        <section id="gallery-section" class="wrapper">
            <div class="section-header">
                <h2>
Galeria
                </h2>
                <img src="assets/images/border.png" alt="border">
            </div>
            <div id="gallery">
                <div class="gallery-item">
                <img src="assets/images/a.jpg" alt="gallery">

                <div class="gallery-mask small-hide">
                    <div class="mask-content">
                        <h5>
                            „U nas każdy ma prawo wybrać swoja drogę prowadzącą do rozwiązania problemu.”
                        </h5>

                        <p>
                        </p>
                    </div>
                </div>
            </div>
            <div class="gallery-item">
                <div class="gallery-mask small-hide">
                    <div class="mask-content">
                        <h5>
                            „Uczymy myślenia, a nie sztuki rozwiązywania testów.”
                        </h5>

                        <p>
                        </p>
                    </div>
                </div>
                <img src="assets/images/b.jpg" alt="gallery">
            </div>
            <div class="gallery-item">
                <img src="assets/images/c.jpg" alt="gallery">

                <div class="gallery-mask small-hide">
                    <div class="mask-content">
                        <h5>
                            „Na zajęciach każdy jest wysłuchany, ponieważ liczy się proces myślowy.”
                        </h5>

                        <p>
                        </p>
                    </div>
                </div>
            </div>
            <div class="gallery-item">
                <div class="gallery-mask small-hide">
                    <div class="mask-content">
                        <h5>
                            „Nasi nauczyciele to wyjątkowi, kreatywni, młodzi ludzie z pasją uczenia i
                            wspaniałym podejściem do dzieci.”
                        </h5>

                        <p>
                        </p>
                    </div>
                </div>
                <img src="assets/images/d.jpg" alt="gallery">
            </div>
            <div class="gallery-item">
                <img src="assets/images/e.jpg" alt="gallery">

                <div class="gallery-mask small-hide">
                    <div class="mask-content">
                        <h5>
                            „Nasze zajęcia zaczynamy już od dzieci w wieku przedszkolnym. One świetnie orientują
                            się w ułamkach, a po pewnym czasie odkrywają, że ich dodawanie to bardzo prosta
                            rzecz.”
                        </h5>

                        <p>
                        </p>
                    </div>
                </div>
            </div>
            <div class="gallery-item">
                <div class="gallery-mask small-hide">
                    <div class="mask-content">
                        <h5>
                            „Budując roboty, dzieci poznają zasady matematyki i fizyki.”
                        </h5>

                        <p>
                        </p>
                    </div>
                </div>
                <img src="assets/images/f.jpg" alt="gallery">
            </div>
            <div class="gallery-item">
                <img src="assets/images/g.jpg" alt="gallery">

                <div class="gallery-mask small-hide">
                    <div class="mask-content">
                        <h5>
                            „Matplaneta to miejsce, gdzie dzieci mogą wspaniale spędzać czas po lekcjach.”
                        </h5>

                        <p>
                        </p>
                    </div>
                </div>
            </div>
            <div class="gallery-item">
                <div class="gallery-mask small-hide">
                    <div class="mask-content">
                        <h5>
                            „Programowanie to nauka formułowania problemu w sposób, który będzie zrozumiały dla
                            maszyny i zamiana problemu w algorytm.”
                        </h5>

                        <p>
                        </p>
                    </div>
                </div>
                <img src="assets/images/h.jpg" alt="gallery">
            </div>
            <div class="gallery-item">
                <img src="assets/images/i.jpg" alt="gallery">

                <div class="gallery-mask small-hide">
                    <div class="mask-content">
                        <h5>
                            „W Matplanecie tworzy się i kieruje robotami, które chodzą, mówią, myślą i zrobią
                            wszystko, co tylko dzieci będą w stanie sobie wyobrazić!”
                        </h5>

                        <p>
                        </p>
                    </div>
                </div>
            </div>
            </div>
        </section>

        <!--contact-section-->
        <section id="contact-section" class="wrapper">
            <div class="container">
                <div class="section-header">
                    <h2>
Kontakt
                    </h2>
                    <img src="assets/images/border.png" alt="border">
                </div>
                <div id="contact-form">
                    <h4>
Masz pytania? Napisz.
                    </h4>

                    <form action="form.php" method="post" id="index-form">
                        <input id="name" type="text" name="user_name" placeholder="Imię i nazwisko" required="required">
                        <input id="mail" type="email" name="user_email" required="required" placeholder="E-mail">
                        <textarea id="msg" name="user_message" placeholder="Treść wiadomości"></textarea>
                        <input type="submit" name="submit" value="Prześlij">
                    </form>
                </div>
            </div>
        </section>

<!-- get footer -->
<?php require("inc/footer.php");?>

<script src="/scripts/responsiveslides.min.js"></script>