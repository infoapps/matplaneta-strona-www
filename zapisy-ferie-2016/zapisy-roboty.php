<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <meta charset="UTF-8">
        <title>Matplaneta - zajęcia pozaszkolne z zakresu matematyki i programowania komputerowego</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="author" content="InfoApps">
        <meta name="description"
              content="Matplaneta to instytucja edukacyjna, która prowadzi zajęcia pozaszkolne z zakresu matematyki i programowania komputerowego. Zajęcia przeznaczone są dla dziecie w wieku od 5 do 14 lat i mają na celu zwiększenie ich zdolności rozwiązywania problemów.">
        <meta name="keywords"
              content="koło matematyczne, matematyka, math circle, matplaneta, programowanie komputerów, warszawa, zajęcia dla dzieci, zajęcia komputerowe, zajęcia z matematyki">
        <link type="text/css" href="../css/main.css" rel="stylesheet">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,latin-ext" rel="stylesheet"
              type="text/css">
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-5445667-4', 'auto');
            ga('send', 'pageview');
        </script>
    </head>
    <body>
        <div id="mobile-nav">
            <div id="phone-logo">
                <a href="../index.html" title="Matplaneta, al. KEN 95, Warszawa"> <img
                        src="../assets/images/matplaneta-phone.png" alt="Matplaneta zajęcia pozaszkolne"> </a>
            </div>
            <div id="menu-button"><img src="../assets/images/navmenu.png" alt="Phone navigation button"></div>
            <ul id="menu-list" style="display: none">
                <li><a href="../index.html#about-section">O nas</a></li>
                <li><a href="../index.html#news-section">News</a></li>
                <li><a href="../index.html#lesson-section">Zajęcia</a></li>
                <li><a href="../index.html#gallery-section">Galeria</a></li>
                <li><a href="../index.html#location-section">Lokalizacje</a></li>
                <li><a href="../index.html#work-section">Praca</a></li>
                <li><a href="../index.html#contact-section">Kontakt</a></li>
                <li><a href="../zapisy.php">Zapisy</a></li>
            </ul>
        </div>
        <div id="side-bar">
            <div id="matplaneta-logo">
                <a href="../index.html" title="Matplaneta, al. KEN 95, Warszawa"> <img
                        src="../assets/images/matplaneta.jpg" alt="Matplaneta zajęcia pozaszkolne"> </a>
            </div>
            <nav id="main-nav">
                <ul>
                    <li><a href="../index.html">Start</a></li>
                    <li><a href="../index.html#about-section">O nas</a></li>
                    <li><a href="../index.html#news-section">Aktualności</a></li>
                    <li><a href="../index.html#lesson-section">Zajęcia</a></li>
                    <li><a href="../index.html#gallery-section">Galeria</a></li>
                    <li><a href="../index.html#location-section">Lokalizacje</a></li>
                    <li><a href="../index.html#work-section">Praca</a></li>
                    <li><a href="../index.html#contact-section">Kontakt</a></li>
                </ul>
            </nav>
            <div id="menu-buttons"><a href="../zapisy.php">Zapisy</a></div>
        </div>
        <div id="main-site">
            <section id="formularz" class="wrapper">
                <div class="section-header">
                    <h2>FERIE Z MATPLANETĄ</h2> <img src="../assets/images/border.png" alt="border"></div>
                <div class="container">
                    <p>To połączenie niezwykłych warsztatów komputerowych z bogatym programem matematycznych oraz ruchowych gier i zabaw! <br><br>

Nasze zajęcia rozwijają:<br>
• wyobraźnię<br>
• krytyczne myślenie<br>
• umiejętność logicznego formułowania i rozwiązywania problemów<br><br>

Dowiedz się więcej o programie naszych zajęć i zapisz swoje dziecko <a class="program" href="ulotka ferie.pdf" target="_blank">ulotka</a>

</p>

                    <p></p>

                    <form id="form" method="post" action="skrypt.php" enctype="multipart/form-data" style="max-width: 650px;">
                        <dl class="zend_form">
                            <dt id="form-label">&nbsp;</dt>
                            <dd id="form-element">
                                <fieldset id="fieldset-form">
                                    <legend>Formularz uczestnictwa w zajęciach</legend>
                                    <dl>
                                        <dd id="nazwiskodzieckaform-label">
                                            <label for="nazwiskodzieckaform" class="label required">Imię i nazwisko
                                                dziecka *</label>
                                        </dd>
                                        <dd id="nazwiskodzieckaform-element">
                                            <input type="text" name="nazwiskodzieckaform" id="nazwiskodzieckaform"
                                                   value="" size="50" class="text ui-corner-all">
                                        </dd>
                                        <dd id="nazwiskorodzicaform-label">
                                            <label for="nazwiskorodzicaform" class="label required">Imię i nazwisko
                                                rodzica *</label>
                                        </dd>
                                        <dd id="nazwiskorodzicaform-element">
                                            <input type="text" name="nazwiskorodzicaform" id="nazwiskorodzicaform"
                                                   value="" size="100" class="text ui-corner-all">
                                        </dd>
                                        <dd id="wiek-label">
                                            <label for="wiek" class="label required">Data urodzenia dziecka *</label>
                                        </dd>
                                        <dd id="wiek-element">
                                            <input type="date" name="wiek" id="wiek" value="" size="100"
                                                   class="text ui-corner-all hasDatepicker">
                                        </dd>
                                        <dd id="telefonform-label">
                                            <label for="telefonform" class="label required">Telefon kontaktowy rodzica
                                                *</label>
                                        </dd>
                                        <dd id="telefonform-element">
                                            <input type="text" name="telefonform" id="telefonform" value="" size="50"
                                                   class="text ui-corner-all">
                                        </dd>
                                        <dd id="emailform-label">
                                            <label for="emailform" class="label required">Adres E-mail *</label>
                                        </dd>
                                        <dd id="emailform-element">
                                            <input type="text" name="emailform" id="emailform" value=""
                                                   class="text ui-corner-all" size="30">
                                        </dd>
                                        <dd id="adresform-label">
                                            <label for="adresform" class="label required">Adres zamieszkania *</label>
                                        </dd>
                                        <dd id="adresform-element">
                                            <textarea name="adresform" id="adresform" cols="40"
                                                      class="text ui-corner-all" style="height:80px"
                                                      rows="10"></textarea>
                                        </dd>
                                    </dl>
                                </fieldset>
                            </dd>
                            <dt id="wakacjea-label">&nbsp;</dt>
                            <dd id="wakacjea-element">
                                <fieldset id="fieldset-wakacjea">
                                    <legend>Ursynów</legend>
                                    <dl>
                                        <div class="checkbox-wrap5">
                                            <label for="legowedoursynow" class="optional">Centrum Edukacyjne Matplaneta Ursynów, Aleja KEN 95. Grupy po max. 12 osób</label>
                                            <label><input type="radio" name="legowedoursynow"
                                                          id="legowedoursynow-1turnus"
                                                          value="1 turnus 1-5 lutego - Grupa I - Dzieci Młodsze (6-9 lat) - Warsztaty z Robotami Lego">1 turnus 1-5 lutego - Grupa I - Dzieci Młodsze (6-9 lat) - Warsztaty z Robotami Lego</label>
                                            <label><input type="radio" name="legowedoursynow"
                                                          id="legowedoursynow-2turnus" value="1 turnus 1-5 lutego - Grupa II - Dzieci Starsze (10-12 lat) - Projektowanie Gier Komputerowych">1 turnus 1-5 lutego - Grupa II - Dzieci Starsze (10-12 lat) - Projektowanie Gier Komputerowych</label>
                                            <label><input type="radio" name="legowedoursynow"
                                                          id="legowedoursynow-3turnus"
                                                          value="1 turnus 1-5 lutego - Grupa III - Dzieci Starsze (10-12 lat) - Grafika Komputerowa">1 turnus 1-5 lutego - Grupa III - Dzieci Starsze (10-12 lat) - Grafika Komputerowa</label>
                                            <label><input type="radio" name="legowedoursynow"
                                                          id="legowedoursynow-4turnus"
                                                          value="2 turnus 8-13 lutego - Grupa I - Dzieci Młodsze (6-9 lat) - Warsztaty z Robotami Lego">2 turnus 8-13 lutego - Grupa I - Dzieci Młodsze (6-9 lat) - Warsztaty z Robotami Lego</label>
                                            <label><input type="radio" name="legowedoursynow"
                                                          id="legowedoursynow-5turnus"
                                                          value="2 turnus 8-13 lutego - Grupa II - Dzieci Starsze (10-12 lat) - Projektowanie Gier Komputerowych">2 turnus 8-13 lutego - Grupa II - Dzieci Starsze (10-12 lat) - Projektowanie Gier Komputerowych</label>
                                            <label><input type="radio" name="legowedoursynow"
                                                          id="legowedoursynow-6turnus"
                                                          value="2 turnus 8-13 lutego - Grupa III - Dzieci Starsze (10-12 lat) - Grafika Komputerowa">2 turnus 8-13 lutego - Grupa III - Dzieci Starsze (10-12 lat) - Grafika Komputerowa</label>                            
										</div>
                                    </dl>
                                </fieldset>
                            </dd>
							<dt id="wakacje-label">&nbsp;</dt>
							<dd id="wakacjea-element">
                                <fieldset id="fieldset-wakacjea">
                                    <legend>Tarchomin </legend>
                                    <dl>
                                        <div class="checkbox-wrap5">
                                            <label for="legowedotarchomin" class="optional">Centrum Edukacyjne
                                                Matplaneta Tarchomin, ul. Myśliborska 98G</label>
                                            <label><input type="radio" name="legowedotarchomin"
                                                          id="legowedotarchomin-1turnus"
                                                          value="1 turnus 1-5 lutego - Grupa I - Dzieci Młodsze (6-9 lat) - Warsztaty z Robotami Lego">1 turnus 1-5 lutego - Grupa I - Dzieci Młodsze (6-9 lat) - Warsztaty z Robotami Lego</label>
                                            <label><input type="radio" name="legowedotarchomin"
                                                          id="legowedotarchomin-2turnus"
                                                          value="1 turnus 1-5 lutego - Grupa II - Dzieci Starsze (10-12 lat) - Projektowanie Gier Komputerowych">1 turnus 1-5 lutego - Grupa II - Dzieci Starsze (10-12 lat) - Projektowanie Gier Komputerowych</label>
                                            <label><input type="radio" name="legowedotarchomin"
                                                          id="legowedotarchomin-3turnus"
                                                          value="2 turnus 8-13 lutego - Grupa I - Dzieci Młodsze (6-9 lat) - Warsztaty z Robotami Lego">2 turnus 8-13 lutego - Grupa I - Dzieci Młodsze (6-9 lat) - Warsztaty z Robotami Lego</label>
                                            <label><input type="radio" name="legowedotarchomin"
                                                          id="legowedotarchomin-4turnus"
                                                          value="2 turnus 8-13 lutego - Grupa II - Dzieci Starsze (10-12 lat) - Projektowanie Gier Komputerowych">2 turnus 8-13 lutego - Grupa II - Dzieci Starsze (10-12 lat) - Projektowanie Gier Komputerowych</label>
                                        </div>
                                    </dl>
                                </fieldset>
                            </dd>
                            <dt id="wakacje-label">&nbsp;</dt>
                            <dd id="wakacje-element">
                                <fieldset id="fieldset-wakacje">
                                    <legend> Kraków</legend>
                                    <dl>
                                        <div class="checkbox-wrap5">
                                            <label for="legomindstorms" class="optional">Centrum Edukacyjne Matplaneta Kraków</label>
                                            <label><input type="radio" name="legomindstorms"
                                                          id="legomindstorms-1turnus"
                                                          value="1 turnus 18-22 stycznia - Grupa I - Dzieci Młodsze (6-9 lat) - Warsztaty z Robotami Lego">1 turnus 18-22 stycznia - Grupa I - Dzieci Młodsze (6-9 lat) - Warsztaty z Robotami Lego</label>
                                            <label><input type="radio" name="legomindstorms"
                                                          id="legomindstorms-2turnus" value="1 turnus 18-22 stycznia  - Grupa II - Dzieci Starsze (10-12 lat) - Projektowanie Gier Komputerowych">1 turnus 18-22 stycznia  - Grupa II - Dzieci Starsze (10-12 lat) - Projektowanie Gier Komputerowych</label>
                                            <label><input type="radio" name="legomindstorms"
                                                          id="legomindstorms-3turnus" value="2 turnus 25-29 lutego - Grupa I - Dzieci Młodsze (6-9 lat) - Warsztaty z Robotami Lego">2 turnus 25-29 lutego - Grupa I - Dzieci Młodsze (6-9 lat) - Warsztaty z Robotami Lego</label>
                                            <label><input type="radio" name="legomindstorms"
                                                          id="legomindstorms-4turnus"
                                                          value="2 turnus 25-29 lutego - Grupa II - Dzieci Starsze (10-12 lat) - Projektowanie Gier Komputerowych ">2 turnus 25-29 lutego - Grupa II - Dzieci Starsze (10-12 lat) - Projektowanie Gier Komputerowych </label>
                                        </div>
                                    </dl>
                                </fieldset>
                            </dd>
                            <dt id="fplatnosc-label">&nbsp;</dt>
                            <dd id="fplatnosc-element">
                                <fieldset id="fieldset-fplatnosc">
                                    <legend>Płatność za zajęcia</legend>
                                    <dl>
                                        <div class="checkbox-wrap3">
                                            <dd id="forma-label">
                                                <label for="forma" class="required">Forma Płatności *</label>
                                            </dd>
                                            <label>
                                                <input type="radio" name="forma" id="forma"
                                                       value="Gotówką w Centrum Edukacyjnym" class="radio">Gotówką w
                                                Centrum Edukacyjnym</label>
                                            <label>
                                                <input type="radio" name="forma" id="forma-2"
                                                       value="Przelewem na konto 38 1950 0001 2006 0889 8214 0002 IDEA BANK S.A. (odbiorca: Matplaneta SA ul. Targowa 20 A, 03-731 Warszawa, NIP 113 287 75 16 ) W tytule przelewu prosimy o wpisanie: lokalizacja, grupa, dzień, imię i nazwisko dziecka"
                                                       class="radio">Przelewem na konto 38 1950 0001 2006 0889 8214 0002
                                                IDEA BANK S.A. (odbiorca: Matplaneta SA ul. Targowa 20 A, 03-731
                                                Warszawa, NIP 113 287 75 16 ) W tytule przelewu prosimy o wpisanie:
                                                lokalizacja, grupa, dzień, imię i nazwisko dziecka</label>
                                        </div>

                                        <dd id="rodzenstwo-element">
                                            <input id="rodzenstwo" type="checkbox" name="rodzenstwo">
                                        </dd>
                                        <label for="rodzenstwo" class="rodzenstwo optional">Przysługuje mi 15% rabatu za rodzeństwo</label>

                                        <dd id="polecenie-element">
                                            <input id="polecenie" type="checkbox" name="polecenie">
                                        </dd>
                                        <label for="polecenie" class="rodzenstwo optional">Jestem uczestnikiem zajęć Matplanety</label>

                                        <div class="grupa" id="regulamin">
                                            <input type="checkbox" name="akceptacja" id="akceptacja" >
                                        </div>
                                        <div id="akceptacja-label">
                                            <label for="akceptacja" class="rodzenstwo required">Akceptuję Regulamin*</label>
                                        </div>

                                        <div class="grupa">
                                            <input type="checkbox" name="zgoda-wizerunek" id="zgoda-wizerunek">
                                        </div>
                                        <div>
                                            <label for="zgoda-wizerunek" class="rodzenstwo optional">Wyrażam zgodę na
                                                wykorzystanie wizerunku mojego dziecka poprzez upowszechnianie zdjęć i
                                                materiałów filmowych dla celów promocyjnych przez Matplaneta SA.
                                            </label>
                                        </div>

                                        <div class="grupa">
                                            <input type="checkbox" name="zgoda" id="zgoda">
                                        </div>
                                        <div id="zgoda-label">
                                            <label for="zgoda" class="rodzenstwo required">Wyrażam zgodę na
                                                przetwarzanie moich danych osobowych dla potrzeb niezbędnych do
                                                realizacji procesu rekrutacji (zgodnie z Ustawą z dnia 29.08.1997 roku o
                                                Ochronie Danych Osobowych; tekst jednolity: Dz. U. z 2002r. Nr 101, poz.
                                                926 ze zm.). *</label>
                                        </div>
                                    </dl>
                                </fieldset>
                            </dd>
                            <dt id="subform-label">&nbsp;</dt>
                            <dd id="subform-element">
                                <fieldset id="fieldset-subform">
                                    <dl>
                                        <dt id="submit-label">&nbsp;</dt>
                                        <dd id="submit-element">
                                            <input type="submit" name="send" id="submit" value="Wyślij"
                                                   class="ui-button ui-widget ui-state-default ui-corner-all"
                                                   role="button">
                                        </dd>
                                        <dt id="reset-label">&nbsp;</dt>
                                        <dd id="reset-element">
                                            <input type="reset" name="reset" id="reset" value="Anuluj"
                                                   class="ui-button ui-widget ui-state-default ui-corner-all"
                                                   role="button">
                                        </dd>
                                    </dl>
                                </fieldset>
                            </dd>
                        </dl>
                    </form>
                </div>
            </section>
            <footer>
                <div id="main-footer" class="wrapper">
                    <div class="main-border"></div>
                    <div class="container">
                        <div class="col6 main-links">
                            <h6> Kontakt </h6>
                            <ul>
                                <li><span class="icon-home"></span> <span>Al. Komisji Edukacji Narodowej 95, klatka 18a, 2 piętro<br> (obok stacji metra STOKŁOSY)</span>
                                </li>
                                <li><span class="icon-phone"></span> <span>+48 22 100 53 47</span></li>
                                <li><span class="icon-envelope"></span> <a href="mailto:biuro@e-matplaneta.pl">biuro@e-matplaneta.pl</a>
                                </li>
                                <li><span class="icon-earth"></span> <a href="../index.html">www.matplaneta.pl</a></li>
                            </ul>
                        </div>
                        <div class="col6 footer-links">
                            <h6> Lokalizacje </h6>
                            <ul>
                                <li><a href="ursynow.html">Ursynów</a></li>
                                <li><a href="bemowo.html">Bemowo</a></li>
                                <li><a href="tarchomin.html">Tarchomin</a></li>
								<li><a href="ursus.html">Ursus</a></li>
                                <li><a href="jozefow.html">Józefów</a></li>
                                <li><a href="jozefoslaw.html">Józefosław</a></li>
                                <li><a href="zary.html">Żary</a></li>
                                <li><a href="krakow.html">Kraków</a></li>
								<li><a href="gdansk.html">Gdańsk</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="sub-footer">
                    <div class="container">
                        <div class="col4"><span id="copyright">Copyright © 2014 InfoApps</span></div>
                        <div id="footer-nav" class="col8 footer-links">
                            <ul>
                                <li><a href="../index.html#about-section">O nas</a></li>
                                <li><a href="../index.html#news-section">News</a></li>
                                <li><a href="../index.html#lesson-section">Zajęcia</a></li>
                                <li><a href="../index.html#gallery-section">Galeria</a></li>
                                <li><a href="../index.html#location-section">Lokalizacje</a></li>
                                <li><a href="../index.html#work-section">Praca</a></li>
                                <li><a href="../index.html#contact-section">Kontakt</a></li>
                                <li><a href="../zapisy.php">Zapisy</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <script src="../scripts/sub-page-script.js" type="text/javascript"></script>
        <script src="validate2.js"></script>
    </body>
</html>