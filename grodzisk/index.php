<!-- get header -->
<?php $root = realpath($_SERVER["DOCUMENT_ROOT"]);
 require("$root/inc/header.php");?>

<!-- get sidebar-->
<?php require("$root/inc/sidebar.php");?>

<!-- start location -->
    <section id="location" class="wrapper">
        <div class="container">
            <div class="section-header">
                <h2> Grodzisk Mazowiecki </h2> <img src="/assets/images/border.png" alt="border"></div>
            <div class="col12"><a href="http://polygon.matplaneta.pl/Oferta.aspx?region=Grodzisk%20Mazowiecki" class="zapisy-button">Zapisy</a></div>
            <div class="col12 row"></div>
            <div class="col6">
                <img class="img-responsive" src="/assets/images/DSC_0213.jpg" alt="Zajęcia w Gdańsku">
            </div>
            <div class="col6">
                <ul>
                    <li>
                        <h4> Centrum Edukacyjne Matplaneta Grodzisk Mazowiecki </h4></li>
                    <li>
                        <p> Pawilon Kultury, ul. Westfala 3, Grodzisk Mazowiecki
						<br>Niepubliczna Szkola Tosi i Franka, ul. Kopernika 12, Grodzisk Mazowiecki
                            <br>Tel. +48 789 320 510
                            <br>Biuro obsługi klienta: +48 (22) 100 53 47 
                            <br>mail: grodzisk@matplaneta.pl 
                            <br>mail: biuro@e-matplaneta.pl  
							
                        </p>
                    </li>
                </ul>
            </div>
			<div class="col12">
                        <ul>
                            <li>
                                <h5> Podmiot świadczący usługi edukacyjne: </h5></li>
                            <li>
                                <p>Małgorzata Liszewska-Bułek, ul. Znajoma 18, 05-825 Kałęczyn, NIP 7921505856</p>
                            </li>
                            <li>
                                <h5> Nr konta bankowego:</h5></li>
                            <li>
                                <p>44195000012006087763270001</p>
                            </li>							
                        </ul>
            </div>

            <div class="col12 row"></div>
            <div class="col12">
                <iframe src="https://www.google.com/maps/d/embed?mid=zBFk1ypiCdAU.kppeZ6vQWTFw" width="100%" height="400"></iframe>
            </div>
            <?php require("$root/inc/harmonogram.php");?>
            <div class="col12">
                <h4 class="news-headline"> Aktualności </h4>
                <div class="local-news-container">
                    <div class="phGallary">
                        <ul id="gallary">
                            <li>
								<div>
                                            <div class="local-news">
                                                <h6> 11-02-2016 </h6>
                                                <h5 class="news-header">GŁOSOWANIE!!!</h5>
												<p>Gorąco zachęcamy wszystkich do oddania głosu na Matplanetę w Plebiscycie na Statuetkę Warszawskiej Rodzinki Prudential.<br><br>
W konkursie walczymy o miano miejsca z najbardziej bogatą, atrakcyjną i wartościową ofertą skierowaną do dzieci i młodzieży:)<br><br>
Zagłosować należy przez stronę:<a href="http://www.warszawskarodzinka.pl/">http://www.warszawskarodzinka.pl/</a> <br><br>
Z góry dziękujemy za każdy oddany głos:)
									</p>   
                                        </div>
								<div>
                                            <div class="local-news">
                                                <h6> 01-02-2016 </h6>
                                                <h5 class="news-header">PRZERWA ZIMOWA</h5>
												<p>Informujemy, że w czasie Ferii Zimowych (1-14.02) zajęcia Matplanety nie odbywają się. W tym czasie organizujemy Ferie z Robotami Lego, Programowaniem Gier Komputerowych oraz Grafiką Komputerową.<br><br>
													Do zobaczenia 15.02:)
												</p>     
                                            </div>
                                        </div><!-- end con-div -->	
								<div>
												<div class="local-news">
													<h6> 16-12-2015 </h6>
													<h5 class="news-header">ŚWIĄTECZNE WIEŚCI Z MATPLANETY</h5>
													<p>W grudniu ukazał się już 4 numer gazetki Matplanety!<br><br>
														A w nim:<br>
														- krótkie podsumowanie minionych miesięcy,<br>
														- zapowiedź wyczekiwanych przez wszystkich FERII ZIMOWYCH<br>
														- krótka wzmianka o świątecznych promocjach oraz o planowanych warsztatach dla rodziców,<br>
														- a przede wszystkim ZAGADKI, ZAGADKI, ZAGADKI!!!<br><br>

														W Święta nie pozwalamy się nudzić:)<br><br>

														<a target="_blank" href="http://matplaneta.pl/assets/download/gazetka_nr_4_grudzien.pdf">Pobierz gazetkę</a><br>
													</p>     
												</div>
											</div><!-- end con-div -->	
									<div>
                                    <div class="local-news">
                                        <h6> 03-09-2015 </h6>
                                        <h5 class="news-header">Festyn Stowarzyszenia Przedsiębiorców</h5>

                                       <p>
W dniu 22.08.2015 w ramach Festynu Stowarzyszenia Przedsiębiorców  w Grodzisku Mazowieckim Matplaneta prezentowała swoją ofertę mieszkańcom.
Rodzice wykazali duże zainteresowanie ofertą i mówili, że takie zajęcia są w Grodzisku Mazowieckim bardzo potrzebne.
                                       </p>    
                                    </div>
                                </div><!-- end con-div -->  
                                <div>
                                    <div class="local-news">
                                        <h6> 25-08-2015 </h6>
                                        <h5 class="news-header">W sobotę 19 września o godzinie 11:00 lekcja pokazowa w Grodzisku Mazowieckim przy ul. Westfala 3!</h5>

                                       <p>
                                           Zajęcia będą odbywały się w następujących godzinach:
                                           <br>
                                            11:00 – 14:30 - zajęcia matematyki
                                            <br>
                                            14:30 – 16:00 – programowanie komputerowe
                                            <br>
                                            <br>
                                            Podczas zajęć dla dzieci  odbędzie się również spotkanie z rodzicami. Chcemy przedstawić Państwu zasady działania Matplanety. Dodatkowo poznają Państwo nasz proponowany plan zajęć na najbliższy semestr oraz zasady prowadzenia zapisów.
                                            <br>
                                            <br>
                                            Już nie możemy się doczekać spotkania z Wami!
                                       </p>    
                                    </div>
                                </div><!-- end con-div -->  
								<div>
                                    <div class="local-news">
                                        <h6> 21-08-2015 </h6>
                                        <h5 class="news-header">Miło nam poinformować, że w Matplanecie uruchomiliśmy nasz własny system komunikacji z klientami - Polygon.</h5>

										<p>W systemie są dostępne funkcjonalności pozwalające m.in. na:<br>
											•	zapisanie dziecka na zajęcia,<br>
											•	wysłanie maila do Nauczyciela lub Administratora oddziału,<br>
											•	sprawdzenie kwot i terminów opłat oraz przejrzenie uiszczonych wpłat,<br>
											•	aktualizację swoich danych kontaktowych.<br></p>
										<p>W menu „Rekomendacja” nasi stali Klienci znajda proponowaną grupę dla  dziecka na I semestr 2015/2016 i skrót do Zapisów.</p>
										<p>Pełna oferta zajęć znajduje się w menu „Zapisy”.</p>
										<p>Wierzymy, że system będzie przyjazny dla Państwa i uprości proces zapisu  oraz kontakty z biruem.</p>
										<p>Zapraszamy do rejestrowania się i zapisywania na zajęcia!</p>
										<p><a href="http://matplaneta.pl/assets/download/System%20Obs%C5%82ugi%20Klienta%20POLYGON.pdf" target="_blank">Krótka instrukcja obsługi</a></p>
										<p>Z pozdrowieniami<br>
										Zespół Matplanety</p>     
                                    </div>
                                </div><!-- end con-div -->							
                                <div>
                                    <div class="local-news">
                                        <h6> 19-06-2015 </h6>
                                        <h5 class="news-header">Jesteśmy w trakcie przygotowania planu zajęć Matplanety na rok szkolny 2015-2016. </h5>
                                        <p class="second-news">Zapraszamy do zapisów w drugiej połowie sierpnia.
                                            Ale już teraz można wypełnić  formularz zgłoszenia chęci udziału dziecka w zajęciach Matplanety w roku 2015-2016.
                                            Dzięki temu będziemy mogli stworzyć harmonogram zajęć najlepiej odpowiadający Państwa potrzebom.</p>
                                    </div><!-- end local-news -->
                                </div><!-- end con-div -->
                            </li><!-- end first-page -->
                        </ul><!-- end gallary -->
                        <center>
                            <span id="PageList" class="clear"></span>
                        </center>
                    </div><!-- end phGallary -->
                  <!-- <div class="local-news">
                        <h6> 02-06-2015 </h6>
                        <h5 class="news-header">W miniony piątek (29 maja) w SP nr 1 w Gdańsku odbył się wspaniały Festyn Rodzinny!</h5>
                        <p class="second-news">Również Matplaneta miała tam swoje stoisko. Była to pierwsza, a zarazem niebywale udana akcja nowopowstałej filii naszej Szkoły.
                            Dzieciaki na Pomorzu odkryły, że sudoku z figurami, tajne kody agentów czy gry planszowe na logikę i bystrość mogą dostarczać masę wrażeń i niezapomnianych emocji.
                            Mamy nadzieję, że dzięki Festynowi Matplaneta już od września przywita całe grupy młodych Pascali, Fermatów, czy Eulerów chętnych samodzielnie odkrywać niesamowity Świat Matematyki!</p>
                    </div>

                    <div class="local-news">
                        <h6> 01-06-2015 </h6>
                        <h5 class="news-header">W nowym roku szkolnym Matplaneta podbije Gdańsk!</h5>

                        <p> Po wakacjach w Gdańsku rozpocznie swoją działalność pierwsza na Pomorzu placówka Matplanety. Zapraszamy do zapisów pod adresem gdansk@e-matplaneta.pl</p>
                    </div>
                </div>
            </div>
            -->
            </div><!-- end local-news container -->
        </div><!-- end col12 -->
    </div><!-- end container -->
    </section>

<!-- get footer -->
<?php require("$root/inc/footer.php");?>