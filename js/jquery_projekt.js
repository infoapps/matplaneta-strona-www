function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
/* Polish initialisation for the jQuery UI date picker plugin. */
/* Written by Jacek Wysocki (jacek.wysocki@gmail.com). */
jQuery(function($){
$.datepicker.regional['pl'] = {
		closeText: 'Zamknij',
		prevText: '&#x3c;Poprzedni',
		nextText: 'Następny&#x3e;',
		currentText: 'Dziś', 
		monthNames: ['Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec',
		'Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'],
		monthNamesShort: ['Sty','Lu','Mar','Kw','Maj','Cze',
		'Lip','Sie','Wrz','Pa','Lis','Gru'],
		dayNames: ['Niedziela','Poniedzialek','Wtorek','Środa','Czwartek','Piątek','Sobota'],
		dayNamesShort: ['Nie','Pn','Wt','Śr','Czw','Pt','So'],
		dayNamesMin: ['N','Pn','Wt','Śr','Cz','Pt','So'],
		dateFormat: 'yy-mm-dd', firstDay: 1,
		isRTL: false};
	$.datepicker.setDefaults($.datepicker.regional['pl']);	

});

$(document).ready(function() {		
$('#baner-top').cycle({fx: 'all',random: 1, speed: 800, timeout: 10000});		
$('#slideshow').cycle({timeout: 9000, cleartype: 1, speed: 1000 });
			   
	$('a.blank').attr('target', '_blank');
							   
	// decription - 
	$("input, textarea, select, radio,checkbox").focus(function () {
         $(this).next("p.description").css('display','inline').fadeIn("slow");				 
    });
	$("input, textarea, select, radio,checkbox").focusout(function () {
        $(this).next("p.description").css('display','inline').fadeOut(500);
    });	
  
 $('input:submit.standard, input:reset.standard').each(function(){
  $(this).replaceWith('<button type="' + $(this).attr('type') + '" style=" height:20px; font-size: 10px; ">' + $(this).val() + '</button>');
 });

	//wyglad buttonow jQwery UI
	 	$("input:submit,input:reset,.standard a").button();   

		$(".standard a.add").button({
            icons: {
                primary: 'ui-icon-circle-plus'
            }
			});	
		$(".standard a.usun").button({
            icons: {
                primary: 'ui-icon-alert'
            }
			});
		$(".standard a.edytuj").button({
            icons: {
                primary: 'ui-icon-document'
            }
			});		
		
		$(".standard a.wlacz").button({
            icons: {
                primary: 'ui-icon-plus'
            }
			});	
		$(".standard a.wylacz").button({
            icons: {
                primary: 'ui-icon-minus'
            }
			});			
		$(".standard a.poprzednia").button({
            icons: {
				primary: 'ui-icon-triangle-1-w',
                secondary: 'ui-icon-grip-dotted-vertical'
            }
			});	
		$(".standard a.nastepna").button({
            icons: {
				primary: 'ui-icon-grip-dotted-vertical',
                secondary: 'ui-icon-triangle-1-e'
            }
			});		
			
		$(":submit").button({
            icons: {
		    primary: 'ui-icon-circle-plus'
            }, text: false 
			});				
}); 

// no jquery
function uniqid (prefix, more_entropy) {
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +    revised by: Kankrelune (http://www.webfaktory.info/)
    // %        note 1: Uses an internal counter (in php_js global) to avoid collision
    // *     example 1: uniqid();
    // *     returns 1: 'a30285b160c14'
    // *     example 2: uniqid('foo');
    // *     returns 2: 'fooa30285b1cd361'
    // *     example 3: uniqid('bar', true);
    // *     returns 3: 'bara20285b23dfd1.31879087'

    if (typeof prefix == 'undefined') {
        prefix = "";
    }

    var retId;
    var formatSeed = function (seed, reqWidth) {
        seed = parseInt(seed,10).toString(16); // to hex str
        if (reqWidth < seed.length) { // so long we split
            return seed.slice(seed.length - reqWidth);
        }
        if (reqWidth > seed.length) { // so short we pad
            return Array(1 + (reqWidth - seed.length)).join('0')+seed;
        }
        return seed;
    };

    // BEGIN REDUNDANT
    if (!this.php_js) {
        this.php_js = {};
    }
    // END REDUNDANT
    if (!this.php_js.uniqidSeed) { // init seed with big random int
        this.php_js.uniqidSeed = Math.floor(Math.random() * 0x75bcd15);
    }
    this.php_js.uniqidSeed++;

    retId  = prefix; // start with prefix, add current milliseconds hex string
    retId += formatSeed(parseInt(new Date().getTime()/1000,10),8);
    retId += formatSeed(this.php_js.uniqidSeed,5); // add seed hex string

    if (more_entropy) {
        // for more entropy we add a float lower to 10
        retId += (Math.random()*10).toFixed(8).toString();
    }

    return retId;
}
