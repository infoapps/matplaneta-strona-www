// I18N constants
// LANG: "pl", ENCODING: UTF-8
{
  "Please confirm that you want to open this link": "Please confirm that you want to open this link",
  "Cancel": "Zamknij",
  "Dictionary": "Dictionary",
  "Finished list of mispelled words": "Finished list of mispelled words",
  "I will open it in a new page.": "I will open it in a new page.",
  "Ignore all": "Ignore all",
  "Ignore": "Ignore",
  "No mispelled words found with the selected dictionary.": "No mispelled words found with the selected dictionary.",
  "Spell check complete, didn't find any mispelled words.  Closing now...": "Spell check complete, didn't find any mispelled words.  Closing now...",
  "OK": "OK",
  "Original word": "Original word",
  "Please wait.  Calling spell checker.": "Please wait.  Calling spell checker.",
  "Please wait: changing dictionary to": "Please wait: changing dictionary to",
  "This will drop changes and quit spell checker.  Please confirm.": "This will drop changes and quit spell checker.  Please confirm.",
  "Re-check": "Re-check",
  "Replace all": "Replace all",
  "Replace with": "Replace with",
  "Replace": "Replace",
  "Spell-check": "Spell-check",
  "Suggestions": "Suggestions",
  "One moment...": "One moment..."
};
