// I18N constants
// LANG: "de", ENCODING: UTF-8
// translated: Raimund Meyer xinha@ray-of-light.org
{
  "Clean up HTML": "Clean up HTML",
  "Please select from the following cleaning options...": "Please select from the following cleaning options...",
  "General tidy up and correction of some problems.": "General tidy up and correction of some problems.",
  "Clean bad HTML from Microsoft Word": "Clean bad HTML from Microsoft Word",
  "Remove custom typefaces (font \"styles\").": "Remove custom typefaces (font \"styles\").",
  "Remove custom font sizes.": "Remove custom font sizes.",
  "Remove custom text colors.": "Remove custom text colors.",
  "Remove lang attributes.": "Remove lang attributes.",
  "Go": "Go",
  "Cancel": "Zamknij",
  "Tidy failed.  Check your HTML for syntax errors.": "Tidy failed.  Check your HTML for syntax errors.",
  "You don't have anything to tidy!": "You don't have anything to tidy!",
  "Please stand by while cleaning in process..." : "Please stand by while cleaning in process..."
};
