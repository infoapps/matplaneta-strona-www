_editor_url  = "/js/xinha/";   // (preferably absolute) URL (including trailing slash) where Xinha is installed
_editor_lang = "pl";       // And the language we need to use in the editor.
_editor_skin = "";    // If you want use a skin, add the name (of the folder) here
_editor_icons = "Classic"; // If you want to use a different iconset, add the name (of the folder, under the `iconsets` folder) here

    //<![CDATA[
		//Create a new Imanager Manager, needs the directory where the manager is
		//and which language translation to use.
		var manager = new ImageManager('/administracja/ImageManager','pl');		
		//Image Manager wrapper. Simply calls the ImageManager
		ImageSelector = 
		{
			//This is called when the user has selected a file
			//and clicked OK, see popManager in IMEStandalone to 
			//see the parameters returned.
			update : function(params)
			{
				if(this.field && this.field.value != null)
				{
					this.field.value = params.f_file; //params.f_url
				}
			},
			//open the Image Manager, updates the textfield
			//value when user has selected a file.
			select: function(textfieldID)
			{
				this.field = document.getElementById(textfieldID);
				manager.popManager(this);	
			}
		};	
    //]]>
//console.log();