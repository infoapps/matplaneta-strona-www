//<![CDATA[
    function ValidateAjax(formid, endpoint){
		
            end_url = endpoint;
            form_id = '#'+formid;

            $(form_id+' input').blur(function(){
                var formElementID = $(this).attr('id');
				if ($(this).attr('id')=='txt_captcha-input'){
				}
				else {
                doValidate(formElementID);
                return false;
				}
            })
            $(form_id+' select').blur(function(){
                var formElementID = $(this).attr('id');
                doValidate(formElementID);
                return false;
            })			
            $(form_id+' textarea').blur(function(){
                var formElementID = $(this).attr('id');
                doValidate(formElementID);
                return false;
            })			
			
        };

       function doValidate(id){
            var url = end_url;
            var data = $(form_id).serialize();
            $.post(url,data,function(response){
                $('#'+id).parent().find('.errors').remove();
                if (response[id])
                    $('#'+id).parent().append(getHTMLValidate(response[id]));
            },'json');

        };
		
       function getHTMLValidate(errArray){
            var o = '<ul class="errors">';
            $.each(errArray,function(key,value){
                o+='<li>'+ value+'</li>';
            });
            o+='</ul>';
            return o;
        }
//]]>