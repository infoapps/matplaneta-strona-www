<?php

?>

<div id="side-bar">
    <div id="matplaneta-logo">
        <a href="" title="Matplaneta, al. KEN 95, Warszawa"> <img src="assets/images/matplaneta.jpg"
                                                                  alt="Matplaneta zajęcia pozaszkolne">
        </a>
    </div>
    <nav id="main-nav">
        <ul>
            <li class="dropdown">
                <a href="index.html">Start</a>
                <ul class="sub-menu">
                    <li>
                        <a href="index.html#about-section">O nas</a>
                    </li>
                    <li>
                        <a href="index.html#gallery-section">Galeria</a>
                    </li>
                    <li>
                        <a href="index.html#lesson-section">Zajęcia</a>
                    </li>

                    <li>
                        <a href="index.html#contact-section">Kontakt</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="aktualnosci.php">Aktualności</a>
            </li>
            <li>
                <a href="lokalizacje.php">Lokalizacje</a>
            </li>
            <li>
                <a href="praca.php">Praca</a>
            </li>
            <li>
                <a href="szkoly.php">Szkoły</a>
            </li>
            <li>
                <a href="sukcesy.php">Sukcesy</a>
            </li>
        </ul>
    </nav>
    <div id="menu-buttons">
        <a href="zapisy.php">Zapisy i ceny</a>
    </div>
</div>