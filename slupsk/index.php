<!-- get header -->
<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
 require("$root/inc/header.php");?>

<!-- get sidebar-->
<?php require("$root/inc/sidebar.php");?>

<!-- start location -->
            <section id="location" class="wrapper">
                <div class="container">
                    <div class="section-header">
                        <h2>Słupsk</h2> <img src="/assets/images/border.png" alt="border"></div>
                    <div class="col12"><a href="http://polygon.matplaneta.pl/Oferta.aspx?region=slupsk" class="zapisy-button">Zapisy</a></div>
                    <div class="col12 row"></div>
                    <div class="col6">
                        <img class="img-responsive" src="/assets/images/a.jpg" alt="Zajęcia w Słupsku">
                    </div>
                    <div class="col6">
                        <ul>
                            <li>
                                <h4>Centrum Edukacyjne Matplaneta Słupsk</h4></li>
                            <li>
                                <p>ul. Filmowa 1, Słupsk
                                    <br>Tel. +48 501 277 810
                                    <br>Biuro obsługi klienta: +48 (22) 100 53 47 
                                    <br>mail: slupsk@matplaneta.pl
                                    <br>mail: biuro@matplaneta.pl </p>
                            </li>
                        </ul>
                    </div>
					<div class="col12">
                        <ul>
                            <li>
                                <h5> Podmiot świadczący usługi edukacyjne: </h5></li>
                            <li>
                                <p>Centrum Nauki i Rozwoju, ul. Filmowa 1, 76-200 Słupsk NIP 8392018682</p>
                            </li>
                            <li>
                                <h5> Nr konta bankowego:</h5></li>
                            <li>
                                <p>42 1140 2004 0000 3902 5015 7025</p>
                            </li>							
                        </ul>
                    </div>
                    <div class="col12 row"></div>
                    <div class="col12">

                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2318.8478081835588!2d17.029478!3d54.465622!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46fe10a7216db627%3A0x616816530748ca3c!2sFilmowa+1%2C+S%C5%82upsk!5e0!3m2!1spl!2spl!4v1440685909902" width="100%" height="400">

                        </iframe>
                    </div>
					<?php require("$root/inc/harmonogram.php") ?>
                    <div class="col12">
                        <h4 class="news-headline"> Aktualności </h4>
                        <div class="local-news-container">
                            <div class="phGallary">
                                <ul id="gallary">
                                    <li>								
									    <div>
                                            <div class="local-news">
                                                <h6> 21-08-2015 </h6>
                                                <h5 class="news-header">Miło nam poinformować, że w Matplanecie uruchomiliśmy nasz własny system komunikacji z klientami - Polygon.</h5>
												<p>W systemie są dostępne funkcjonalności pozwalające m.in. na:<br>
												•	zapisanie dziecka na zajęcia,<br>
												•	wysłanie maila do Nauczyciela lub Administratora oddziału,<br>
												•	sprawdzenie kwot i terminów opłat oraz przejrzenie uiszczonych wpłat,<br>
												•	aktualizację swoich danych kontaktowych.<br></p>
												<p>W menu „Rekomendacja” nasi stali Klienci znajda proponowaną grupę dla  dziecka na I semestr 2015/2016 i skrót do Zapisów.</p>
												<p>Pełna oferta zajęć znajduje się w menu „Zapisy”.</p>
												<p>Wierzymy, że system będzie przyjazny dla Państwa i uprości proces zapisu  oraz kontakty z biruem.</p>
												<p>Zapraszamy do rejestrowania się i zapisywania na zajęcia!</p>
												<p><a href="http://matplaneta.pl/assets/download/System%20Obs%C5%82ugi%20Klienta%20POLYGON.pdf" target="_blank">Krótka instrukcja obsługi</a></p>
												<p>Z pozdrowieniami<br>
												Zespół Matplanety</p>   
                                            </div>
                                        </div><!-- end con-div -->
                                    </li><!-- end first page/pagination -->
                                </ul><!-- end gallary -->
                                <center>
                                    <!-- pagination -->
                                    <span id="PageList" class="clear"></span>
                                </center>
                            </div><!-- end phGalarry -->
                        </div><!-- end local-news-container -->
                    </div><!-- end col12 -->
                </div><!-- end container -->
            </section>

<!-- get footer -->
<?php require("$root/inc/footer.php");?>