<!-- get header -->
<?php $root = realpath($_SERVER["DOCUMENT_ROOT"]);
require("$root/inc/header.php");?>

<!-- get sidebar-->
<?php require("$root/inc/sidebar.php");?>

<!-- start location -->
    <section id="location" class="wrapper">
        <div class="container">
            <div class="section-header">
                <h2> Konstancin Jeziorna </h2>
                <img src="/assets/images/border.png" alt="border">
            </div>

            <div class="col12">
                <a href="http://polygon.matplaneta.pl/Oferta.aspx?region=Konstancin-Jeziorna" class="zapisy-button">Zapisy</a>
            </div>
            <div class="col12 row"></div>
            <div class="col6">
                <img class="img-responsive" src="/assets/images/DSC_0213.jpg" alt="Zajęcia w Krakowie">
            </div>
            <div class="col6">
                <ul>
                    <li>
                        <h4> Centrum Edukacyjne Matplaneta Konstancin-Jeziorna </h4></li>
                    <li>
                        <p>ul. Sobieskiego 6, Konstancin-Jeziorna

                            <br>Tel. +48 535 314 314
                            <br>Biuro obsługi klienta: +48 (22) 100 53 47 
                            <br>mail: biuro@e-matplaneta.pl
							<br>mail: konstancin-jeziorna@e-matplaneta.pl 
                        </p>
                    </li>
                </ul>
            </div>
			<div class="col12">
                        <ul>
                            <li>
                                <h5> Podmiot świadczący usługi edukacyjne: </h5></li>
                            <li>
                                <p>MatEmotion Agnieszka Dzierzgowska</br>
									ul. Sobieskiego 6</br>
									05-520 Konstancin Jeziorna</br>
									NIP 521 223 98 08</p>
                            </li>
                            <li>
                                <h5> Nr konta bankowego:</h5></li>
                            <li>
                                <p>94 1940 1076 3152 1826 0000 0000</p>
                            </li>							
                        </ul>
            </div>
            <div class="col12 row"></div>
            <div class="col12">
                <iframe src="https://www.google.com/maps/d/embed?mid=zBFk1ypiCdAU.kzgYVL8a4RO0&z=17" width="100%" height="400">

                </iframe>
            </div>
            <?php require("$root/inc/harmonogram.php");?>
            <div class="col12">
                <h4 class="news-headline">
                    Aktualności
                </h4>
                <div class="local-news-container">
                    <div class="phGallary">
                        <ul id="gallary">
                            <li>
										<div>
                                            <div class="local-news">
                                                <h6> 11-02-2016 </h6>
                                                <h5 class="news-header">GŁOSOWANIE!!!</h5>
												<p>Gorąco zachęcamy wszystkich do oddania głosu na Matplanetę w Plebiscycie na Statuetkę Warszawskiej Rodzinki Prudential.<br><br>
W konkursie walczymy o miano miejsca z najbardziej bogatą, atrakcyjną i wartościową ofertą skierowaną do dzieci i młodzieży:)<br><br>
Zagłosować należy przez stronę:<a href="http://www.warszawskarodzinka.pl/">http://www.warszawskarodzinka.pl/</a> <br><br>
Z góry dziękujemy za każdy oddany głos:)
									</p>   
                                        </div>
										<div>
                                            <div class="local-news">
                                                <h6> 01-02-2016 </h6>
                                                <h5 class="news-header">PRZERWA ZIMOWA</h5>
												<p>Informujemy, że w czasie Ferii Zimowych (1-14.02) zajęcia Matplanety nie odbywają się. W tym czasie organizujemy Ferie z Robotami Lego, Programowaniem Gier Komputerowych oraz Grafiką Komputerową.<br><br>
													Do zobaczenia 15.02:)
												</p>     
                                            </div>
                                        </div><!-- end con-div -->	
								<div>
                                            <div class="local-news">
                                                <h6> 11-01-2015 </h6>
                                                <h5 class="news-header">UWAGA!!! RABAT 15% NA FERIE Z MATPLANETĄ</h5>
												 <img style="max-height: 280px;" src="/assets/images/rabat.png">
												 <p>
Dla rodzeństw oraz uczniów naszych zajęć mamy specjalną ofertę!!!<br><br>
Wiedząc, że cena jednego turnusu Ferii w Matplanecie wynosi 680zł, rozwiążcie zagadkę ze zdjęcia:) Grafika komputerowa / Tworzenie gier komputerowych / Budowanie robotów mogą stać się pasją Twojego dziecka - Zapisz je już teraz: http://matplaneta.pl/zapisy-ferie-2016/
									</p>     
                                            </div>
                                </div><!-- end con-div -->	
								<div>
                                            <div class="local-news">
                                                <h6> 16-12-2015 </h6>
                                                <h5 class="news-header">ŚWIĄTECZNE WIEŚCI Z MATPLANETY</h5>
												<p>W grudniu ukazał się już 4 numer gazetki Matplanety!<br><br>
													A w nim:<br>
													- krótkie podsumowanie minionych miesięcy,<br>
													- zapowiedź wyczekiwanych przez wszystkich FERII ZIMOWYCH<br>
													- krótka wzmianka o świątecznych promocjach oraz o planowanych warsztatach dla rodziców,<br>
													- a przede wszystkim ZAGADKI, ZAGADKI, ZAGADKI!!!<br><br>

													W Święta nie pozwalamy się nudzić:)<br><br>

													<a target="_blank" href="http://matplaneta.pl/assets/download/gazetka_nr_4_grudzien.pdf">Pobierz gazetkę</a><br>
												</p>     
                                            </div>
                                </div><!-- end con-div -->	
								<div>
                                            <div class="local-news">
                                                <h6> 18-09-2015 </h6>
                                                <h5 class="news-header">Już jutro startujemy!</h5>
												<p>Już w tą sobotę (19.09) ruszamy z pierwszymi zajęciami w naszych placówkach.<br>
													Od samego rana grupy naszych starych oraz nowych uczniów rozpoczną przygodę z matematycznymi oraz logicznymi wyzwaniami. Będziemy programować pierwsze programy komputerowe oraz łamać głowy nad trudnymi zadaniami.<br>
													Wszyscy czekamy z niecierpliwością na jutrzejszy dzień i nie możemy się już doczekać!
												</p>    
                                            </div>
                                        </div><!-- end con-div -->	
								<div>
                                    <div class="local-news">
                                        <h6> 21-08-2015 </h6>
                                        <h5 class="news-header">                W sobotę 12 września o godzinie 11:00 lekcja pokazowa w Konstancinie! ul.  Sobieskiego 6
                                        Zajęcia będą odbywały się w następujących godzinach dla poszczególnych  grup wiekowych:</h5>
                                        <p class="second-news">
                                        11:00 - 11:45 - zajęcia matematyki dla dzieci w wieku 4-5 lat</br>
                                        12:00 - 12:45- zajęcia matematyki dla dzieci w wieku 6-7 lat</br>
                                        13:00 - 13:45 - zajęcia połączone dla dzieci w wieku 8-9 lat </br>
										14:00 - 15:00 (60 minut) - zajęcia połączone dla dzieci w wieku 10-11 lat  (matematyka i programowanie komputerowe)</br>
                                        15:00 - 16:00 (60 minut) - zajęcia połączone dla dzieci w wieku 12-14 lat  (matematyka i programowanie komputerowe)</br></br>

                                        W drugiej sali podczas zajęć Państwa dzieci - odbędzie się spotkanie z  rodzicami. Chcemy przedstawić Państwu zasady działania Matplanety.  Dodatkowo poznają Państwo nasz proponowany plan zajęć na najbliższy  semestr oraz zasady prowadzenia zapisów.</br></br>

                                        Na koniec każdych zajęć będzie przeprowadzona loteria z nagrodami!
                                        Nagrody oczywiście pozostają tajemnicą Emotikon smile Możemy tylko
                                        zdradzić, że nikt nie wyjdzie z pustymi rękami.</br></br>
                                        Już nie możemy się doczekać spotkania z Wami!
                                        </p>
                                    </div><!-- end local-news -->
                                </div><!-- end con-div -->
									    <div>
                                            <div class="local-news">
                                                <h6> 21-08-2015 </h6>
                                                <h5 class="news-header">Miło nam poinformować, że w Matplanecie uruchomiliśmy nasz własny system komunikacji z klientami - Polygon.</h5>

												<p>W systemie są dostępne funkcjonalności pozwalające m.in. na:<br>
												•	zapisanie dziecka na zajęcia,<br>
												•	wysłanie maila do Nauczyciela lub Administratora oddziału,<br>
												•	sprawdzenie kwot i terminów opłat oraz przejrzenie uiszczonych wpłat,<br>
												•	aktualizację swoich danych kontaktowych.<br></p>
												<p>W menu „Rekomendacja” nasi stali Klienci znajda proponowaną grupę dla  dziecka na I semestr 2015/2016 i skrót do Zapisów.</p>
												<p>Pełna oferta zajęć znajduje się w menu „Zapisy”.</p>
												<p>Wierzymy, że system będzie przyjazny dla Państwa i uprości proces zapisu  oraz kontakty z biruem.</p>
												<p>Zapraszamy do rejestrowania się i zapisywania na zajęcia!</p>
												<p><a href="http://matplaneta.pl/assets/download/System%20Obs%C5%82ugi%20Klienta%20POLYGON.pdf" target="_blank">Krótka instrukcja obsługi</a></p>
												<p>Z pozdrowieniami<br>
												Zespół Matplanety</p>     
                                            </div>
                                        </div><!-- end con-div -->
							    
                                <div>
                                    <div class="local-news">
                                        <h6> 19-06-2015 </h6>
                                        <h5 class="news-header">Jesteśmy w trakcie przygotowania planu zajęć Matplanety na rok szkolny 2015-2016. </h5>
                                        <p class="second-news">Zapraszamy do zapisów w drugiej połowie sierpnia.
                                            Ale już teraz można wypełnić  formularz zgłoszenia chęci udziału dziecka w zajęciach Matplanety w roku 2015-2016.
                                            Dzięki temu będziemy mogli stworzyć harmonogram zajęć najlepiej odpowiadający Państwa potrzebom.</p>
                                    </div><!-- end local-news -->
                                </div><!-- end con-div -->
                            </li><!-- end first-page -->
                        </ul><!-- end gallary -->
                        <center>
                            <span id="PageList" class="clear"></span>
                        </center>
                    </div><!-- end phGallary -->
                    <!--
                    <div class="local-news">
                        <h6> 16-06-2015 </h6>
                        <h5 class="news-header">Święto dzielnicy Borek Fałęcki</h5>

                        <p>W ostatnią niedzielę (14 czerwca) gościliśmy na święcie dzielnicy Borek Fałęcki

                            I zaobserwowaliśmy  ciekawą prawidłowość:

                            Im więcej wysiłku rodzic zmuszony był włożyć w przekonanie dziecka do spróbowania naszych łamigłówek,

                            tym więcej musiał potem musi użyć siły, by swoje dziecko od nich oderwać!
                            </br>
                            Dzieci, które poznały już swoją siłę myślenia, nie odpuszczały nowych wyzwań, nawet kiedy zaczął się występ klauna a słońce zaczęło palić niemiłosiernie.

                            Na nowe ciekawe wyzwania zapraszamy póki co na wakacje do nowego Centrum przy Torfowej <a class="program" href="http://matplaneta.pl/formularze/wakacje-krakow.php" target="_blank">tutaj.</a>
                        </p>
                    </div>

                    <div class="local-news">
                        <h6> 02-06-2015 </h6>
                        <h5 class="news-header">Kto nie wierzy, że dzieci uwielbiają myśleć i lubią wyzwania, powinien był przyjść do nas na święto Białego Prądnika.</h5>

                        <p>Starsi i młodsi ochoczo łamali sobie głowy nad sposobami optymalnego transportu przez rzekę, przekładem z tajemniczego języka, czy zagadką Einsteina. A uporawszy się z nimi, dzieci prosiły o więcej i więcej. Nie chciały więcej cukierków, balonów czy innych nagród – chciały więcej zagadek!
                            </br>
                            Mamy ich jeszcze sporo w zanadrzu, więc zapraszamy do Matplanety!
                            </br>
                            Następne spotkanie w Krakowie planujemy już 14 czerwca. Będziemy wtedy gościć na święcie dzielnicy Borek Fałęcki na stadionie KS Borek przy ul. Żywieckiej 13.
                        </p>
                    </div>

                    <div class="local-news">
                        <h6> 25-05-2015 </h6>
                        <h5 class="news-header">Matplaneta ma już swoje centrum w Krakowie! Na Ruczaju, przy ul. Torfowej 4.</h5>

                        <p>Startujemy już 29 czerwca!<br>
                            Zapraszamy do naszego Centrum na: Wakacje z robotami Lego i językiem angielskim.
                            <br>

                            Więcej informacji dostępnych
                            <a class="program" href="assets/download/MTPLNT_A5_krakowSMALL.pdf" target="_blank">tutaj.</a>



                        </p>
                    </div>

                    <!-- <div class="local-news">
                         <h6> 11-01-2014 </h6>
                         <h5 class="news-header"> Nowy semestr zajęć w Matplanecie rusza już 13 lutego! </h5>

                         <p> Można jeszcze dopisywać dzieci niemalże do wszystkich grup w naszych placówkach, a
                             więc zapraszamy do zapoznania się z HARMONOGRAMEM.
                             <br>Dzieci, które chodziły na nasze zajęcia w tym semestrze, są automatycznie
                             przenoszone na kolejny.</p>
                     </div>-->
                </div><!-- end local-news-container -->
            </div><!-- end col12 -->
        </div><!-- end container -->
    </section>

<!-- get footer -->
<?php require("$root/inc/footer.php");?>