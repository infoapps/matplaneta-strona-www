<!-- get header -->
<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
 require("$root/inc/header.php");?>

<!-- get sidebar-->
<?php require("$root/inc/sidebar.php");?>

<!-- start location -->
            <section id="location" class="wrapper">
                <div class="container">
                    <div class="section-header">
                        <h2> Radzymin - Nowe Załubice </h2> <img src="/assets/images/border.png" alt="border"></div>
                    <div class="col12"><a href="http://polygon.matplaneta.pl/Oferta.aspx?region=Radzymin-%20Nowe%20Zalubice" class="zapisy-button">Zapisy</a></div>
                    <div class="col12 row"></div>
                    <div class="col6">
                        <img class="img-responsive" src="/assets/images/a.jpg" alt="Zajęcia na Bemowie">
                    </div>
                    <div class="col6">
                        <ul>
                            <li>
                                <h4> Centrum Edukacyjne Radzymin - Nowe Załubice </h4></li>
                            <li>
                                <p> ul. Krucza 5, Nowe Załubice 
                                    <br>mail: radzymin@matplaneta.pl
                                    <br>mail: biuro@matplaneta.pl</p>
                            </li>
                        </ul>
                    </div>
					<div class="col12">
                        <ul>
                            <li>
                                <h5> Podmiot świadczący usługi edukacyjne: </h5></li>
                            <li>
                                <p>Jagoda Balińska-Czajka, ul. Krucza 5, 05-255 Nowe Załubice, NIP 1180650377</p>
                            </li>
                            <li>
                                <h5> Nr konta bankowego:</h5></li>
                            <li>
                                <p>91195000012006089465110002</p>
                            </li>							
                        </ul>
                    </div>
                    <div class="col12 row"></div>
                    <div class="col12">

                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2429.82215569388!2d21.1352305!3d52.48235570000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x471ec1a064646a39%3A0x79c58ed44a874f9f!2sKrucza+5%2C+05-255+Nowe+Za%C5%82ubice!5e0!3m2!1spl!2spl!4v1441376091044" width="100%" height="400"></iframe>

                        </iframe>
                    </div>
					<?php require("$root/inc/harmonogram.php") ?>
                    <div class="col12">
                        <h4 class="news-headline"> Aktualności </h4>
                        <div class="local-news-container">
                            <div class="phGallary">
                                <ul id="gallary">
                                    <li>								
									    <div>
                                            <div class="local-news">
                                                <h6> 11-02-2016 </h6>
                                                <h5 class="news-header">GŁOSOWANIE!!!</h5>
												<p>Gorąco zachęcamy wszystkich do oddania głosu na Matplanetę w Plebiscycie na Statuetkę Warszawskiej Rodzinki Prudential.<br><br>
W konkursie walczymy o miano miejsca z najbardziej bogatą, atrakcyjną i wartościową ofertą skierowaną do dzieci i młodzieży:)<br><br>
Zagłosować należy przez stronę:<a href="http://www.warszawskarodzinka.pl/">http://www.warszawskarodzinka.pl/</a> <br><br>
Z góry dziękujemy za każdy oddany głos:)
									</p>   
                                        </div>
										<div>
                                            <div class="local-news">
                                                <h6> 01-02-2016 </h6>
                                                <h5 class="news-header">PRZERWA ZIMOWA</h5>
												<p>Informujemy, że w czasie Ferii Zimowych (1-14.02) zajęcia Matplanety nie odbywają się. W tym czasie organizujemy Ferie z Robotami Lego, Programowaniem Gier Komputerowych oraz Grafiką Komputerową.<br><br>
													Do zobaczenia 15.02:)
												</p>     
                                            </div>
                                        </div><!-- end con-div -->	
										<div>
                                            <div class="local-news">
                                                <h6> 16-12-2015 </h6>
                                                <h5 class="news-header">ŚWIĄTECZNE WIEŚCI Z MATPLANETY</h5>
												<p>W grudniu ukazał się już 4 numer gazetki Matplanety!<br><br>
													A w nim:<br>
													- krótkie podsumowanie minionych miesięcy,<br>
													- zapowiedź wyczekiwanych przez wszystkich FERII ZIMOWYCH<br>
													- krótka wzmianka o świątecznych promocjach oraz o planowanych warsztatach dla rodziców,<br>
													- a przede wszystkim ZAGADKI, ZAGADKI, ZAGADKI!!!<br><br>

													W Święta nie pozwalamy się nudzić:)<br><br>

													<a target="_blank" href="http://matplaneta.pl/assets/download/gazetka_nr_4_grudzien.pdf">Pobierz gazetkę</a><br>
												</p>     
                                            </div>
                                        </div><!-- end con-div -->	
										<div>
                                            <div class="local-news">
                                                <h6> 21-08-2015 </h6>
                                                <h5 class="news-header">Miło nam poinformować, że w Matplanecie uruchomiliśmy nasz własny system komunikacji z klientami - Polygon.</h5>
												<p>W systemie są dostępne funkcjonalności pozwalające m.in. na:<br>
												•	zapisanie dziecka na zajęcia,<br>
												•	wysłanie maila do Nauczyciela lub Administratora oddziału,<br>
												•	sprawdzenie kwot i terminów opłat oraz przejrzenie uiszczonych wpłat,<br>
												•	aktualizację swoich danych kontaktowych.<br></p>
												<p>W menu „Rekomendacja” nasi stali Klienci znajda proponowaną grupę dla  dziecka na I semestr 2015/2016 i skrót do Zapisów.</p>
												<p>Pełna oferta zajęć znajduje się w menu „Zapisy”.</p>
												<p>Wierzymy, że system będzie przyjazny dla Państwa i uprości proces zapisu  oraz kontakty z biruem.</p>
												<p>Zapraszamy do rejestrowania się i zapisywania na zajęcia!</p>
												<p><a href="http://matplaneta.pl/assets/download/System%20Obs%C5%82ugi%20Klienta%20POLYGON.pdf" target="_blank">Krótka instrukcja obsługi</a></p>
												<p>Z pozdrowieniami<br>
												Zespół Matplanety</p>   
                                            </div>
                                        </div><!-- end con-div -->
                                    </li><!-- end first page/pagination -->
                                </ul><!-- end gallary -->
                                <center>
                                    <!-- pagination -->
                                    <span id="PageList" class="clear"></span>
                                </center>
                            </div><!-- end phGalarry -->
                        </div><!-- end local-news-container -->
                    </div><!-- end col12 -->
                </div><!-- end container -->
            </section>

<!-- get footer -->
<?php require("$root/inc/footer.php");?>