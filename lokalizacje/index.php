<!-- get header -->
<?php 
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require("$root/inc/header.php");?>

<!-- get sidebar-->
<?php require("$root/inc/sidebar.php");?>

<!--location-section-->
    <section id="location-section" class="wrapper">
        <div class="container">
            <div class="section-header">
                <h2>
                    Lokalizacje
                </h2>
                <img src="/assets/images/border.png" alt="border">
            </div>
			<div class="col12">
				<h4 class="news-headline">Wirtualny spacer po Matplanecie</h4>
				<p>Istnieje możliwość wirtulanego spaceru po centrum Matplanety na Ursynowie. Zapraszamy do obejrzenia przykładowych sal. Aby zobaczyć wszystkie lokalizacji przewiń stronę lub kliknij w <a class="program" href="#anchor1">link.</a></p>
					
				<div class="text-center" style="margin-top: 13px;">
					<iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1spl!2spl!4v1444902465709!6m8!1m7!1sFUcOsU93NwMAAAQo8U0udw!2m2!1d52.15627143329753!2d21.03287152690484!3f265.09!4f0!5f0.7820865974627469" width="700" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
            <div class="col12">
                <h4 class="news-headline" id="anchor1">Warszawa</h4>
                <!-- Warsaw Location -->
                <ul>

                    <li>
                        <h6>
                            CE Matplaneta Bemowo
                        </h6>

                        <div>
                            Ośrodek Kultury i Edukacji SM Wola, ul. Powstańców Śląskich 108A 
                            <br>
                            Ośrodek Kultury i Edukacji SM Wola, ul. Bolkowska 2c 
                            <br>
                            Tel. +48 22 100 53 47
                            <br>
                        </div>
                        <a class="location-button" href="/bemowo/">Zapisy</a>
                    </li>

					<li>
                        <h6>
                            CE Matplaneta Białołęka
                        </h6>

                        <div>
                            
                            ul. Skarbka z Gór 142 B
                            <br>
                            Tel. +48 22 100 53 47
                        </div>
                        <a class="location-button" href="/bialoleka/">Zapisy</a>
                    </li>
					
                    <li>
                        <h6>
                            CE Matplaneta Mokotów
                        </h6>

                        <div>
                            
                            Ene-Due Centrum Językowo-Kulturowe dla Dzieci, ul. Chodkiewicza 8
                            </br>
                            Tel. +48 22 100 53 47
                        </div>
                        <a class="location-button" href="/mokotow/">Zapisy</a>
                    </li>

                    <li>
                        <h6>
                            CE Matplaneta Tarchomin
                        </h6>

                        <div>
                            
                            ul. Myśliborska 98G
                            <br>
                            Tel. +48 730 660 770
                        </div>
                        <a class="location-button" href="/tarchomin/">Zapisy</a>
                    </li>

                    <li>
                        <h6>
                            CE Matplaneta Ursus
                        </h6>

                        <div>
                            
                            Dom Kultury Kolorowa, ul. gen.K. Sosnkowskiego 16 
                            <br>
                            Tel. +48 501 489 849
                        </div>
                        <a class="location-button" href="/ursus/">Zapisy</a>
                    </li>

                    <li>
                        <h6>
                            CE Matplaneta Ursynów
                        </h6>

                        <div>
                            
                            Al. Komisji Edukacji Narodowej 95
                            <br>
                            Tel. +48 22 100 53 47
                        </div>
                        <a class="location-button" href="/ursynow/">Zapisy</a>
                    </li>
						

                    <li>
                        <h6>
                             CE Matplaneta Wilanów
                        </h6>

                        <div>
                            
                            ul. Wandy Rutkiewicz 2
                            <br>
                            Tel. +48 22 100 53 47
                        </div>
                        <a class="location-button" href="/wilanow/">Zapisy</a>
                    </li>						

                </ul><!-- end Warsaw Location -->
            </div>

            <div class="col12">
                <h4 class="news-headline" id="anchor2">Okolice Warszawy</h4>
                <!-- start near Warsaw -->
                <ul>

                    <li>
                        <h6>
                            CE Matplaneta Grodzisk Mazowiecki
                        </h6>

                        <div>
                            
                            Pawilon Kultury ul. Westfala 3
                            <br>
                            Tel. +48 789 320 510

                        </div>
                        <a class="location-button" href="/grodzisk/">Zapisy</a>
                    </li>

                    <li>
                        <h6>
                            CE Matplaneta Józefosław
                        </h6>

                        <div>
                            
                            ul. Julianowska 67A
                            <br>Tel. +48 514 727 753
                        </div>
                        <a class="location-button" href="/jozefoslaw/">Zapisy</a>
                    </li>

                    <li>
                        <h6>
                            CE Matplaneta Józefów
                        </h6>

                        <div>
                            ul. Zawiszy Czarnego 1c
                            <br>Tel. +48 600 920 129
                        </div>
                        <a class="location-button" href="/jozefow/">Zapisy</a>
                    </li>

                    <li>
                        <h6>
                            CE Matplaneta Konstancin Jeziorna
                        </h6>

                        <div>
                            
                            ul. Sobieskiego 6 
                            <br>
                            Tel. +48 535 314 314 
                        </div>
                        <a class="location-button" href="/konstancin/">Zapisy</a>
                    </li>

                </ul><!-- end near warsaw -->
            </div>
            <div class="col12">
                <h4 class="news-headline" id="anchor3">Pozostałe miasta</h4>
                <!-- start other location -->
                <ul>

                    <li>
                        <h6>
                            CE Matplaneta Trójmiasto
                        </h6>

                        <div>
                            
                            ul. Warneńska 8c/3, Gdańsk - Morena
                            <br>
                            Tel.+48 508 129 042
                        </div>
                        <a class="location-button" href="/gdansk/">Zapisy</a>
                    </li>

                    <li>
                        <h6>
                            CE Matplaneta Kraków
                        </h6>

                        <div>
                            
                                ul. Torfowa 4, Kraków - Ruczaj 
                            <br>Dworek Białoprądnicki ul. Papiernicza 2, Kraków - Prądnik Biały 
                            <br>Klub Kultury "Mydlniki" ul. Balicka 289, Kraków - Bronowice 
                            <br>Tel. +48 533 886 942
                        </div>
                        <a class="location-button bottom-button" href="/krakow/">Zapisy</a>
                    </li>

                    <li>
                        <h6>
                            CE Matplaneta Żary
                        </h6>

                        <div>
                            
                            Przy Katolickiej Szkole Podstawowej, ul. Kalinowskiego 15
                            <br>Tel.+48 608 318 744
                        </div>
                        <a class="location-button" href="/zary/">Zapisy</a>
                    </li>
					
					<li>
                        <h6>
                            CE Matplaneta Słupsk
                        </h6>

                    <div>
                            
                            ul. Filmowa 1
                            <br>Tel. +48 501 277 810
                        </div>
                        <a class="location-button" href="/slupsk/">Zapisy</a>
                    </li>

                </ul><!-- end other location -->
            </div>
		</div>	
    </section><!-- end location-section -->

<!-- get footer -->
<?php require("$root/inc/footer.php");?>