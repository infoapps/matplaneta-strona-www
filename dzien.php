<!-- get header -->
<?php require("inc/header.php"); ?>

<!-- get sidebar-->
<?php require("inc/sidebar.php"); ?>

<?php
if (isset($_POST['submit'])) {
    $to = "zapisy@e-matplaneta.pl";
    $from = $_POST['mail'];

    // Checks if form is for math or programming demostration
    if ($_POST['lesson'] === 'math') {
        $subject = "Zapisy na lekcje pokazowe z matematyki!";
    } else if ($_POST['lesson'] === 'programming') {
        $subject = "Zapisy na lekcje pokazowe z programowania!";
    }

    $first_name = $_POST['first_name'];
    $second_name = $_POST['second_name'];

    // Checks child age since inputs have got different names attributes
    if ($_POST['child-age-programming']) {
        $child_age = $_POST['child-age-programming'];
    } else if ($_POST['Wiek']) {
        $child_age = $_POST['Wiek'];
    }

    $localization = $_POST['Lokalizacja'];
    $date = $_POST['Data'];
    $hour = $_POST['Godzina'];
    $phone = $_POST['phone'];

    $message = "<!doctype html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <title>$subject</title>
</head>
<body style='margin: 0; padding: 5%; background: #9DBB1D'>
   <img style='display: block; margin: 0 auto' src='http://infoapps.pl/matplaneta-rozwoj/assets/images/matplaneta.jpg' alt=''>
    <div style='text-align: center'><h2>$subject</h2></div>
<table style='width: 100%; max-width: 600px; margin: 0 auto; border-collapse: collapse'>
    <thead>
    </thead>
    <tbody>
    <tr>
        <td style='width: 140px;padding: 8px;font-size:  16px;border:1px solid #7E9227;color: #485218;font-weight: bold'>Imię dziecka</td>
        <td style='padding: 8px; border: 1px solid #7E9227; font-size: 14px; color: #485218; font-family: sans-serif'>$first_name</td>
    </tr>
    <tr>
        <td style='width: 140px;padding: 8px;font-size:  16px;border:1px solid #7E9227;color: #485218;font-weight: bold'>Nazwisko dziecka</td>
        <td style='padding: 8px; border: 1px solid #7E9227; font-size: 14px; color: #485218; font-family: sans-serif'>$second_name</td>
    </tr>
    <tr>
        <td style='width: 140px;padding: 8px;font-size:  16px;border:1px solid #7E9227;color: #485218;font-weight: bold'>Wiek dziecka</td>
        <td style='padding: 8px; border: 1px solid #7E9227; font-size: 14px; color: #485218; font-family: sans-serif'>$child_age</td>
    </tr>
    <tr>
        <td style='width: 140px;padding: 8px;font-size:  16px;border:1px solid #7E9227;color: #485218;font-weight: bold'>Lokalizacja</td>
        <td style='padding: 8px; border: 1px solid #7E9227; font-size: 14px; color: #485218; font-family: sans-serif'>$localization</td>
    </tr>
    <tr>
        <td style='width: 140px;padding: 8px;font-size:  16px;border:1px solid #7E9227;color: #485218;font-weight: bold'>Data</td>
        <td style='padding: 8px; border: 1px solid #7E9227; font-size: 14px; color: #485218; font-family: sans-serif'>$date</td>
    </tr>
    <tr>
        <td style='width: 140px;padding: 8px;font-size:  16px;border:1px solid #7E9227;color: #485218;font-weight: bold'>Godzina</td>
        <td style='padding: 8px; border: 1px solid #7E9227; font-size: 14px; color: #485218; font-family: sans-serif'>$hour</td>
    </tr>
    <tr>
        <td style='width: 140px;padding: 8px;font-size:  16px;border:1px solid #7E9227;color: #485218;font-weight: bold'>Telefon</td>
        <td style='padding: 8px; border: 1px solid #7E9227; font-size: 14px; color: #485218; font-family: sans-serif'>$phone</td>
    </tr>
    <tr>
        <td style='width: 140px;padding: 8px;font-size:  16px;border:1px solid #7E9227;color: #485218;font-weight: bold'>Email</td>
        <td style='padding: 8px; border: 1px solid #7E9227; font-size: 14px; color: #485218; font-family: sans-serif'>$from</td>
    </tr>
    </tbody>
</table>

</body>
</html>
";
    $headers .= "Reply-to:  ".$from."   ".PHP_EOL;
    $headers .= "From: zapisy@e-matplaneta.pl ;".PHP_EOL;
    $headers .= "MIME-Version: 1.0".PHP_EOL;
    $headers .= "Content-type: text/html; charset=utf-8".PHP_EOL;
    mail($to,$subject, $message, $headers);
    mail($from, $subject, $message, $headers);

    echo '
		<section id="form-message">
			<div id="message" class="container">
				<h1>Dziękujemy za  zapisanie wiadomości! Za chwile dostaniesz maila z potwierdzeniem.</h1>
				<a class="back-button" href="dzien.php">Wróć</a>
			</div>
		</section>
		';
    } else {
        require("demonstration-form.php");
    }

?>

<?php require("inc/footer.php"); ?>

