<!-- get header -->
<?php $root = realpath($_SERVER["DOCUMENT_ROOT"]);
require("$root/inc/header.php");?>

<!-- get sidebar-->
<?php require("$root/inc/sidebar.php");?>

<!-- start location -->
            <section id="location" class="wrapper">
                <div class="container">
                    <div class="section-header">
                        <h2> Kraków </h2>
                        <img src="/assets/images/border.png" alt="border">
                    </div>

                    <div class="col12">
                        <a href="http://polygon.matplaneta.pl/Oferta.aspx?region=krakow" class="zapisy-button">Zapisy</a>
                       <!-- <a href="http://matplaneta.pl/formularze/wakacje-krakow.php"
                           class="zapisy-button">WakacjeKraków</a>-->
                    </div>

                    <div class="col12 row"></div>
                    <div class="col6">
                        <img class="img-responsive" src="/assets/images/DSC_0213.jpg" alt="Zajęcia w Krakowie">
                    </div>
                    <div class="col6">
                        <ul>
                            <li>
                                <h4> Centrum Edukacyjne Matplaneta Kraków </h4></li>
                            <li>
                                <p>ul. Torfowa 4, Kraków - Ruczaj

								    <br>Dworek Białoprądnicki, ul. Papiernicza 2, Kraków – Prądnik Biały 
                                    <br>Klub Kultury "Mydlniki", ul. Balicka 289, Kraków – Bronowice
                                    <br>Tel. +48 (12) 444 68 28 
                                    <br>Tel. +48 533 886 942
                                    <br>Biuro obsługi klienta: +48 (22) 100 53 47 
                                    <br>mail: krakow@e-matplaneta.pl  
                                    <br>mail: biuro@matplaneta.pl 
                                </p>
                            </li>
                        </ul>
                    </div>
					<div class="col12">
                        <ul>
                            <li>
                                <h5> Podmiot świadczący usługi edukacyjne: </h5></li>
                            <li>
                                <p>Matplaneta SA, ul. Targowa 20a, 03-731 Warszawa, NIP 1132877516</p>
                            </li>
                            <li>
                                <h5> Nr konta bankowego:</h5></li>
                            <li>
                                <p>38 1950 0001 2006 0889 8214 0002 (Idea Bank)</p>
                            </li>							
                        </ul>
					</div>

                    <div class="col12 row"></div>
                    <div class="col12">
                        <iframe src="https://www.google.com/maps/d/embed?mid=zBFk1ypiCdAU.kTv-F_KvlX0Y&z=12" width="100%" height="345"></iframe>

                        </iframe>
                    </div>
                    <?php require("$root/inc/harmonogram.php");?>
                    <div class="col12">
                        <h4 class="news-headline"> Aktualności </h4>
                        <div class="local-news-container">
                            <div class="phGallary">
                                <ul id="gallary">
                                    <li>
										<div>
                                            <div class="local-news">
                                                <h6> 29-02-2016 </h6>
                                                <h5 class="news-header">Wydrukuj własny świat w Matplanecie - Projektowanie 3D</h5>
												<p>
												Podczas kursu uczestnicy będą mieli możliwość zaprojektować i stworzyć, czyli wydrukować na drukarce 3D, własne wymyślone miasto. <br><br>
												Inżynier Konstruktor, Artysta Postaci, Architekt, Projektant Postaci do gry komputerowej i/albo animacji komputerowej - te wszystkie zawody wykorzystują narzędzie do projektowania - modeler. Modelowanie 3D to proces tworzenia obiektów trójwymiarowych za pomocą programu komputerowego.<br><br>
												Poznaj moc takich programów do modelowania jak Wings 3D, Sculptris, Tinkercad i Blender! Nauczysz się różnych podejść do modelowania oraz dowiesz się, jak używać Slicer'ów (Cura, Sli3er...). Slicer to program do dzielenia obiektu na warstwy i generowania kodu zrozumiałego przez drukarkę 3D. <br> 
												Proponujemy 8 spotkań po 90 minut. <br>
												Cena kursu : 480 zł lub w 2 ratach po 250 zł. Dla naszych słuchaczy zniżka 15%.<br>
												Terminy: 1 grupa - wtorki 18.20 , 2 grupa - czwartki 16.30 <br>
												Lokalizacja: Centrum Edukacyjne Matplaneta Kraków - Ruczaj, ul. Torfowa 4. Dla dzieci i młodzieży od 8 lat, tel.  <br>
												Zaczynamy 8 marca!
												</p> 
                                        </div>
                                        </div>
										<div>
                                            <div class="local-news">
                                                <h6> 10-02-2016 </h6>
                                                <h5 class="news-header">DNI OTWARTE</h5>
												<p>W Krakowie już 22 i 26 lutego odbędą się lekcje pokazowe Matplanety w szkołach.<br><br>
A na początku marca wszystkich zainteresowanych zajęciami w Matplanecie zapraszamy na bezpłatne Dni Otwarte w Centrach Edukacyjnych na Ursynowie, Tarchominie, Bemowie oraz w Krakowie.<br><br>
Terminy, godziny oraz szczegółowe informacje znajdziecie tutaj: <a href="http://matplaneta.pl/dni_zapisy/public/dni_otwarte">http://matplaneta.pl/dni_zapisy/public/dni_otwarte.</a> <br><br>
Zapraszamy do zapisów!!!
									</p>    
                                        </div>
                                        </div>
										<div>
                                            <div class="local-news">
                                                <h6> 01-02-2016 </h6>
                                                <h5 class="news-header">PO FERIACH</h5>
												 <p>W Krakowie wracamy na zajęcia Matplanety już 1. lutego.</br></br>Do zobaczenia:)
												</p>    
                                            </div>
                                        </div>
										<div>
                                            <div class="local-news">
                                                <h6> 11-01-2015 </h6>
                                                <h5 class="news-header">DNI OTWARTE W KRAKOWIE!!!</h5>
												 <p>
Już 30 stycznia 2016 od godz. 9:00 zapraszamy wszystkie dzieci na lekcję pokazową do Matplanety w Centrum Ruczaj (ul. Torfowa 4 - przecznica Kobierzyńskiej).<br><br>
A najbliższe lekcje pokazowe z Matplanetą w krakowskich szkołach odbędą się już w lutym!<br><br>
Szczegółowe informacje będą zamieszczane na stronie internetowej;) 	<br><br>
												</p>    
                                            </div>
                                        </div>
										<div>
                                            <div class="local-news">
                                                <h6> 11-01-2015 </h6>
                                                <h5 class="news-header">UWAGA!!! RABAT 15% NA FERIE Z MATPLANETĄ</h5>
												 <img style="max-height: 280px;" src="/assets/images/rabat.png">
												 <p>
Dla rodzeństw oraz uczniów naszych zajęć mamy specjalną ofertę!!!<br><br>
Wiedząc, że cena jednego turnusu Ferii w Matplanecie wynosi 680zł, rozwiążcie zagadkę ze zdjęcia:) Grafika komputerowa / Tworzenie gier komputerowych / Budowanie robotów mogą stać się pasją Twojego dziecka - Zapisz je już teraz: http://matplaneta.pl/zapisy-ferie-2016/
									</p>     
                                            </div>
                                        </div><!-- end con-div -->	
										<div>
                                            <div class="local-news">
                                                <h6> 05-01-2015 </h6>
                                                <h5 class="news-header">ŚWIĄTECZNE WIEŚCI Z MATPLANETY</h5>
												<p>
												FERIE Z MATPLANETĄ już za niecały miesiąc, a Wy wciąż możecie dopisać swoje dziecko na:<br><br>

												- ROBOTY LEGO WEDO<br>
												- GRAFIKĘ KOMPUTEROWĄ <br>
												- PROGRAMOWANIE GIER KOMPUTEROWYCH<br><br>

												W czasie naszych ferii dzieci rozwiną wyobraźnię i logiczne myślenie oraz przeżyją wspaniałą przygodę, którą zapamiętają na długo!!! Zapiszcie się już dziś!<br><br>

												Wystarczy wejść na stronę i wypełnić formularz: <a href="http://matplaneta.pl/zapisy-ferie-2016/">http://matplaneta.pl/zapisy-ferie-2016/</a><br><br>
												Specjalne rabaty dla rodzeństwa oraz uczestników zajęć Matplanety:)
												</p>     
                                            </div>
                                        </div><!-- end con-div -->	
										<div>
                                            <div class="local-news">
                                                <h6> 12-12-2015 </h6>
                                                <h5 class="news-header">FERIE Z MATPLANETĄ - Najlepsza przygoda dla każdego dziecka!</h5>
												<p>Wszystkie dzieci, które zostają w mieście chociaż przez jeden tydzień ferii, zapraszamy na fantastyczne zajęcia w Matplanecie. Proponujemy całodniową opiekę, zajęcia w pracowni komputerowej, gry i zabawy nie tylko matematyczne, pyszne posiłki i mnóstwo zabawy.<br><br>

													Młodsze dzieci zapraszamy na spotkanie z robotami Lego.<br>
													Dla nieco starszych proponujemy w tym roku nowości: Projektowanie gier komputerowych oraz Grafikę komputerową (również dla Dziewczynek!).<br><br>

													Więcej informacji i zapisy: <a href="http://matplaneta.pl/zapisy-ferie-2016/">tutaj</a><br>
													Zarezerwuj miejsce dla swojego dziecka już dziś!
												</p>    
                                            </div>
                                        </div><!-- end con-div -->
									<div>
                                            <div class="local-news">
                                                <h6> 27-09-2015 </h6>
                                                <h5 class="news-header">Festiwal Kino Dzieci</h5>
												<p>W najbliższe niedziele Matplaneta w Krakowie zaprasza do Kina pod Baranami na festiwal Kino Dzieci <a href="http://www.kinopodbaranami.pl/film.php?film_id=8491" target="_blank">http://www.kinopodbaranami.pl/film.php?film_id=8491</a></br></p>
												<p>Dla tych, którzy nie wiedzą jeszcze, co MATPLANETA ma wspólnego z Biurem detektywistycznym Lessego i Mai podpowiadamy: główkowanie!!!</br></br>
												 A co wspólnego MATPLANETA ma z Jill i Joy? Wyobraźnię i świetną zabawę!!!</br></br>
												Na główkowanie z wyobraźnią i świetną zabawę = na warsztaty MATPLANETY w ramach festiwalu Kino Dzieci zapraszamy w najbliższe niedziele do Kina pod Baranami w Krakowie:</br></br>
												<b>27 września:</b>  Biuro detektywistyczne Lessego i Mai  <a href="http://www.kinopodbaranami.pl/film.php?film_id=8491" target="_blank">http://www.kinopodbaranami.pl/film.php?film_id=8491</a></br>
												<b>4 października:</b> Jill i Joy <a href="http://www.kinopodbaranami.pl/film.php?film_id=8496" target="_blank">http://www.kinopodbaranami.pl/film.php?film_id=8496</a></p>
												</p>    
                                            </div>
                                        </div><!-- end con-div -->	
									<div>
										<div>
                                            <div class="local-news">
                                                <h6> 18-09-2015 </h6>
                                                <h5 class="news-header">Już jutro startujemy!</h5>
												<p>Już w tą sobotę (19.09) ruszamy z pierwszymi zajęciami w naszych placówkach.<br>
													Od samego rana grupy naszych starych oraz nowych uczniów rozpoczną przygodę z matematycznymi oraz logicznymi wyzwaniami. Będziemy programować pierwsze programy komputerowe oraz łamać głowy nad trudnymi zadaniami.<br>
													Wszyscy czekamy z niecierpliwością na jutrzejszy dzień i nie możemy się już doczekać!
												</p>    
                                            </div>
                                        </div><!-- end con-div -->	
									<div>
                                            <div class="local-news">
                                                <h6> 17-09-2015 </h6>
                                                <h5 class="news-header">Weekend w krakowskiej Matplanecie</h5>

												<p>Za nami intensywny weekend w krakowskiej Matplanecie: Gościliśmy Państwa na naszym dniu otwartym, wzięliśmy udział w targach zajęć pozalekcyjnych w Galerii Bronowickiej i żegnaliśmy lato z Dzielnicą Dębniki. Ogłosiliśmy też kilka konkursów. Historia autora jednej z gier – nagród po raz kolejny pokazuje, że matematyka rozwija umiejętność radzenia sobie z różnego kalibru problemami. Ten, z którym zmierzył się Karol Borsuk był doprawdy ciężki: jak po zamknięciu uniwersytetu w czasie wojny i okupacji profesor matematyki ma utrzymać rodzinę? I wymyślił grę: „Hodowla zwierzątek”, na tyle dobrze pomyślaną, że cieszyła się wielkim powodzeniem w okupowanej Warszawie.
A niezawodne dzieciaki, pogimnastykowały się na naszych łamigłówkach i  wyśmienicie poradziły sobie z naszymi konkursami.
												</p>   
                                            </div>
                                        </div><!-- end con-div -->
										<div>
                                            <div class="local-news">
                                                <h6> 17-09-2015 </h6>
                                                <h5 class="news-header">Ostatni dzień dni otwartych</h5>

												<p>We czwartek 17 IX zapraszamy na ostatni spośród dni otwartych w naszym krakowskim centrum przy Torfowej 4 a w niedzielę 20 IX gościmy na dniu otwartym Klubu Kultury Mydlniki przy ulicy Balickiej 289. Dziękujemy naszym gościom, zwłaszcza tym małym, którzy od razu poczuli, że w Matplanecie są u siebie (albo jak powiedziałby Hegel: są wolni = są sobą u siebie)
												</p>   
                                            </div>
                                        </div><!-- end con-div -->
										<div>
                                            <div class="local-news">
                                                <h6> 11-09-2015 </h6>
                                                <h5 class="news-header">Sobota z Matplanetą</h5>

												<p>Jeśli nie udało Wam się zapisać na nasz dzień otwarty 12 września, to zapraszamy na  Święto Dzielnicy Dębniki (ul. Kobierzyńska 174, Rancho Texas od 14.00 do 15.00 .<br>
												<a href="http://dzielnica8.krakow.pl/87-aktualnnosci/303-swieto-dzielnicy-viii-debniki" target="_blank">http://dzielnica8.krakow.pl/87-aktualnnosci/303-swieto-dzielnicy-viii-debniki</a><br>
												W sobotę i niedzielę będziemy też na targach zajęć pozalekcyjnych w galerii Bronowice.<br>
												Zapraszamy serdecznie!
												</p>   
                                            </div>
                                        </div><!-- end con-div -->
										<div>
                                            <div class="local-news">
                                                <h6> 10-09-2015 </h6>
                                                <h5 class="news-header">Złoty Słonecznik dla krakowskiej Matplanety!</h5>

												<p>W miniony piątek odebraliśmy w Krakowie wielkie wyróżnienie:) Podczas Gali zwieńczającej piątą edycję Festiwalu Słoneczniki - za najbardziej rozwojowe inicjatywy dla dzieci, został nam przyznany przez zespół ekspertów ZŁOTY SŁONECZNIK - w kategorii LOGIKA!!!<br>
												Gratulujemy!<br><br>
												więcej: <a href="http://sloneczniki.czasdzieci.pl/aktualnosci/id,81-sloneczniki_2015_czyli.html" target="_blank">http://sloneczniki.czasdzieci.pl/aktualnosci/id,81-sloneczniki_2015_czyli.html</a>
												</p>   
                                            </div>
                                        </div><!-- end con-div -->
										<div>
                                            <div class="local-news">
                                                <h6> 04-09-2015 </h6>
                                                <h5 class="news-header">Biuro Matplanety</h5>

												<p>Biuro Matplanety w Krakowie jest czynne jutro 9-14, a w tygodniu 10-19.</p>    
                                            </div>
                                        </div><!-- end con-div -->
										<div>
                                            <div class="local-news">
                                                <h6> 31-08-2015 </h6>
                                                <h5 class="news-header">STARTUJEMY!!!</h5>

												<p>Po wakacyjnej przerwie zapraszamy na nasze krakowskie Dni otwarte Matplanety! <br>
												Jeżeli chcecie Państwo zobaczyć swoje dzieci w akcji, chcecie zobaczyć na ich buziach radość odkrywców, satysfakcję zwycięzców, jeżeli chcecie się przekonać, że łamanie główki może sprawiać przyjemność, to przyprowadźcie je na nasze dni otwarte:<br>
												5 IX sobota, godz.: 9:00 – 12:00    Dworek Białoprądnicki, ul. Papiernicza 2<br>
												10 IX czwartek, godz.: 18:20 – 19:30 Centrum Matplanety na Ruczaju, ul. Torfowa 4<br>
												12 IX sobota, godz. 9:30 – 12:30 Centrum Matplanety na Ruczaju, ul. Torfowa 4<br>
												17 IX czwartek, godz.: 18:20 – 19:30 Centrum Matplanety na Ruczaju, ul. Torfowa 4<br></p>
												<p>Rodzice będą mogli zadać nam pytania dotyczące zajęć, a w naszym Centrum także obejrzeć miejsca, gdzie będziemy się uczyć – powstała wreszcie w Krakowie Matplanetowa pracownia komputerowa i możemy ruszyć z zajęciami z programowania!</p>    
                                            </div>
                                        </div><!-- end con-div -->
										<div>
                                            <div class="local-news">
                                                <h6> 21-08-2015 </h6>
                                                <h5 class="news-header">Miło nam poinformować, że w Matplanecie uruchomiliśmy nasz własny system komunikacji z klientami - Polygon.</h5>

												<p>W systemie są dostępne funkcjonalności pozwalające m.in. na:<br>
												•	zapisanie dziecka na zajęcia,<br>
												•	wysłanie maila do Nauczyciela lub Administratora oddziału,<br>
												•	sprawdzenie kwot i terminów opłat oraz przejrzenie uiszczonych wpłat,<br>
												•	aktualizację swoich danych kontaktowych.<br></p>
												<p>W menu „Rekomendacja” nasi stali Klienci znajda proponowaną grupę dla  dziecka na I semestr 2015/2016 i skrót do Zapisów.</p>
												<p>Pełna oferta zajęć znajduje się w menu „Zapisy”.</p>
												<p>Wierzymy, że system będzie przyjazny dla Państwa i uprości proces zapisu  oraz kontakty z biruem.</p>
												<p>Zapraszamy do rejestrowania się i zapisywania na zajęcia!</p>
												<p><a href="http://matplaneta.pl/assets/download/System%20Obs%C5%82ugi%20Klienta%20POLYGON.pdf" target="_blank">Krótka instrukcja obsługi</a></p>
												<p>Z pozdrowieniami<br>
												Zespół Matplanety</p>     
                                            </div>
                                        </div><!-- end con-div -->
                                        <div>
                                            <div class="local-news">
                                                <h6> 19-06-2015 </h6>
                                                <h5 class="news-header">Jesteśmy w trakcie przygotowania planu zajęć Matplanety na rok szkolny 2015-2016. </h5>
                                                <p class="second-news">Zapraszamy do zapisów w drugiej połowie sierpnia.
                                                    Ale już teraz można wypełnić  formularz zgłoszenia chęci udziału dziecka w zajęciach Matplanety w roku 2015-2016.
                                                    Dzięki temu będziemy mogli stworzyć harmonogram zajęć najlepiej odpowiadający Państwa potrzebom.</p>
                                            </div><!-- end local-news -->
                                        </div><!-- end con-div -->
                                        <div>
                                            <div class="local-news">
                                                <h6> 16-06-2015 </h6>
                                                <h5 class="news-header">Święto dzielnicy Borek Fałęcki</h5>

                                                <p>W ostatnią niedzielę (14 czerwca) gościliśmy na święcie dzielnicy Borek Fałęcki

                                                    I zaobserwowaliśmy  ciekawą prawidłowość:

                                                    Im więcej wysiłku rodzic zmuszony był włożyć w przekonanie dziecka do spróbowania naszych łamigłówek,

                                                    tym więcej musiał potem musi użyć siły, by swoje dziecko od nich oderwać!
                                                    </br>
                                                    Dzieci, które poznały już swoją siłę myślenia, nie odpuszczały nowych wyzwań, nawet kiedy zaczął się występ klauna a słońce zaczęło palić niemiłosiernie.

                                                    Na nowe ciekawe wyzwania zapraszamy póki co na wakacje do nowego Centrum przy Torfowej <a class="program" href="http://matplaneta.pl/formularze/wakacje-krakow.php" target="_blank">tutaj.</a>
                                                </p>
                                            </div><!-- end local-news -->
                                        </div><!-- end con-div -->
                                        <div>
                                            <div class="local-news">
                                                <h6> 02-06-2015 </h6>
                                                <h5 class="news-header">Kto nie wierzy, że dzieci uwielbiają myśleć i lubią wyzwania, powinien był przyjść do nas na święto Białego Prądnika.</h5>

                                                <p>Starsi i młodsi ochoczo łamali sobie głowy nad sposobami optymalnego transportu przez rzekę, przekładem z tajemniczego języka, czy zagadką Einsteina. A uporawszy się z nimi, dzieci prosiły o więcej i więcej. Nie chciały więcej cukierków, balonów czy innych nagród – chciały więcej zagadek!
                                                    </br>
                                                    Mamy ich jeszcze sporo w zanadrzu, więc zapraszamy do Matplanety!
                                                    </br>
                                                    Następne spotkanie w Krakowie planujemy już 14 czerwca. Będziemy wtedy gościć na święcie dzielnicy Borek Fałęcki na stadionie KS Borek przy ul. Żywieckiej 13.
                                                </p>
                                            </div><!-- end local-news -->
                                        </div><!-- end con-div -->
                                        <div>
                                            <div class="local-news">
                                                <h6> 25-05-2015 </h6>
                                                <h5 class="news-header">Matplaneta ma już swoje centrum w Krakowie! Na Ruczaju, przy ul. Torfowej 4.</h5>

                                                <p>
                                                    Startujemy już 29 czerwca!
                                                    <br>
                                                    Zapraszamy do naszego Centrum na: Wakacje z robotami Lego i językiem angielskim.
                                                    <br>

                                                    Więcej informacji dostępnych
                                                    <a class="program" href="assets/download/MTPLNT_A5_krakowSMALL.pdf" target="_blank">tutaj.</a>

                                                </p>
                                            </div><!-- end local-news -->
                                        </div><!-- end con-div -->
                                    </li><!-- end first-page -->
                                </ul><!-- end gallary -->
                                <center>
                                    <span id="PageList" class="clear"></span>
                                </center>
                            </div><!-- end phGallary -->
                           <!-- <div class="local-news">
                                <h6> 11-01-2014 </h6>
                                <h5 class="news-header"> Nowy semestr zajęć w Matplanecie rusza już 13 lutego! </h5>

                                <p> Można jeszcze dopisywać dzieci niemalże do wszystkich grup w naszych placówkach, a
                                    więc zapraszamy do zapoznania się z HARMONOGRAMEM.
                                    <br>Dzieci, które chodziły na nasze zajęcia w tym semestrze, są automatycznie
                                    przenoszone na kolejny.</p>
                            </div>-->
                        </div><!-- end local-news-container -->
                    </div><!-- end col12 -->
                </div><!-- end container -->
            </section>

<!-- get footer -->
<?php require("$root/inc/footer.php");?>