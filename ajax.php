<?php

// We will be using this class to define each selectbox

class SelectBox{
	public $items = array();
	public $defaultText = '';
	public $title = '';
	
	public function __construct($title, $default){
		$this->defaultText = $default;
		$this->title = $title;
	}
	
	public function addItem($name, $connection = NULL){
		$this->items[$name] = $connection;
		return $this; 
	}
	
	public function toJSON(){
		return json_encode($this);
	}
}


/* Configuring the selectboxes */

// Product selectbox

$locationSelect = new SelectBox('Lokalizacja','Wybierz lokalizacje');
$locationSelect->addItem('Ursynów		/al. KEN 95, kl. 18a','ursynowSelect')
			  ->addItem('Tarchomin		/ul. Myśliborska 98g','tarchominSelect')
			  ->addItem('Bemowo			/ul. Powst. Śląskich 108a','bemowoSelect')
			  ->addItem('Kraków			/ul. Torfowa 4','krakowSelect')
			  ->addItem('Grodzisk			/ul. Westfala 3','grodziskSelect')
			  ->addItem('Konstancin			/ul. Sobieskiego 6','konstancinSelect')
			  ->addItem('Józefosław			/ul. Julianowska 67A','jozefoslawSelect');

$jozefoslawSelect = new SelectBox('Data',"Wybierz datę");
$jozefoslawSelect->addItem('5.09','firstdatejozefoslawSelect');
$firstdatejozefoslawSelect = new SelectBox('Godzina','Wybierz godzinę');
$firstdatejozefoslawSelect->addItem('11:00','11:00')
						  ->addItem('13:00','13:00');
			  
$grodziskSelect = new SelectBox('Data',"Wybierz datę");
$grodziskSelect->addItem('19.09','firstdategrodziskSelect');
$firstdategrodziskSelect = new SelectBox('Godzina','Wybierz godzinę');
$firstdategrodziskSelect->addItem('14:30','14:30');


$konstancinSelect = new SelectBox('Data',"Wybierz datę");
$konstancinSelect->addItem('12.09','firstdatekonstancinSelect');
$firstdatekonstancinSelect = new SelectBox('Godzina','Wybierz godzinę');
$firstdatekonstancinSelect->addItem('14:00 - grupa 10-11 lat','14:00 - grupa 10-11 lat ')
						  ->addItem('15:00 - grupa 12-14 lat','15:00 - grupa 12-14 lat ');
			  // First location/ Ursynów

$ursynowSelect = new SelectBox('Data', 'Wybierz datę');
$ursynowSelect->addItem('08.09 BRAK WOLNYCH MIEJSC','firstdateursynowSelect')
			->addItem('18.09 BRAK WOLNYCH MIEJSC','seconddateursynowSelect');

// Second location/ Tarchomin

$tarchominSelect = new SelectBox('Data', 'Wybierz datę');
$tarchominSelect->addItem('10.09 BRAK WOLNYCH MIEJSC','firstdatetarchominSelect');

// Third location/ Bemowo

$bemowoSelect = new SelectBox('Data',"Wybierz datę");
$bemowoSelect->addItem('16.09 BRAK WOLNYCH MIEJSC','firstdatebemowoSelect');

// Fourth location/ Krakow 

$krakowSelect = new SelectBox('Data','Wybierz datę');
$krakowSelect->addItem('10.09','firstdatekrakowSelect')
			 ->addItem('12.09','seconddatekrakowSelect')
			 ->addItem('17.09','thirddatekrakowSelect');

// First hour Ursynow

$firstdateursynowSelect = new SelectBox('Godzina','Wybierz godzinę');
$firstdateursynowSelect->addItem('18:20  BRAK WOLNYCH MIJESC','18:20  BRAK WOLNYCH MIJESC');

// Second hour Ursynow

$seconddateursynowSelect = new SelectBox('Godzina','Wybierz godzinę');
$seconddateursynowSelect->addItem('16:30  BRAK WOLNYCH MIEJSC','16:30  BRAK WOLNYCH MIEJSC');

// First hour Tarchomin

$firstdatetarchominSelect = new SelectBox('Godzina','Wybierz godzinę');
$firstdatetarchominSelect->addItem('18:20  BRAK WOLNYCH MIEJSC','18:20  BRAK WOLNYCH MIEJSC');

// First hour Bemowo

$firstdatebemowoSelect = new SelectBox('Godzina','Wybierz godzinę');
$firstdatebemowoSelect->addItem('18:20  BRAK WOLNYCH MIEJSC','18:20  BRAK WOLNYCH MIEJSC');

//First hour Krakow

$firstdatekrakowSelect = new SelectBox('Godzina','Wybierz godzinę');
$firstdatekrakowSelect->addItem('18:20','18:20');	

//Second hour Krakow

$seconddatekrakowSelect = new SelectBox('Godzina','Wybierz godzinę');
$seconddatekrakowSelect->addItem('09.30','09:30');	

//Third hour Krakow

$thirddatekrakowSelect = new SelectBox('Godzina','Wybierz godzinę');
$thirddatekrakowSelect->addItem('18:20','18:20');	

// Register all the select items in an array

$selects = array(
	'locationSelect'			=> $locationSelect,
	'ursynowSelect'			=> $ursynowSelect,
	'bemowoSelect'		=> $bemowoSelect,
	'tarchominSelect'	=> $tarchominSelect,
	'krakowSelect'			=> $krakowSelect,
	'firstdateursynowSelect'	=> $firstdateursynowSelect,
	'seconddateursynowSelect'	=> $seconddateursynowSelect,
	'firstdatetarchominSelect'	=> $firstdatetarchominSelect,
	'firstdatebemowoSelect'		=> $firstdatebemowoSelect,
	'firstdatekrakowSelect'		=> $firstdatekrakowSelect,
	'seconddatekrakowSelect'	=> $seconddatekrakowSelect,
	'thirddatekrakowSelect'     => $thirddatekrakowSelect,
	'firstdategrodziskSelect'		=> $firstdategrodziskSelect,
	'grodziskSelect'		=> $grodziskSelect,
	'firstdatekonstancinSelect'		=> $firstdatekonstancinSelect,
	'konstancinSelect'		=> $konstancinSelect,

	'jozefoslawSelect'		=> $jozefoslawSelect,
	'firstdatejozefoslawSelect'		=> $firstdatejozefoslawSelect,
	
);

// We look up this array and return a select object depending
// on the $_GET['key'] parameter passed by jQuery

// You can modify it to select results from a database instead

if(array_key_exists($_GET['key'],$selects)){
	header('Content-type: application/json');
	echo $selects[$_GET['key']]->toJSON();
}
else{
	header("HTTP/1.0 404 Not Found");
	header('Status: 404 Not Found');
}

?>