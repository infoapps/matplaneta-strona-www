<!-- get header -->
<?php $root = realpath($_SERVER["DOCUMENT_ROOT"]);
require("$root/inc/header.php");?>

<!-- get sidebar-->
<?php require("$root/inc/sidebar.php");?>

<!-- start location -->
            <section id="location" class="wrapper">
                <div class="container">
                    <div class="section-header">
                        <h2> Żary </h2>
                        <img src="/assets/images/border.png" alt="border">
                    </div>
                    <div class="col12">
                        <a href="http://polygon.matplaneta.pl/Oferta.aspx" class="zapisy-button">Zapisy</a>
                    </div>
                    <div class="col12 row"></div>
                    <div class="col6">
                        <img class="img-responsive" src="/assets/images/a.jpg" alt="Zajęcia w Żarach">
                    </div>
                    <div class="col6">
                        <ul>
                            <li>
                                <h4> Centrum Edukacyjne Matplaneta Żary </h4></li>
                            <li>
                                <p> Przy Katolicka Szkoła Podstawowa, ul. Kalinowskiego 15, Żary

                                    <br>Partner regionalny Edyta Piskorowska 
                                    <br>Tel. +48 608 318 744 
                                    <br>Biuro obsługi klienta: +48 (22) 100 53 47 
                                    <br>mail: zary@e-matplaneta.pl  
                                    <br>mail: biuro@matplaneta.pl 

                                </p>
                            </li>
                        </ul>
                    </div>
					<div class="col12">
                        <ul>
                            <li>
                                <h5> Podmiot świadczący usługi edukacyjne: </h5></li>
                            <li>
                                <p>NAT Edyta Piskorowska, ul. Długa 18/3, 68-100 Żagań NIP 9281744109</p>
                            </li>
                            <li>
                                <h5> Nr konta bankowego:</h5></li>
                            <li>
                                <p>40 1090 2561 0000 0001 2414 8465</p>
                            </li>							
                        </ul>
					</div>
                    <div class="col12 row"></div>
                    <div class="col12">
                        <iframe src="https://www.google.com/maps/d/embed?mid=zBFk1ypiCdAU.kdRK4HYSmQwI&z=17" width="100%" height="400">

                        </iframe>
                    </div>
					<?php require("$root/inc/harmonogram.php") ?>
                    <div class="col12">
                        <h4 class="news-headline"> Aktualności </h4>
                        <div class="local-news-container">
                            <div class="phGallary">
                                <ul id="gallary">
                                    <li>
										<div>
                                            <div class="local-news">
                                                <h6> 18-09-2015 </h6>
                                                <h5 class="news-header">Już jutro startujemy!</h5>
												<p>Już w tą sobotę (19.09) ruszamy z pierwszymi zajęciami w naszych placówkach.<br>
													Od samego rana grupy naszych starych oraz nowych uczniów rozpoczną przygodę z matematycznymi oraz logicznymi wyzwaniami. Będziemy programować pierwsze programy komputerowe oraz łamać głowy nad trudnymi zadaniami.<br>
													Wszyscy czekamy z niecierpliwością na jutrzejszy dzień i nie możemy się już doczekać!
												</p>    
                                            </div>
                                        </div><!-- end con-div -->	
										<div>
                                            <div class="local-news">
                                                <h6> 21-08-2015 </h6>
                                                <h5 class="news-header">Miło nam poinformować, że w Matplanecie uruchomiliśmy nasz własny system komunikacji z klientami - Polygon.</h5>

												<p>W systemie są dostępne funkcjonalności pozwalające m.in. na:<br>
												•	zapisanie dziecka na zajęcia,<br>
												•	wysłanie maila do Nauczyciela lub Administratora oddziału,<br>
												•	sprawdzenie kwot i terminów opłat oraz przejrzenie uiszczonych wpłat,<br>
												•	aktualizację swoich danych kontaktowych.<br></p>
												<p>W menu „Rekomendacja” nasi stali Klienci znajda proponowaną grupę dla  dziecka na I semestr 2015/2016 i skrót do Zapisów.</p>
												<p>Pełna oferta zajęć znajduje się w menu „Zapisy”.</p>
												<p>Wierzymy, że system będzie przyjazny dla Państwa i uprości proces zapisu  oraz kontakty z biruem.</p>
												<p>Zapraszamy do rejestrowania się i zapisywania na zajęcia!</p>
												<p><a href="http://matplaneta.pl/assets/download/System%20Obs%C5%82ugi%20Klienta%20POLYGON.pdf" target="_blank">Krótka instrukcja obsługi</a></p>
												<p>Z pozdrowieniami<br>
												Zespół Matplanety</p>     
                                            </div>
                                        </div><!-- end con-div -->
                                        <div>
                                            <div class="local-news">
                                                <h6> 19-06-2015 </h6>
                                                <h5 class="news-header">Jesteśmy w trakcie przygotowania planu zajęć Matplanety na rok szkolny 2015-2016. </h5>
                                                <p class="second-news">Zapraszamy do zapisów w drugiej połowie sierpnia.Ale już teraz można wypełnić  formularz zgłoszenia chęci udziału dziecka w zajęciach Matplanety w roku 2015-2016.Dzięki temu będziemy mogli stworzyć harmonogram zajęć najlepiej odpowiadający Państwa potrzebom.
                                                </p>
                                            </div><!-- end local-news -->
                                        </div><!-- end con-div -->
                                        <div>
                                            <div class="local-news">
                                                <h6> 11-01-2014 </h6>
                                                <h5 class="news-header"> Nowy semestr zajęć w Matplanecie rusza już 13 lutego! </h5>

                                                <p> Można jeszcze dopisywać dzieci niemalże do wszystkich grup w naszych placówkach, a
                                                    więc zapraszamy do zapoznania się z HARMONOGRAMEM.

                                                    <br>Dzieci, które chodziły na nasze zajęcia w tym semestrze, są automatycznie
                                                    przenoszone na kolejny.

                                                </p>
                                            </div><!-- end local-news -->
                                        </div><!-- end con-div -->
                                    </li><!-- end first-page -->
                                </ul><!-- end gallary -->
                                <center>
                                    <span id="PageList" class="clear"></span>
                                </center>
                            </div><!-- end phGallary -->
                        </div><!-- end local-news-container -->
                    </div><!-- end col12 -->
                </div><!-- end container -->
            </section>

<!-- get footer -->
<?php require("$root/inc/footer.php");?>