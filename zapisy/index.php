<!-- get header -->
<?php $root = realpath($_SERVER["DOCUMENT_ROOT"]);
require("$root/inc/header.php");?>

<!-- get sidebar-->
<?php require("$root/inc/sidebar.php");?>

<!-- popup -->
<div class="popup">
    <button class="close-button">x</button>
	<div class="info"><p>Zajęcia z programowania i matematyki.</p>
        <a class="zapisy" href="http://polygon.matplaneta.pl/Oferta.aspx">Harmonogram i zapisy</a>
    </div>
</div>


<!-- start zapisy -->
            <section id="zapisy" class="wrapper">
                <div class="container">
                    <div class="section-header">
                        <h2>
                            Zapisy i ceny
                        </h2>
                        <img src="/assets/images/border.png" alt="">
                    </div>
                    <div class="regulamin">
                        <h5>
                            Lubimy, kiedy rzeczy są proste. Tak też ustalamy nasze ceny.
                        </h5>

                        <h5>
                            Cena obejmuje dwugodzinne spotkania 1 raz w tygodniu (2 godziny lekcyjne) przez okres
                            jednego semestru.
                        </h5>

                        <p>
                            W semestrze odbywa się 17 spotkań (34 lekcje) w przypadku zajęć z Matematyki i Programowania
                            komputerów oraz 10 spotkań (20 lekcji) w przypadku Programowania robotów.
                        </p>
                        <h5>
 Matematyka, Programowanie komputerów, Programowanie Robotów - opłata za jeden semestr wynosi 980 zł w przypadku jednorazowej wpłaty lub 5 rat po 199 zł, płatnych wg harmonogramu:
                        </h5>

                        <p>
                            •	I rata w ciągu 5 dni roboczych od zapisu, nie później niż przed dniem rozpoczęcia zajęć
                        </p>
						<p>
                            •	II rata 199 zł do 20 marca
                        </p>
						<p>
                            •	III rata 199 zł do 20 kwietnia
                        </p>
						<p>
                            •	IV rata 199 zł  do 20 maja
                        </p>
						<p>
                            •	V rata 199 zł do 10 czerwca 2016.
                        </p>

						<h5>
                            Płatności za zajęcia można wnosić gotówką w Centrum Edukacyjnym lub przelewem na konto. 
                        </h5>
						
                        <h5>
                            Bezpieczne zapisy.
                        </h5>

                        <p>
                            Oferujemy możliwość rezygnacji z zajęć po pierwszych dwóch spotkaniach za pełnym zwrotem
                            wniesionych opłat.
                        </p>

                        <h5>
                            Rabat za rodzeństwo i za polecenie 15%
                        </h5>

                        <!--<p>Więcej o cenach znajdziesz <a class="program" href="/assets/download/zapisy_i_ceny.pdf"
                                                         target="_blank">tutaj.</a></p>-->

                        <p>Warunki Umowy Świadczenia Usług Edukacyjnych dostępne są <a class="program"
                                                                                     href="/assets/download/Umowa_Świadczenia_Usług_Edukacyjnych.docx"
                                                                                     target="_blank">tutaj.</a></p>

                        <!--<p>Regulamin Świadczenia Usług Edukacyjnych - Roboty dostępny jest <a class="program"
                                                                                              href="/assets/download/regulamin_swiadczenia_uslug_edukacyjnych_roboty.pdf"
                                                                                              target="_blank">tutaj.</a>
                        </p>-->
                    </div>
					<div class="lesson-item active-semestr">
                        <h4>
                            Terminy. Semestr Wiosna - Lato 2016
                        </h4>

                        <p>
                        </p>
                        <a class="location-button to-left" href="http://polygon.matplaneta.pl/Oferta.aspx">Zapisz się</a>
                    </div>
					<div class="lesson-item">
                        <h4>
                            Ferie z Matplanetą 
                        </h4>

                        <p>
						To połączenie niezwykłych warsztatów komputerowych z bogatym programem matematycznych oraz ruchowych gier i zabaw! 
                        </p>
                        <a class="location-button to-left" href="http://matplaneta.pl/zapisy-ferie-2016/">Zapisz się</a>
                    </div>
                    
					<!--
                    <div class="lesson-item">
                        <h4>
                            Zajęcia. Semestr Jesień 2015
                        </h4>

                        <p>
                           W roku szkolnym 2015-2016 Semestr 1 trwa od 19 września 2015 r. do 13 

lutego 2016 r. z przerwą na ferie zimowe.  W Trójmieście Semestr 1 kończymy 

29 stycznia 2015 r. 

Drugi semestr trwa od 15 stycznia 2016 r. do 25 czerwca 2016 r.
                        </p>
                        <ul id="harmonogram-nav">
                            <li>
                                <a class="location-button" href="zapisy-ursynow.php">Ursynów</a>
                            </li>
                            <li>
                                <a class="location-button" href="zapisy-bemowo.php">Bemowo</a>
                            </li>
                            <li>
                                <a class="location-button" href="zapisy-tarchomin.php">Tarchomin</a>
                            </li>
							<li>
                                <a class="location-button" href="zapisy-ursus.php">Ursus</a>
                            </li>
                            <li>
                                <a class="location-button" href="zapisy-jozefow.php">Józefów</a>
                            </li>
                            <li>
                                <a class="location-button" href="zapisy-jozefoslaw.php">Józefosław</a>
                            </li>
                            <li>
                                <a class="location-button" href="zapisy-zary.php">Żary</a>
                            </li>
                            <li>
                                <a class="location-button" href="zapisy-krakow.php">Kraków</a>
                            </li>
                            <li>
                            <a class="location-button" href="zapisy-gdansk.php">Gdańsk</a>
                            </li>
                            <li>
                                <a class="location-button" href="zapisy-konstancin.php">Konstancin</a>
                            </li>
                            <li>
                                <a class="location-button" href="zapisy-grodzisk.php">Grodzisk Maz.</a>
                            </li>
                            <li>
                                <a class="location-button" href="zapisy-mokotow.php">Mokotów</a>
                            </li>
                        </ul>
                    </div>
					-->
                </div>
            </section>

<!-- get footer -->
<?php require("$root/inc/footer.php");?>